import React from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import { myCustomDropzoneStyle } from '../../styles/theme/components/myCustomDropzone';
import { typographiesStyles } from '../../styles/theme/components/typographies';
import ImageUpload from '../../assets/images/imageUpload.png';

export const MyCustomDropzoneComponent = ({ accept, onFiles, files, getFilesFromEvent, onChangeStatus }) => {
    const mCDStyle = myCustomDropzoneStyle();
    const tStyle = typographiesStyles();
    
    return (
        <label className={mCDStyle.container} >
            <Box >
                <img alt='Model img' src={ImageUpload} className={mCDStyle.image} />
                <Typography variant={"body1"} className={`${mCDStyle.text} ${tStyle.body1Bold}`}>Selecciona los archivos</Typography>
                <Typography variant={"body1"} className={mCDStyle.text}>Suelta el archivo aquí<br />Tamaño máximo 20mb</Typography>
                <input
                    style={{ display: 'none' }}
                    type="file"
                    accept={accept}
                    multiple
                    onChangeStatus={onChangeStatus}
                    onChange={e => {
                        getFilesFromEvent(e).then(chosenFiles => {
                            onFiles(chosenFiles)
                        })
                    }}
                />
            </Box>
        </label>
    )
};




// const Input = ({ accept, onFiles, files, getFilesFromEvent, onChangeStatus }) => {
//     const text = files.length > 0 ? 'Add more files' : 'Choose files'

//     return (
//         <label style={{ backgroundColor: '#007bff', color: '#fff', cursor: 'pointer', padding: 15, borderRadius: 3 }}>
//             {text}
//             <input
//                 style={{ display: 'none' }}
//                 type="file"
//                 accept={accept}
//                 multiple
//                 onChangeStatus={onChangeStatus}
//                 onChange={e => {
//                     getFilesFromEvent(e).then(chosenFiles => {
//                         onFiles(chosenFiles)
//                     })
//                 }}
//             />
//         </label>
//     )
// }

// export const MyCustomDropzoneComponent = () => {
//     const handleSubmit = (files, allFiles) => {
//         console.log(files.map(f => f.meta))
//         allFiles.forEach(f => f.remove())
//     }

//     const handleChangeStatus = (meta, file, status, position) => {
//         console.log(status, position);

//     };

//     const getFilesFromEvent = e => {
//         return new Promise(resolve => {
//             getDroppedOrSelectedFiles(e).then(chosenFiles => {
//                 resolve(chosenFiles.map(f => f.fileObject))
//             })
//         })
//     }

//     return (
//         <Dropzone
//             accept="image/*,audio/*,video/*,.pdf"
//             getUploadParams={() => ({ url: 'https://httpbin.org/post' })}
//             onSubmit={handleSubmit}
//             InputComponent={Input}
//             getFilesFromEvent={getFilesFromEvent}
//             onChangeStatus={({ meta, file }, status) => handleChangeStatus(meta, file, status, 'frontCC')}
//         />
//     )
// }