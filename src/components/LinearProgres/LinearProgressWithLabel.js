// import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

// function LinearProgressWithLabel(props) {
//   return (
//     <Box display="flex" flexDirection="column" alignItems="center">
//       <Box width="100%" mr={1}>
//         <LinearProgress variant="determinate" {...props} />
//       </Box>
//       <Box display="flex" justifyContent="space-between" width="100%">
//         <Typography variant="body2" color="textSecondary">0%</Typography>
//         <Typography variant="body2" color="textSecondary">{`${Math.round(
//           props.value,
//         )}%`}</Typography>
//         <Typography variant="body2" color="textSecondary">100%</Typography>
//       </Box>
//     </Box>
//   );
// }

// LinearProgressWithLabel.propTypes = {
//   /**
//    * The value of the progress indicator for the determinate and buffer variants.
//    * Value between 0 and 100.
//    */
//   value: PropTypes.number.isRequired,
// };

// const useStyles = makeStyles({
//   root: {
//     width: '100%',
//   },
// });

// export default function LinearWithValueLabel(props) {
//   const classes = useStyles();
//   const [progress] = React.useState(props.value);

//   return (
//     <div className={classes.root}>
//       <LinearProgressWithLabel value={progress} />
//     </div>
//   );
// }

import React, { Component } from 'react';

export default class LinearWithValueLabel extends Component {
  render() {
    return (
      <div style={{ width: '100%' }}>
        <Box display="flex" flexDirection="column" alignItems="center">
          <Box width="100%" mr={1}>
            <LinearProgress variant="determinate" {...this.props} />
          </Box>
          <Box display="flex" justifyContent="space-between" width="100%">
            <Typography variant="body2" color="textSecondary">0%</Typography>
            <Typography variant="body2" color="textSecondary">{`${Math.round(this.props.value,)}%`}</Typography>
            <Typography variant="body2" color="textSecondary">100%</Typography>
          </Box>
        </Box>
      </div>
    );
  }
}