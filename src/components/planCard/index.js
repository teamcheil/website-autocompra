import React, { useState, useEffect } from 'react';
import { Card, CardContent, Checkbox, Grid, withStyles, Link, makeStyles, Typography } from '@material-ui/core';
import { GOALS_A, GOALS} from '../ImageGoals/ImagesGoals';

import Carousel from 'react-material-ui-carousel';
import { typographiesStyles } from "../../styles/theme/components/typographies";
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';


const OrangeCheckbox = withStyles({
    root: {
        color: '#fe5b02',
        '&$checked': {
            color: '#fe5b02',
        },
        padding: '0px'
    },
    checked: {},
})((props) => <Checkbox color='default' {...props} />);


const useStyles = makeStyles((theme) => ({
    cardRoot:{
        width:'287px',
        maxWidth:'287px',
        [theme.breakpoints.up('xs')]: {
            width:'244px',
            maxWidth: '244px',
        },
        [theme.breakpoints.up('md')]: {
            width:'287px',
            maxWidth: '287px',
        }
    },
    contentRoot:{
        paddingBottom: '15px !important'
    },
    containerCard:{
        display: 'flex',
        justifyContent:'center',
        width:'100%'
    },
    container:{
        display:'flex',
        alignItems:'center',
        justifyContent: 'space-between',
        textAlign: 'right',
        '& a': {
            cursor: 'pointer'
        }
    },
    imageGoal:{
        width:'60px',
        marginLeft:'.5rem'
    },
    textPlan:{
        textAlign: 'rigth !important',
    },
    Carousel: {
        [theme.breakpoints.down('sm')]: {
            '& >div:nth-child(2)': {
                height: '50% !important',
                top: '25% !important',
                '& .MuiButtonBase-root': {
                    padding: '5px',
                    right: '-15px'
                }
            },
            '& >div:nth-child(3)': {
                height: '100% !important',
                '& .MuiButtonBase-root': {
                    padding: '5px',
                    left: '-15px'
                }
            },
        },
        ['@media (max-width:359px)']: { // eslint-disable-line no-useless-computed-key
            '& >div:nth-child(1)': {
                maxWidth: '340px',
                width: '95%',
                margin: 'auto',
            },
            '& >div:nth-child(2)': {
                '& .MuiButtonBase-root': {
                    right: '-25px'
                }
            },
            '& >div:nth-child(3)': {
                '& .MuiButtonBase-root': {
                    left: '-25px'
                }
            },
        },
    }
}));

export const CarouselPlanCard = ({ items, onClick, handleOpen }) => {

    const classes = useStyles();
    const typographie = typographiesStyles();
    const { goal } = useSelector(state => state.stepOne);
    const [isChecked, setisChecked] = useState(false);
    const [idPlain, setidPlain] = useState(false);
    const [width, setWidth] = useState(window.innerWidth);
    const breakpoint = 620;


    const handleCheckBoxChange = ({ target }) =>{
        setidPlain(target.value);
        setisChecked(target.checked);
        const obj = items.filter((value) =>( value.IdPlan == target.value ));
        onClick(obj);
    }

    useEffect(() => {
        window.addEventListener("resize", () => setWidth(window.innerWidth));
    }, []);

    const handleClickDetail = (e) => {
        handleOpen(e);
    }


    return(
        <Carousel
            navButtonsAlwaysVisible={true}
            indicators={false}
            animation="fade"
            autoPlay={false}
            className={classes.Carousel}>
            {items.map((value, index)=>{
                let InitialPayment = Intl.NumberFormat(["ban", "id"]).format(value.PagoInicial);
                let InitialFee = Intl.NumberFormat(["ban", "id"]).format(value.Cuota);
                return(
                    <div key={index} className={classes.containerCard}>
                        <Card className={classes.cardRoot}>
                            <CardContent className={classes.contentRoot}>

                                <Grid container direction={"row"}>
                                    <Grid container direction={"row"} className={classes.container}>
                                        <Grid item>
                                            <OrangeCheckbox checked={(value.IdPlan ==  idPlain) ? isChecked : false } name={`idPlan_${value.IdPlan}`} value={value.IdPlan} onClick={handleCheckBoxChange} />
                                        </Grid>
                                        <Grid item>
                                            <Link onClick={handleClickDetail}><Typography variant="body1" color="primary">Ver Detalle</Typography></Link>
                                        </Grid>
                                    </Grid>

                                    <Grid container direction={"row"} className={classes.container}>
                                        <Grid item>
                                            <img alt="Goal" src={(value.IdPlan ==  idPlain) ? GOALS_A[goal] : GOALS[goal]} className={classes.imageGoal} />
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="h3" className={typographie.h1Bold}>{value.Nombre}</Typography>
                                            <Typography variant="h6" className={typographie.h1Black}>{`${value.Meses} Meses`}</Typography>
                                        </Grid>
                                    </Grid>


                                    <Grid container direction={"row"} className={classes.container}>
                                        <Grid item>
                                            <Typography className={typographie.h1Black}>Cuota de ingreso</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="h3" className={typographie.h1Bold}>{`$${InitialPayment}`}</Typography>
                                        </Grid>
                                    </Grid>

                                    <Grid container direction={"row"} className={classes.container}>
                                        <Grid item>
                                            <Typography className={typographie.h1Black}>Cuota Mensual</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="h3" className={typographie.h1Bold}>{`$${InitialFee}`}</Typography>
                                        </Grid>
                                    </Grid>

                                </Grid>
                            </CardContent>
                        </Card>
                    </div>
                )
            })}

        </Carousel>
    )
}

CarouselPlanCard.prototype = {
    items: PropTypes.array.isRequired
}
