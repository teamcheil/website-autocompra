import React from 'react';
import { Fade, makeStyles } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import IconSupport from '../../assets/images/chatA.svg';
import IconChat from '../../assets/images/iconChat.svg';
import IconPhone from '../../assets/images/iconPhone.svg';

const styles = makeStyles((theme) => ({
    root: {
        position: "absolute",
        textAlign: 'center',
        right: '-25px',
        top: '-15px',
        backgroundColor: '#fe5b02',
        borderRadius: '100%',
        width: '214px',
        height: '214px',
        color: '#fff'
    },
    closeIcon: {
        cursor: 'pointer',
        position: "absolute",
        borderRadius: '100%',
        backgroundColor: '#fff',
        color: '#000',
        border: "1px solid #fe5b02",
        width: '32px',
        height: '32px',
        right: '30px'
    },
    textTitle: {
        marginTop: '30px',
        fontSize: '18px',
        fontWeight: 'bold'
    },
    textSubtitle: {
        fontSize: '12px',
        textAlign: 'center',
        padding: '5px',
        marginTop: '-30px'
    }
}));

const ChatScript = () => {
    window.sagicc_chat_config = {
        back_url: 'https://polen.jelow.co',
        chat_token: '30a349aa18ead7cfbcfd7c7a9d07a7cf',
        chat_type: 'widget',
        client_first_name: '',
        client_last_name: '',
        client_email: '',
        auto_initialize: false
    };
    var sagicc_chat_isSecured = (window.location && window.location.protocol == 'https:');
    var sagicc_chat_css = document.createElement('link');
    sagicc_chat_css.setAttribute('rel', 'stylesheet');
    sagicc_chat_css.setAttribute('href',
        sagicc_chat_isSecured ?
            'https://polen.jelow.co/chat/assets/css/sagicc-chat.css' :
            'https://polen.jelow.co/chat/assets/css/sagicc-chat.css'
    );
    document.head.appendChild(sagicc_chat_css);
    var sagicc_chat_js = document.createElement('script');
    sagicc_chat_js.setAttribute('src',
        sagicc_chat_isSecured ?
            'https://polen.jelow.co/chat/assets/js/sagicc-chat.js' :
            'https://polen.jelow.co/chat/assets/js/sagicc-chat.js'
    );
    document.body.appendChild(sagicc_chat_js);
    abrir_widget();
}

const abrir_widget = () => {
    setTimeout(() => {
        const sagicc_chat_div = document.getElementById('sagicc-chat-div');
        try {
            var sagicc_chat_widget = sagicc_chat_div.getElementsByTagName('button');
            sagicc_chat_widget[2].click();
        } catch (e) { }
    }, 2000);
}

export const SupportComponent = ({ handleClose }) => {
    const classes = styles();

    return (
        <Fade in={true} timeout={800}>
            <div className={classes.root}>
                <CloseIcon onClick={handleClose} className={classes.closeIcon} />
                <div className={classes.textTitle} ><p>¿Tienes dudas?</p></div>
                <div className={classes.textSubtitle}><p>Si tienes dudas sobre el proceso, puedes contactar a nuestros asesores, quienes te ayudarán en cada paso.</p></div>
                <div style={{ fontSize: '12px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <div 
                        style={{ fontSize: '12px' }}
                        onClick={() => {
                            ChatScript()
                        }}
                    >
                        <img alt="Icon chat" src={IconChat} style={{ height: "23px" }} />
                        <div>Chat</div>
                    </div>
                    <div style={{ fontSize: '12px', textAlign: 'center', marginLeft: '20px' }}>
                        <img alt="Icon chat" src={IconPhone} style={{ height: "23px" }} />
                        <div style={{ fontSize: '12px', textAlign: 'center' }}>Te llamamos</div>
                    </div>
                </div>
            </div>
        </Fade>
    )
}
