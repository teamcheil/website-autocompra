import React, { useState } from 'react';
import { Modal, Backdrop, makeStyles, Fade, Button, Grid, Card, Typography, IconButton } from '@material-ui/core';
import { buttonsStyles } from '../../styles/theme/components/buttons';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100% !important',
  },
  paper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    outline: 'none',
    padding: theme.spacing(2, 4, 3),
    border: 'none',
    borderRadius: '10px',
    maxWidth: '500px',
    maxHeight: '90vh',
  },
  bgGray: {
    backgroundColor: 'rgba(242, 242, 242, 1)',
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'auto',
    maxHeight: '90vh',
    [theme.breakpoints.up('xl')]: {
      maxHeight: '90vh',
    },
    height: '90%',
    padding: '15px 0px',
  },
  iconClose: {
    display: 'flex',
    justifyContent: 'flex-end',
  }
}));

export const ModalComponent = ({ ...props }) => {
  const classes = useStyles();
  const { gray } = props;

  return (
    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      {...props}
      className={classes.modal}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={props.open}>
        <Card className={`${classes.paper} ${classes[gray ? 'bgGray' : '']}`}>
          <Grid container direction="row" className={classes.center}>
            <Grid item xs={12} sm={12} md={12} className={classes.iconClose}>
              <CloseIcon onClick={props.onClose} style={{ zIndex: 5, cursor: "pointer" }} />
            </Grid>
            {props.children}
          </Grid>
        </Card>
      </Fade>
    </Modal>
  )
};
