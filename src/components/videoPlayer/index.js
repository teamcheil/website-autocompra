/* eslint-disable react-hooks/exhaustive-deps */
import React, { useRef, useEffect } from "react";
import videojs from "video.js";
import 'video.js/dist/video-js.min.css';
import './videoPlayer.css';

export const VideoPlayer = ({src, onPlay}) => {
  const videoPlayerRef = useRef(null);
  const videoJSOptions = {
    autoplay: false,
    controls: true,
    userActions: { hotkeys: true },
    playbackRates: [0.5, 1, 1.5, 2],
    suppressNotSupportedError: true
  };

  useEffect(() => {
    const player = videojs(videoPlayerRef.current, videoJSOptions, () => {
      player.src(src);
      player.on("ended", () => {
        console.log("ended");
      });
      player.on("timeupdate", () => {
        onPlay(player.currentTime());
      });
    });
    return () => {
      player.dispose();
    };
  }, [ ]);



  return (
    <div data-vjs-player>
        <video ref={videoPlayerRef} className='video-js vjs-16-9' autoPlay={true} />
    </div>
  );
};
