import React from 'react';
import { withStyles, Checkbox } from '@material-ui/core';

export const OrangeCheckbox = withStyles({
    root: {
        color: '#fe5b02',
        '&$checked': {
            color: '#fe5b02',
        },
    },
    checked: {},
})((props) => <Checkbox color='default' {...props} />);
