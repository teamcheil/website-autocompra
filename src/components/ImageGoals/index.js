/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import Car from '../../assets/images/car.png';
import CarA from '../../assets/images/carA.svg';
import Travel from '../../assets/images/travel.png';
import TravelA from '../../assets/images/travelA.svg';
import Motorcycle from '../../assets/images/motorcycle.png';
import MotorcycleA from '../../assets/images/motorcycleA.svg';
import Phone from '../../assets/images/phone.png';
import PhoneA from '../../assets/images/phoneA.svg';
import House from '../../assets/images/house.png';
import HouseA from '../../assets/images/houseA.svg';
import { Grid, Typography } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { setGoal } from '../../redux/actions/stepOneActions';
import { imageGoalStyles } from '../../styles/theme/components/imageGoal';

const GOALS = [
    { id: '1', 'inactive': Car, 'label': 'Vehículo', 'active': CarA },
    { id: '2', 'inactive': Travel, 'label': 'Viaje', 'active': TravelA },
    { id: '3', 'inactive': Motorcycle, 'label': 'Moto', 'active': MotorcycleA },
    { id: '4', 'inactive': Phone, 'label': 'Tecnología', 'active': PhoneA },
    { id: '5', 'inactive': House, 'label': 'Remodelación', 'active': HouseA }
];

export const ImageGoal = ({ onClick }) => {
    const dispatch = useDispatch();
    const { goal } = useSelector(state => state.stepOne);
    const [idGoalAtive, setidGoalAtive] = useState(goal);
    const classes = imageGoalStyles();

    const clickGoal = (event) => {
        onClick(event);
        setidGoalAtive(event.target.id);
        dispatch(setGoal(event.target.id));
    };

    useEffect(() => {
        setidGoalAtive(goal);
    });

    return (
        <Grid spacing={2} container direction={"row"} className={classes.containerIG}>
            {GOALS.map((val, index) => {
                return (
                    <Grid item xs md={2} key={val.id}>
                        <Grid>
                            <img src={idGoalAtive === val.id ? val.active : val.inactive} id={val.id} alt={val.label} className={classes.rootIG} onClick={clickGoal} />
                            <Grid>
                                <Typography variant={"body1"} className={`${classes[idGoalAtive === val.id ? 'active' : 'inActive']}`} >{val.label}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                )
            })}
        </Grid>
    )
};
