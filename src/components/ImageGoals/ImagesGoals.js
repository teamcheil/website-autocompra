import Car from '../../assets/images/car.png';
import CarA from '../../assets/images/carA.svg';
import Travel from '../../assets/images/travel.png';
import TravelA from '../../assets/images/travelA.svg';
import Motorcycle from '../../assets/images/motorcycle.png';
import MotorcycleA from '../../assets/images/motorcycleA.svg';
import Phone from '../../assets/images/phone.png';
import PhoneA from '../../assets/images/phoneA.svg';
import House from '../../assets/images/house.png';
import HouseA from '../../assets/images/houseA.svg';

export const GOALS = {
    1:  Car,
    2: Travel,
    3: Motorcycle,
    4: Phone,
    5: House
}

export const GOALS_A = {
    1: CarA,
    2: TravelA,
    3: MotorcycleA,
    4: PhoneA,
    5: HouseA
}

export const LABEL_A = {
    1: 'Vehículo',
    2: 'Viaje',
    3: 'Moto',
    4: 'Tecnología',
    5: 'Remodelación'
}
