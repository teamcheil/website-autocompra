import React from 'react';
import { Box, Grid } from '@material-ui/core';
import { modelImageStyles } from '../../styles/theme/components/modelImage';

export const ModelImageComponent = (props) => {
    const mIStyles = modelImageStyles();
    const src = props.src || '';

    const alignBubbles = props.align !== 'left' ? (props.align !== 'right' ? mIStyles.littleBubblesBox : mIStyles.littleBubblesBoxRight) : mIStyles.littleBubblesBoxLeft;
    let width = 0;
    console.log(props)
    console.log(props.width)
    switch (props.width) {
        case 60:
            console.log("props.width: 60");
            width = mIStyles.modelImage60;
            break;
        case 70:
            width = mIStyles.modelImage70;
        break;    
        case 90:
            console.log("props.width: 90");
            width = mIStyles.modelImage90;
            break;
        case 100:
            console.log("props.width: 100");
            width = mIStyles.modelImage100;
            break;
        case 130:
            console.log("props.width: 130");
            width = mIStyles.modelImage130;
            break;
        case 150:
            console.log("props.width: 150");
            width = mIStyles.modelImage150;
            break;
        case 200:
            console.log("props.width: 200");
            width = mIStyles.modelImage200;
            break;
        default:
            console.log("props.width: 100");
            width = mIStyles.modelImage;
            break;
    }
    return (
        <Box className={mIStyles.modelContainer}>
            <Box className={mIStyles.adjustBottom}>
                <Box className={mIStyles.bubble}>
                    <Grid container direction="row" justify="center">
                        {props.children}
                    </Grid>
                </Box>
                <Box className={alignBubbles}>
                    <Box className={mIStyles.bubbleCircleLg}></Box>
                    <Box className={mIStyles.bubbleCircleMd}></Box>
                    <Box className={mIStyles.bubbleCircleSm}></Box>
                </Box>
                <img alt='Model img' src={src} className={width} />
            </Box>
        </Box>
    )
};
