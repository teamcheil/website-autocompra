

export const types = {
    setPageStep: '[PAGE STEP]',
    setGoal: '[STEP 1] set goal state', 
    setNameLastName: '[STEP 1] set names last names' ,
    setFormStepOne: '[STEP 1] SET FORM STEP ONE',
    setPlain: '[STEP 2] SET PLAN select user',
    setInfo: '[STEP 3] SET INFO',
    setContract: '[STEP 3] SET CONTRACT',
    setPayMethod: '[STEP 3] SET PAYMETHOD',
    setDocuments: '[STEP 4] set documents',
    setDataAccount: '[STEP 4] set data acounts',
    setDataToken: '[STEP 4] set data token',
}