import { types } from "../types/types";


export const setGoal = ( goalId ) => ({
    type: types.setGoal,
    payload: {
        goalId
    }
});

export const setNamesLastNames = ( names, lastNames ) => ({
    type: types.setNameLastName,
    payload: {
        names,
        lastNames
    }
});

export const setFormRegister = ( email, phone, terms_conditions ) => ({
    type: types.setFormStepOne,
    payload: {
        email,
        phone,
        terms_conditions
    }
});