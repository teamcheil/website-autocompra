import { types } from "../types/types";

export const setInfo = ( idNumber, idExpeditionDate, address, birthDate, city, departamento ) => ({
    type: types.setInfo,
    payload: {
        idNumber,
        idExpeditionDate,
        address,
        birthDate,
        city,
        departamento
    }
});

export const setContract = ( contractNumber, token ) => ({
    type: types.setContract,
    payload: {
        contractNumber,
        token
    }
});

export const setPayMethod = ( payMethod ) => ({
    type: types.setPayMethod,
    payload: {
        payMethod
    }
});