import { types } from "../types/types"


export const setPageStep = ( page ) => ({
    type: types.setPageStep,
    payload: {
        page
    }
});