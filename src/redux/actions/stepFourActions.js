import { types } from "../types/types";

export const setFormDocuments = (frontCC, backCC, photoFace) => ({
    type: types.setDocuments,
    payload: {
        photoFace,
        frontCC,
        backCC
    }
});

export const setFormDataAccount = (agree, banco, typeAccount, numberAccount) => ({
    type: types.setDataAccount,
    payload: {
        agree,
        banco,
        typeAccount,
        numberAccount,
    }
});


export const setFormDataToken = (token) => ({
    type: types.setDataToken,
    payload: {
        token
    }
});