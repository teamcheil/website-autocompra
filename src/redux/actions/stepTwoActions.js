import { types } from "../types/types";

export const setPlain = ( savingType, savingValue, IdPlan, Nombre, Meses, PagoInicial, Cuota ) => ({
    type: types.setPlain,
    payload: {
        savingType,
        savingValue,
        IdPlan,
        Nombre,
        Meses,
        PagoInicial,
        Cuota
    }
});