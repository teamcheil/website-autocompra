import { types } from '../types/types';


export const stepFourReducer = (state = {}, action) => {
    switch (action.type) {
        case types.setDocuments:
            return {
                ...state,
                photoFace: action.payload.photoFace,
                frontCC: action.payload.frontCC,
                backCC: action.payload.backCC
            }
        case types.setDataAccount:
            return {
                ...state,
                agree: action.payload.agree,
                banco: action.payload.banco,
                typeAccount: action.payload.typeAccount,
                numberAccount: action.payload.numberAccount
            }
        case types.setDataToken:
            return {
                ...state,
                token: action.payload.token,
            }
        default:
            return state;
    }
}