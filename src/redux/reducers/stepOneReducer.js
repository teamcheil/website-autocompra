import { types } from '../types/types';


export const stepOneReducer = (state = { }, action ) => {
    switch (action.type) {
        case types.setGoal:
            return{
                ...state,
                goal: action.payload.goalId
            }
        case types.setNameLastName:
            return{
                ...state,
                Names: action.payload.names,
                LastNames: action.payload.lastNames
         }
        case types.setFormStepOne:
            return{
                ...state,
                email: action.payload.email,
                phone: action.payload.phone,
                terms_conditions: action.payload.terms_conditions
            } 
        default:
            return state;
    }
}