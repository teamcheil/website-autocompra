import { types } from '../types/types';

export const stepThreeReducer = (state = {}, action) => {
    switch (action.type) {
        case types.setInfo:
            return {
                idNumber: action.payload.idNumber,
                idExpeditionDate: action.payload.idExpeditionDate,
                address: action.payload.address,
                birthDate: action.payload.birthDate,
                city: action.payload.city,
                departmento: action.payload.departamento,
            }
        case types.setContract:
            return {
                ...state,
                contractNumber: action.payload.contractNumber,
                token: action.payload.token
            }
        case types.setPayMethod:
            return {
                ...state,
                payMethod: action.payload.payMethod
            }
        default:
            return state;
    }
}