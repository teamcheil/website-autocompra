import { types } from '../types/types';


export const stepperPageReducer = (state = { }, action ) => {
    switch (action.type) {
        case types.setPageStep:
            return{
                pageStepper: action.payload.page
            }
        default:
            return state;
    }
}