import { types } from '../types/types';

export const stepTwoReducer = (state = { }, action ) => {
    switch (action.type) {
        case types.setPlain:
            return{
                savingType: action.payload.savingType,
                savingValue: action.payload.savingValue,
                plain: {
                    IdPlan: action.payload.IdPlan,
                    Nombre: action.payload.Nombre,
                    Meses: action.payload.Meses,
                    PagoInicial: action.payload.PagoInicial,
                    Cuota: action.payload.Cuota
                }
            }
        default:
            return state;
    }
}