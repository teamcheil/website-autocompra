import { createStore, combineReducers } from 'redux';
import { stepOneReducer } from '../reducers/stepOneReducer';
import { stepperPageReducer } from '../reducers/stepperPageReducer';
import { stepTwoReducer } from '../reducers/stepTwoReducer';
import { stepThreeReducer } from '../reducers/stepThreeReducer';
import { stepFourReducer } from '../reducers/stepFourReducer';

const reducers = combineReducers({
    pageStepper: stepperPageReducer,
    stepOne: stepOneReducer,
    stepTwo: stepTwoReducer,
    stepThree: stepThreeReducer,
    stepFour: stepFourReducer,
});

export const store = createStore(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);