import React, { useState, useEffect } from 'react';
import { Container, Grid, Stepper, Step, StepLabel, StepContent, Typography, Fade, Box } from '@material-ui/core';


// Styles
import { MainContainer } from '../../styles/theme/layout/main';
import { stepsStyles } from '../../styles/theme/layout/steps/steps';
import { stepperStyles } from '../../styles/theme/components/stepper';
import { typographiesStyles } from '../../styles/theme/components/typographies';

import ArrowUp from '../../assets/images/arrowUp.png';
import ArrowDown from '../../assets/images/arrowDown.png';
import IconSupport from '../../assets/images/chatA.svg';
import IconChat from '../../assets/images/iconChat.svg';
import IconPhone from '../../assets/images/iconPhone.svg';

// Steps
import Step1 from './stepper/steps/step1/step1';
import Step11 from './stepper/steps/step1/step1.1';
import Step12 from './stepper/steps/step1/step1.2';
import Step13 from './stepper/steps/step1/step1.3';
import Step14 from './stepper/steps/step1/step1.4';
import Step2 from './stepper/steps/step2/step2';
import Step21 from './stepper/steps/step2/step21';
import Step22 from './stepper/steps/step2/step22';
import Step23 from './stepper/steps/step2/step23';
import Step24 from './stepper/steps/step2/step24';

import Step3 from './stepper/steps/step3/step3';
import Step31 from './stepper/steps/step3/step3.1';
import Step32 from './stepper/steps/step3/step3.2';
import Step33 from './stepper/steps/step3/step3.3';
import Step34 from './stepper/steps/step3/step3.4';
import Step35 from './stepper/steps/step3/step3.5';
import Step36 from './stepper/steps/step3/step3.6';

import Step4 from './stepper/steps/step4/step4';
import Step41 from './stepper/steps/step4/step4.1';
import Step42 from './stepper/steps/step4/step4.2';
import Step43 from './stepper/steps/step5/step5';
import LinearWithValueLabel from "../../components/LinearProgres/LinearProgressWithLabel";
import { SupportComponent } from '../../components/Support';
import { useMediaQuery } from "@material-ui/core";

export default ({ ...props }) => {
    const stepClasses = stepsStyles();
    const stepperClasses = stepperStyles();
    const tClasses = typographiesStyles();
    const [open, setOpen] = useState(false);
    const [step, setStep] = useState(1);
    const [lastStep, setLastStep] = useState(1);
    const [percentage, setPercentage] = useState(0);
    const isSmallScreen = useMediaQuery(theme => theme.breakpoints.down("sm"));
    const lastStepCanChange = 12;

    const scrollToRef = (ref, position = 0) => window.scrollTo(position, ref.current.offsetTop)
    const myRef = React.useRef(null)

    const nextStep = (index, positionScroll = 0) => {
        if (isNaN(parseInt(index))) {
            setStep(step + 1);
        } else {
            setStep(parseInt(index));
        }
        let temp = parseInt(((100 / 21) * (step + 1)));
        setPercentage(temp);
        setLastStep(lastStep < (step + 1) ? step + 1 : lastStep);
        SetPorcentageFcuntion(step);
        scrollToRef(myRef, positionScroll);
        return step;
    };


    const AllSteps4 = [
        {
            label: 'Datos Adicionales',
            labelDetail: 'Conocerte mejor es nuestra prioridad.',
            paso: 4,
            type: 'MyStepMain',
            content: <AutoImcrement nextStep={nextStep} />
        },
        {
            label: '¿Para qué sirven los datos adicionales?',
            labelDetail: '',
            paso: 4,
            content: <Step4 nextStep={nextStep} />
        },
        {
            label: 'Chequeo de identidad',
            labelDetail: '',
            paso: 4,
            content: <Step41 nextStep={nextStep} />
        },
        {
            label: 'Información bancaria',
            labelDetail: '',
            paso: 4,
            content: <Step42 nextStep={nextStep} />
        },
        {
            label: 'Felicitaciones',
            labelDetail: '',
            type: 'MyStepMain',
            paso: 5,
            content: <Step43 nextStep={nextStep} />
        },
    ];

    const steps = [
        {
            label: 'Conozcámonos',
            labelDetail: 'Te explicamos cómo funciona nuestro sistema.',
            type: 'MyStepMain',
            paso: 1,
            content: <Step1 nextStep={nextStep} />
        },
        {
            label: 'Elige tu meta',
            labelDetail: '',
            paso: 1,
            content: <Step1 nextStep={nextStep} />
        },
        {
            label: 'Ingresa tu nombre',
            labelDetail: '',
            paso: 1,
            content: <Step12 nextStep={nextStep} />
        },
        {
            label: 'Bienvenida',
            labelDetail: '',
            paso: 1,
            content: <Step13 nextStep={nextStep} />
        },
        {
            label: 'Datos adicionales',
            labelDetail: '',
            paso: 1,
            content: <Step14 nextStep={nextStep} />
        },
        {
            label: 'Simula',
            type: 'MyStepMain',
            paso: 2,
            labelDetail: 'Podrás comparar y escoger el mejor plan.',
            content: <AutoImcrement nextStep={nextStep} />
            // content: <Step2 nextStep={nextStep} />
        },
        {
            label: '¿Cómo funciona la simulación de Polen?',
            paso: 2,
            labelDetail: '',
            content: <Step21 nextStep={nextStep} />
        },
        {
            label: 'Selecciona el valor de tu meta o de tu cuota mensual',
            labelDetail: '',
            paso: 2,
            content: <Step22 nextStep={nextStep} />
        },
        {
            label: 'Video',
            labelDetail: '',
            paso: 2,
            content: <Step23 nextStep={nextStep} />
        },
        {
            label: 'Resumen de tu plan',
            labelDetail: '',
            paso: 2,
            content: <Step24 nextStep={nextStep} />
        },
        {
            label: 'Adquiere tu plan',
            type: 'MyStepMain',
            paso: 3,
            labelDetail: 'Decídete por Polen y paga tu plan con el medio de pago que prefieras',
            content: <AutoImcrement nextStep={nextStep} />
            // content: <Step3 nextStep={nextStep} />
        },
        {
            label: 'Datos de compra ',
            labelDetail: '',
            paso: 3,
            content: <Step31 nextStep={nextStep} />
        },
        {
            label: 'Token y contrato',
            labelDetail: '',
            paso: 3,
            content: <Step32 nextStep={nextStep} />
        },
        {
            label: 'Validación token y contrato',
            labelDetail: '',
            paso: 3,
            content: <Step33 nextStep={nextStep} />
        },
        {
            label: 'Paga',
            labelDetail: '',
            paso: 3,
            content: <Step34 nextStep={nextStep} />
        },
        {
            label: 'Resumen de transacción',
            labelDetail: '',
            paso: 3,
            content: <Step35 nextStep={nextStep} />
        },
        {
            label: 'Felicitaciones ',
            labelDetail: '',
            paso: 3,
            content: <Step36 nextStep={nextStep} />
        },
        ...AllSteps4
    ];

    const handleChangeStep = (index) => {
        const canChangeStep = lastStep < lastStepCanChange;
        if (lastStep >= index && canChangeStep) {
            nextStep(index);
        }
    };

    const SetPorcentageFcuntion = (step) => {
        let Leng = steps.length - 1;
        let temp = parseInt(((100 / Leng) * (step + 1)));
        setPercentage(temp)
    };

    const handleClose = () => {
        setOpen(!open);
    };

    return (
        <MainContainer className={stepClasses.mainContainer}>
            <Grid container direction={"row"}>
                <Grid item xs={12} className={stepClasses.showMobile}>
                    <Grid container className={stepClasses.mobileHelpButton} onClick={handleClose}>
                        <Grid item xs={4} className={stepClasses.mobileHelpButtonIcon}>
                            <img alt="Icon chat" src={IconChat} />
                        </Grid>
                        <Grid item xs={4}>
                            <Typography variant={"h1"} className={`${tClasses.h116White} ${stepClasses.alignCenter}`}>¿Tienes dudas?</Typography>
                        </Grid>
                        <Grid item xs={3} className={stepClasses.mobileHelpButtonArrow}>
                            <img alt="Icon chat" src={!open ? ArrowDown : ArrowUp} />
                        </Grid>
                    </Grid>
                    {open &&
                        <Fade in={true} timeout={800}>
                            <Box className={stepClasses.mobileHelpContainer}>
                                <Typography variant={"h1"} className={`${tClasses.h118White} ${stepClasses.mt30p}`}>¿Necesitas ayuda?</Typography>
                                <Typography variant={"body1"} className={`${tClasses.body116White} ${stepClasses.mobileHelpBody1}`}>Si tienes dudas sobre el proceso puedes contactar a nuestros asesores te ayudaran en cada paso.</Typography>
                                <Grid container>
                                    <Grid item xs={2}></Grid>
                                    <Grid item xs={4} className={`${stepClasses.mobileHelpContainerButton}`}>
                                        <img alt="Icon chat" src={IconChat} />
                                        <Typography variant={"body1"} className={`${tClasses.body116White}`}>Chat</Typography>
                                    </Grid>
                                    <Grid item xs={4} className={`${stepClasses.mobileHelpContainerButton}`}>
                                        <img alt="Icon chat" src={IconPhone} />
                                        <Typography variant={"body1"} className={`${tClasses.body116White}`}>Te llámamos</Typography>
                                    </Grid>
                                    <Grid item xs={2}></Grid>
                                </Grid>
                            </Box>
                        </Fade>
                    }
                </Grid>
                <div className={`${stepClasses.showMobile} ${stepClasses.LinearMobile}`}>
                    <LinearWithValueLabel value={percentage} />
                </ div>
                <Grid item xs={12} md={3}>
                    <div>
                        <Stepper ref={myRef} activeStep={step} orientation="vertical" className={stepperClasses.MyStepper}>
                            {
                                steps.map((item, index) => (
                                    <Step key={item.label} className={`${stepperClasses[item.type || 'MyStepSecondary']} ${item.paso === steps[step].paso ? 'active' : 'inactive'}`}>
                                        <StepLabel className={`${item.paso === steps[step].paso ? 'active' : 'inactive'}`} onClick={() => { handleChangeStep(index) }}>
                                            <Typography variant={"h1"} className={`${tClasses.h1Stepper}`}>{item.label}</Typography>
                                            <Typography variant={"body1"}>{item.labelDetail}</Typography>
                                            <Typography variant={"h1"} className={`${tClasses.h1StepperNum} ${'h1StepperNum'}`}>{item.paso}</Typography>
                                        </StepLabel>
                                        <StepContent className={`${stepClasses.showMobile}`}>
                                            {isSmallScreen && item.content}
                                        </StepContent>
                                    </Step>
                                ))
                            }
                        </Stepper>
                    </div>
                </Grid>
                <Grid item md={9} className={stepClasses.showDesktop}>
                    <Container>
                        <Grid container direction={"row"}>
                            <Grid item md={11}>
                                {!isSmallScreen && steps[step].content}
                            </Grid>
                            <Grid item md={1} style={{ position: 'relative' }}>
                                <img alt="" src={IconSupport} style={{ cursor: 'pointer' }} onClick={handleClose} className={stepClasses.iconSupport} />
                                {open && <SupportComponent handleClose={handleClose} />}
                            </Grid>
                        </Grid>
                    </Container>
                </Grid>
            </Grid>
        </MainContainer>
    );
}

const AutoImcrement = ({ nextStep }) => {
    useEffect(() => {
        const timer = setTimeout(() => {
            nextStep();
        }, 100);
        return () => clearTimeout(timer);
    });
    return (
        <Grid></Grid>
    );
}
