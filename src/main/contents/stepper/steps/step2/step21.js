import React, { useRef, useState } from 'react';
import { Button, Grid, Typography, Fade } from '@material-ui/core';

//styles
import { buttonsStyles } from '../../../../../styles/theme/components/buttons'
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps'
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import imageSrc from "../../../../../assets/images/model_step1_3.png";

import car from "../../../../../assets/images/car.png";
import travel from "../../../../../assets/images/travel.png";
import motorcycle from "../../../../../assets/images/motorcycle.png";
import phone from "../../../../../assets/images/phone.png";
import house from "../../../../../assets/images/house.png";

import leftArrow from "../../../../../assets/images/flecha-izquierda.svg";
import rightArrow from "../../../../../assets/images/flecha-derecha.svg";
import close from "../../../../../assets/images/close.svg";
import iconWarning from "../../../../../assets/images/iconWarning.png";

import iconopopuppaso2 from "../../../../../assets/images/icono-popup-paso-2.svg";
import iconopopuppaso3 from "../../../../../assets/images/icono-popup-paso-3.svg";
import iconopopuppaso4 from "../../../../../assets/images/icono-popup-paso-4.svg";
import iconopopuppaso5 from "../../../../../assets/images/icono-popup-paso-5.svg";

//components
import LinearProgressWithLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { VideoPlayer } from '../../../../../components/videoPlayer';
import { ModelImageComponent } from '../../../../../components/ModelImage';

import { useDispatch, useSelector } from 'react-redux';
import { setPlain } from '../../../../../redux/actions/stepTwoActions';
import { bold, underline } from 'ansi-colors';
import { fontWeight } from '@material-ui/system';

export default ({ nextStep }) => {

    const [iconopopup, setIconopopup] = useState(false);

    const { email, phone } = useSelector(state => state.stepOne)

    const dispatch = useDispatch();
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const typographiesClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);
    const refVideo = useRef(null);
    const src = "https://media.w3.org/2010/05/sintel/trailer_hd.mp4";

    const [show, setShow] = useState(false);
    const [show2, setShow2] = useState(false);
    const [show3, setShow3] = useState(false);
    const [show4, setShow4] = useState(false);
    const [deleteSimulationIndex, setDeleteSimulationIndex] = useState(-1);
    const [simulationIndex, setSimulationIndex] = useState(-1);

    const scrollRef = React.createRef();
    const [scrollConstant, setScrollConstant] = useState(60);

    const [simulations, setSimulations] = useState([
        {
            simulationID: 1,
            planID: 1,
            planTypeID: 1,
            planName: "Plan vehículo 1",
            planSpan: "24",
            initialPayment: "100",
            goalValue: "30000000",
            monthlyFee: "500000",
            simulationStageID: 9,
            simulationStage: "Resumen de simulación"
        },
        {
            simulationID: 2,
            planID: 2,
            planTypeID: 1,
            planName: "Plan vehículo 2",
            planSpan: "24",
            initialPayment: "100",
            goalValue: "30000000",
            monthlyFee: "1000000",
            simulationStageID: 10,
            simulationStage: "Validación de contrato y token"
        },
        {
            simulationID: 3,
            planID: 3,
            planTypeID: 3,
            planName: "Plan moto 1",
            planSpan: "36",
            initialPayment: "100",
            goalValue: "60000000",
            monthlyFee: "500000",
            simulationStageID: 13,
            simulationStage: "Paga"
        },
        {
            simulationID: 1,
            planID: 1,
            planTypeID: 1,
            planName: "Plan vehículo 1",
            planSpan: "24",
            initialPayment: "100",
            goalValue: "30000000",
            monthlyFee: "500000",
            simulationStageID: 14,
            simulationStage: "Estado CRM"
        },
        {
            simulationID: 2,
            planID: 2,
            planTypeID: 1,
            planName: "Plan vehículo 2",
            planSpan: "24",
            initialPayment: "100",
            goalValue: "30000000",
            monthlyFee: "1000000",
            simulationStageID: 9,
            simulationStage: "Estado CRM"
        },
        {
            simulationID: 3,
            planID: 3,
            planTypeID: 3,
            planName: "Plan moto 1",
            planSpan: "36",
            initialPayment: "100",
            goalValue: "60000000",
            monthlyFee: "500000",
            simulationStageID: 9,
            simulationStage: "Estado CRM"
        }
    ]);

    const onPlay = (currentTime) => {
        const currentTimeControl = 2.5;
        (currentTime > currentTimeControl) ? setFormValid(true) : setFormValid(false);
        let s = simulations
        s.map((item) => {
            item["isSelected"] = false
        })
        setSimulations(s)
    };

    const onDeleteSimulation = (index) => {
        setShow(false)
        setShow3(true)
        setDeleteSimulationIndex(index)
    };

    const onCardSelection = (index) => {
        let s = simulations
        s.map((item, i) => {
            i == index ? s[index]["isSelected"] = true : item["isSelected"] = false
        })
        setSimulations(s)
        setSimulationIndex(index)
    };

    const onModalClose = () => {
        setShow(false)
        setSimulationIndex(-1)
        setDeleteSimulationIndex(-1)
        let s = simulations
        s.map((item) => {
            item["isSelected"] = false
        })
        setSimulations(s)
    };

    const onContinue = () => {
        dispatch(setPlain(simulations[simulationIndex].planTypeID, simulations[simulationIndex].goalValue, simulations[simulationIndex].planID, simulations[simulationIndex].planName, simulations[simulationIndex].planID, simulations[simulationIndex].planSpan, simulations[simulationIndex].planID, simulations[simulationIndex].initialPayment, simulations[simulationIndex].monthlyFee));
        nextStep(simulations[simulationIndex].simulationStageID)
    };

    return (
        <Fade in={true} timeout={900}>
            <>
                <Grid container direction={"row"}>
                    <Grid item xs={12} md={7}>
                        <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                            <LinearProgressWithLabel value={35} />
                        </Grid>
                        <Grid item xs={12} className={stepsClasses.withLinearProgress}>
                            <Typography variant="h3">Paso 2</Typography>
                            <Typography variant="h1">Simula tu Plan</Typography>
                            <Typography variant="body1">Nuestro simulador te mostrará diferentes opciones para que unidos logremos llegar a tu meta.</Typography>
                            <Grid item xs={12} className={stepsClasses.mt35}>
                                <Typography variant="h6">Ahora descubrirás por qué Polen es diferente.</Typography>
                                <VideoPlayer src={src} onPlay={onPlay} />
                            </Grid>
                            <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                                <Button variant="contained"
                                    onClick={() => {
                                        setShow(!show)
                                        console.log("show: ", show)
                                        //nextStep()
                                    }} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Siguiente</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={5} className={stepsClasses.modelLeft}>
                        <ModelImageComponent src={imageSrc} align="center" width={90}>
                            <Typography variant="h1" className={typographiesClasses.h1Black}><span className={typographiesClasses.h1Black}>¡En comunidad llegamos a donde queremos estar!</span></Typography>
                        </ModelImageComponent>
                    </Grid>
                </Grid>
                {
                    show4 ?
                        <Grid className={stepsClasses.modalContainer}>

                            <Grid className={stepsClasses.modal2}>
                                <img alt='' src={close} className={stepsClasses.close2} onClick={() => { setShow(true); setShow4(false) }} />
                                <Grid className={stepsClasses.modalContent2}>
                                    <img alt='' src={iconWarning} className={stepsClasses.iconopopuppaso2} />
                                    <Typography className={stepsClasses.title}>
                                        Nueva simulación
                                    </Typography>
                                    <Typography className={stepsClasses.subtitle2}>
                                        ¿Estás seguro de que deseas realizar una nueva simulación?
                                    </Typography>
                                    <Grid className={stepsClasses.buttonsContainer}>
                                        <Grid
                                            className={stepsClasses.enabledButton}
                                            onClick={() => {
                                                setShow(true); setShow4(false)
                                            }}
                                        >
                                            <Typography className={stepsClasses.enabledButtonText}>
                                                NO
                                        </Typography>
                                        </Grid>
                                        <Grid
                                            className={stepsClasses.enabledButton2}
                                            onClick={() => {
                                                nextStep(7)
                                            }}
                                        >
                                            <Typography className={stepsClasses.enabledButton2Text}>
                                                SÍ
                                        </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                        </Grid>
                        :
                        null
                }
                {
                    show3 ?
                        <Grid className={stepsClasses.modalContainer}>

                            <Grid className={stepsClasses.modal2}>
                                <img alt='' src={close} className={stepsClasses.close2} onClick={() => { setShow(true); setShow3(false) }} />
                                <Grid className={stepsClasses.modalContent2}>
                                    <img alt='' src={iconWarning} className={stepsClasses.iconopopuppaso2} />
                                    <Typography className={stepsClasses.title}>
                                        Eliminar simulación
                                    </Typography>
                                    <Typography className={stepsClasses.subtitle2}>
                                        ¿Estás seguro de que deseas eliminar esta simulación?
                                    </Typography>
                                    <Grid className={stepsClasses.buttonsContainer}>
                                        <Grid
                                            className={stepsClasses.enabledButton}
                                            onClick={() => {
                                                setShow(true); setShow3(false)
                                            }}
                                        >
                                            <Typography className={stepsClasses.enabledButtonText}>
                                                NO
                                        </Typography>
                                        </Grid>
                                        <Grid
                                            className={stepsClasses.enabledButton2}
                                            onClick={() => {
                                                onModalClose();
                                                setShow(true)
                                                setShow3(false);
                                            }}
                                        >
                                            <Typography className={stepsClasses.enabledButton2Text}>
                                                SÍ
                                        </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                        </Grid>
                        :
                        null
                }
                {
                    show2 ?
                        <Grid className={stepsClasses.modalContainer}>

                            <Grid className={simulations[simulationIndex].simulationStageID == 13 ? stepsClasses.modal3 : stepsClasses.modal2}>
                                <img alt='' src={close} className={stepsClasses.close2} onClick={() => { onModalClose(); setShow2(false) }} />
                                <Grid className={simulations[simulationIndex].simulationStageID == 13 ? stepsClasses.modalContent3 : stepsClasses.modalContent2}>
                                    <img alt='' src={iconopopup} className={stepsClasses.iconopopuppaso2} />
                                    {
                                        simulations[simulationIndex].simulationStageID == 13 ?
                                            <Typography className={stepsClasses.subtitle2}>
                                                Tu plan de compra programada espera <br />
                                                por ti. <br /><br />
                                                Hemos enviado a tu email: <br /><br />
                                                <span style={{ fontWeight: "bold", color: "#fe5b02" }}>{"email@email.com"}</span> <br /><br />
                                                La información de tu plan, con tu <span style={{ fontWeight: "bold" }}>número</span> <br />
                                                <span style={{ fontWeight: "bold" }}>de contrato</span>, y un <span style={{ fontWeight: "bold" }}>token</span> a tu celular: <br /><br />
                                                <span style={{ fontWeight: "bold", color: "#fe5b02" }}>{"333 333 33 33"}</span> <br /><br />
                                                Ingresa estos números a continuación
                                            </Typography>
                                            : simulations[simulationIndex].simulationStageID == 14 ?
                                                <Typography className={stepsClasses.subtitle2}>
                                                    Cada vez estamos más cerca del plan <br />
                                                    que hará realidad tu meta. <br />
                                                    Selecciona el método de pago para<br />
                                                    continuar con el proceso
                                            </Typography>
                                                :
                                                <Typography className={stepsClasses.subtitle2}>
                                                    {
                                                        simulations[simulationIndex].simulationStageID == 10 ?
                                                            "Pronto obtendrás el plan con el cual" + "\n" +
                                                            "alcanzarás tu meta. Ingresa algunos" + "\n" +
                                                            "datos personales para continuar con el" + "\n" +
                                                            "proceso."
                                                            :
                                                            "La meta que tanto deseas espera por ti." + "\n" +
                                                            "Confirma tu plan y el seguro para" + "\n" +
                                                            "continuar con el proceso."
                                                    }
                                                </Typography>
                                    }
                                    <Typography className={stepsClasses.title2}>
                                        ¡Con Polen, la logras!
                                    </Typography>
                                    <Grid
                                        className={stepsClasses.enabledButton2}
                                        onClick={() => {
                                            onContinue()
                                        }}
                                    >
                                        <Typography className={stepsClasses.enabledButton2Text}>
                                            Siguiente
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>

                        </Grid>
                        :
                        null
                }
                {
                    show ?
                        <Grid className={stepsClasses.modalContainer}>
                            <Grid className={stepsClasses.modal}>
                                <img alt='' src={close} className={stepsClasses.close} onClick={() => { onModalClose() }} />
                                <Grid className={stepsClasses.modalContent}>
                                    <Typography className={stepsClasses.title}>
                                        Tus simulaciones
                                    </Typography>
                                    <Typography className={stepsClasses.subtitle}>
                                        Estas son tus simulaciones previas, por favor selecciona una para continuar o inicia otra nueva.
                                    </Typography>
                                    <Grid className={stepsClasses.simulationsContainerWrapper}>

                                        <Grid
                                            className={stepsClasses.arrowsContainer}
                                            onClick={() => {
                                                console.log("REF: ", scrollRef);
                                                scrollRef.current.scrollTo((scrollRef.current.scrollLeft - scrollConstant), 0)
                                            }}
                                        >
                                            <img alt='' src={leftArrow} />
                                        </Grid>
                                        <Grid ref={scrollRef} className={stepsClasses.simulationsContainer}>
                                            {
                                                simulations.map((item, index) => {
                                                    let icon
                                                    switch (item.planTypeID) {
                                                        case 1:
                                                            icon = car
                                                            break;
                                                        case 2:
                                                            icon = travel
                                                            break;
                                                        case 3:
                                                            icon = motorcycle
                                                            break;
                                                        case 4:
                                                            icon = phone
                                                            break;
                                                        case 5:
                                                            icon = house
                                                            break;
                                                    }
                                                    return (
                                                        <Grid
                                                            key={index}
                                                            className={item.isSelected ? stepsClasses.simulationCardSelected : stepsClasses.simulationCard}
                                                            onClick={() => {
                                                                onCardSelection(index)
                                                            }}
                                                        >
                                                            <img alt='' src={icon} className={stepsClasses.simulationCardIcon} />
                                                            <Typography className={stepsClasses.simulationCardPlanName}>
                                                                {item.planName}
                                                            </Typography>
                                                            <Typography>
                                                                {item.planSpan} meses
                                                            </Typography>
                                                            <Typography className={stepsClasses.simulationCardLabels}>
                                                                Valor meta
                                                            </Typography>
                                                            <Typography className={stepsClasses.simulationCardValues}>
                                                                ${item.goalValue}
                                                            </Typography>
                                                            <Typography className={stepsClasses.simulationCardLabels}>
                                                                Valor cuota mensual
                                                            </Typography>
                                                            <Typography className={stepsClasses.simulationCardValues}>
                                                                ${item.monthlyFee}
                                                            </Typography>
                                                            <Typography className={stepsClasses.simulationCardLabels}>
                                                                Etapa de simulación
                                                            </Typography>
                                                            <Typography className={stepsClasses.simulationCardValues}>
                                                                "{item.simulationStage}"
                                                            </Typography>
                                                            <Typography className={stepsClasses.simulationCardDelete} onClick={() => { onDeleteSimulation(index) }}>
                                                                Eliminar simulación
                                                            </Typography>
                                                        </Grid>
                                                    )
                                                })
                                            }
                                        </Grid>
                                        <Grid
                                            className={stepsClasses.arrowsContainer}
                                            onClick={() => {
                                                console.log("REF: ", scrollRef);
                                                scrollRef.current.scrollTo((scrollRef.current.scrollLeft + scrollConstant), 0)
                                            }}
                                        >
                                            <img alt='' src={rightArrow} />
                                        </Grid>

                                    </Grid>
                                    <Grid className={stepsClasses.buttonsContainer}>
                                        <Grid
                                            className={stepsClasses.enabledButton}
                                            onClick={() => {
                                                setShow4(true)
                                                setShow(false)
                                                //onContinue()
                                            }}
                                        >
                                            <Typography className={stepsClasses.enabledButtonText}>
                                                Nueva simulación
                                            </Typography>
                                        </Grid>
                                        <Grid
                                            className={simulationIndex < 0 ? stepsClasses.disabledButton2 : stepsClasses.enabledButton2}
                                            onClick={() => {
                                                if (simulationIndex >= 0) {
                                                    console.log("simulationIndex", simulationIndex)
                                                    switch (simulations[simulationIndex].simulationStageID) {
                                                        case 10:
                                                            setIconopopup(iconopopuppaso3)
                                                            break;
                                                        case 13:
                                                            setIconopopup(iconopopuppaso4)
                                                            break;
                                                        case 14:
                                                            setIconopopup(iconopopuppaso5)
                                                            break;
                                                        default:
                                                            setIconopopup(iconopopuppaso2)
                                                            break;
                                                    }
                                                    setShow2(true)
                                                    setShow(false)
                                                }
                                                //onContinue()
                                            }}
                                        >
                                            <Typography className={simulationIndex < 0 ? stepsClasses.disabledButton2Text : stepsClasses.enabledButton2Text}>
                                                Continuar
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        :
                        null
                }
            </>
        </Fade>
    )

}
