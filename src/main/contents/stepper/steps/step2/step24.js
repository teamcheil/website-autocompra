import React, { useState } from 'react';
import { Grid, Typography, Button, Divider, makeStyles, Checkbox, Card, CardContent, Fade, Link } from '@material-ui/core';
import CarA from '../../../../../assets/images/carA.svg';
import TravelA from '../../../../../assets/images/travelA.svg';
import MotorcycleA from '../../../../../assets/images/motorcycleA.svg';
import PhoneA from '../../../../../assets/images/phoneA.svg';
import HouseA from '../../../../../assets/images/houseA.svg';

import { GOALS_A, LABEL_A } from '../../../../../components/ImageGoals/ImagesGoals';

import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import srcImg from "../../../../../assets/images/model_step1_3.png";
import LinearWithValueLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { ModelImageComponent } from '../../../../../components/ModelImage';
import { useSelector } from 'react-redux';
import { OrangeCheckbox } from '../../../../../components/OrangeCheckBox';
import { ModalComponent } from '../../../../../components/Modal';
import SuportImg from '../../../../../assets/images/supportA.svg';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '400px',
        backgroundColor: 'rgba(242, 242, 242, 1)',
        marginBottom: '50px',
        borderRadius: '4px',
        boxShadow: 'none'
    },
    container: {
        display: 'flex'
    },
    boxBtnNext: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: '30px'
    },
    textStart: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        [theme.breakpoints.down('md')]: {
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
        }
    },
    textEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        [theme.breakpoints.down('xs')]: {
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
        }
    },
    textCenter: {
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    },
    textCenter2: {
        textAlign: 'left',
        [theme.breakpoints.down('md')]: {
            textAlign: 'center',
        }
    },
    textCenter3: {
        textAlign: 'right',
        [theme.breakpoints.down('md')]: {
            textAlign: 'center',
        }
    },
    Checkbox: {
        padding: '0px'
    },
    chekR: {
        marginRight: '.8rem',
        textAlign: 'center'
    },
    center: {
        textAlign: 'center'
    },
    flexCenter: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    left: {
        marginLeft: '.5rem'
    },
    helpIcon: {
        color: 'gray'
    },
    flex2: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    flex: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginRight: 10,
        [theme.breakpoints.down('xs')]: {
            marginRight: 0,
            justifyContent: "flex-start",
            alignItems: "flex-start",
        }
    },
    ml10: {
        marginLeft: 10,
        [theme.breakpoints.down('xs')]: {
            marginLeft: 0,
        }
    },
    ml5: {
        marginLeft: 5,
        [theme.breakpoints.down('xs')]: {
            marginLeft: 5,
        }
    },
    bold: {
        fontWeight: "bold"
    }
}));

export default ({ nextStep }) => {
    const classes = useStyles();
    const { goal, Names, LastNames, email } = useSelector(state => state.stepOne);
    const { savingType, savingValue } = useSelector(state => state.stepTwo);
    const { Nombre, Meses, PagoInicial, Cuota } = useSelector(state => state.stepTwo.plain);
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const typographiesClasses = typographiesStyles();
    const [isCheckedInsurance, setisCheckedInsurance] = useState(true);
    const savingValueFormat = Intl.NumberFormat(["ban", "id"]).format(savingValue);
    const initialPayment = Intl.NumberFormat(["ban", "id"]).format(PagoInicial);
    const initialFee = Intl.NumberFormat(["ban", "id"]).format(Cuota);
    const subTotal = Intl.NumberFormat(["ban", "id"]).format(parseFloat(PagoInicial) + parseFloat(Cuota));
    const [open, setOpen] = useState(false);

    const [openHere, setOpenHere] = useState(false);
    const [openPayments, setOpenPayments] = useState(false);

    const handleChangeCheckBox = (e) => {
        setisCheckedInsurance(e.target.checked);
        if (!e.target.checked) {
            handleOpen();
        }
    }

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = (value) => {
        if (value == "here") {
            setOpenHere(false);
        } else if (value == "payments") {
            setOpenPayments(false);
        } else {
            setOpen(false);
        }
    };

    const handleDecline = (e) => {
        setisCheckedInsurance(true);
        setOpen(false);
    }

    return (
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>
                <ModalComponent open={open} onClose={handleClose}>

                    <Grid item xs={12} md={12} className={classes.center}>
                        <img alt="" src={SuportImg} />
                    </Grid>

                    <Grid item xs={12} md={12} className={classes.center} >
                        <Typography variant="h1" color="inherit">Seguro de vida</Typography>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <Typography variant="body2" style={{ marginTop: 15, fontWeight: 'bold' }}>El Seguro de vida te brinda los siguientes beneficios:</Typography>
                        <Typography variant="body1">
                            Ante tu ausencia, los seres que más amas estarán acompañados en el duelo por profesionales que les garantizarán el logro de la meta que tú definiste.
                    </Typography>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <Typography variant="body1">
                            Disfruta de un alivio que te permitirá compensar la disminución de tus ingresos en caso de incapacidad por enfermedades o accidentes.
                    </Typography>
                    </Grid>

                    <Grid item xs={12} md={12} className={`${classes.center} ${stepsClasses.mt30} ${stepsClasses.mb20}`}>
                        <Typography variant="h2" color="inherit" style={{ fontWeight: 'bold' }}>¿Deseas tomar el seguro de vida con tu plan?</Typography>
                    </Grid>

                    <Grid item xs={12} className={`${classes.center} ${stepsClasses.mt20}`} >
                        <Grid container>
                            <Grid item xs={12} md={6} className={`${stepsClasses.mb10}`}>
                                <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.whiteButton}`} onClick={handleClose}>NO</Button>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={handleDecline}>SÍ</Button>
                            </Grid>
                        </Grid>
                    </Grid>

                </ModalComponent>

                <ModalComponent open={openHere} onClose={() => handleClose("here")}>

                    <Grid item xs={12} md={12} className={classes.center}>
                        <img alt="" src={SuportImg} style={{ width: "96px", marginBottom: "20px" }} />
                    </Grid>

                    <Grid item xs={12} md={12} className={classes.center} >
                        <Typography variant="h1" color="inherit">Exclusiones Seguro de Vida</Typography>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <Typography variant="body2" style={{ marginTop: 15, fontWeight: 'bold' }}>A la hora de adquirir tu seguro debes tener en cuenta:</Typography>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <Typography variant="body1">
                            1. La cobertura de tu seguro de vida inicia en el mes que participes en la primera asamblea de adjudicación.
                        </Typography>
                        <Typography variant="body1">
                            2. Si eres mayor de 60 años y/o el valor asegurado en vida de todos tus planes es mayor a 200 millones, te contactaremos para diligenciar la solucitud individual del seguro, previo a la expedición del mismo.
                        </Typography>
                        <Typography variant="body1">
                            3. Las exclusiones del seguro de vida son las siguientes:
                        </Typography>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <Typography variant="body2" style={{ marginTop: 15, fontWeight: 'bold' }}>EXCLUSIONES POR CONDICIÓN FÍSICA:</Typography>
                        <Typography variant="body1">
                            - Personas discapacitadas.
                        </Typography>
                        <Typography variant="body1">
                            - Personas que sufran de epilepsia.
                        </Typography>
                        <Typography variant="body1">
                            - Personas con trastornos mentales y drogadicción.
                        </Typography>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <Typography variant="body2" style={{ marginTop: 15, fontWeight: 'bold' }}>EXCLUSIONES POR OCUPACIÓN:</Typography>
                        <Typography variant="body1">
                            - Trabajadores de obras subterraneas. Mineros de socavón y túneles.
                        </Typography>
                        <Typography variant="body1">
                            - Mataderos (matarifes).
                        </Typography>
                        <Typography variant="body1">
                            - Personal de fábricas de pólvora y explosivos.
                        </Typography>
                        <Typography variant="body1">
                            - Productos químicos, producción, manipulación de clorhídrico, nítrico, clahídrico, álcalis, arsénico, etc.
                        </Typography>
                    </Grid>

                    <Grid item xs={12} className={`${classes.flexCenter} ${stepsClasses.mt20}`} >
                        <Grid item xs={12} md={6} className={`${classes.flexCenter}`}>
                            <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={() => handleClose("here")}>Cerrar</Button>
                        </Grid>
                    </Grid>

                </ModalComponent>

                <ModalComponent open={openPayments} onClose={() => handleClose("payments")}>
                    <Grid container direction="row" justify="center">
                        <Grid item xs={12} md={12} className={classes.center}>
                            <img alt="" src={GOALS_A[goal]} />
                        </Grid>

                        <Grid item xs={12} md={12} className={`${stepsClasses.mt15} ${classes.center}`}>
                            <Typography variant="h1" >Pensando en tu comodidad,</Typography>
                            <Typography variant="h1" >diferiremos tu cuota de ingreso en</Typography>
                            <Typography variant="h1" >XX pagos, así:</Typography>
                        </Grid>

                        {/* <Grid item xs={12} md={12} className={`${stepsClasses.mt10} ${classes.center}`}>
                            <Typography variant="h3" color="primary">XX pagos así:</Typography>
                        </Grid> */}

                        <Grid item xs={12} md={12}
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center",
                                marginTop: "15px"
                            }}
                        >
                            <Typography className={`${classes.bold}`}>1. X%: </Typography>
                            <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                        </Grid>

                        <Grid item xs={12} md={12}
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center",
                                marginTop: "15px"
                            }}
                        >
                            <Typography className={`${classes.bold}`}>1. X%: </Typography>
                            <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                        </Grid>

                        <Grid item xs={12} md={12}
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center",
                                marginTop: "15px"
                            }}
                        >
                            <Typography className={`${classes.bold}`}>1. X%: </Typography>
                            <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                        </Grid>

                        <Grid item xs={12} md={12}
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center",
                                marginTop: "15px"
                            }}
                        >
                            <Typography className={`${classes.bold}`}>1. X%: </Typography>
                            <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                        </Grid>

                        <Grid item xs={12} md={12}
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center",
                                marginTop: "15px"
                            }}
                        >
                            <Typography className={`${classes.bold}`}>1. X%: </Typography>
                            <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                        </Grid>

                        <Grid item xs={12} sm={12} md={12} className={classes.center}>
                            <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton} ${stepsClasses.mt30}`} onClick={() => handleClose("payments")}>Cerrar</Button>
                        </Grid>
                    </Grid>
                </ModalComponent>

                <Grid item xs={12} md={7}>

                    <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                        <LinearWithValueLabel value={50} />
                    </Grid>

                    <Grid item xs={12} className={`${stepsClasses.mt35} ${stepsClasses.mb20}`}>
                        <Typography variant="h3" className={`${typographiesClasses.h2BlackBold} ${stepsClasses.mt30}`}>Paso 2</Typography>
                        <Typography variant="h1">Simula tu Plan</Typography>
                        <Typography variant="body1">Tu meta es nuestra prioridad ¡Todos somos un equipo aquí! Así se ve tu plan de compra programada:</Typography>
                    </Grid>

                    <Grid item xs={12} sm={12} className={`${stepsClasses.mt35} ${stepsClasses.showMobile}`}>
                        <Card className={classes.root}>
                            <CardContent>
                                <Grid item xs={12} sm={12}>
                                    <Typography variant="h6" className={typographiesClasses.h1Bold}>Sucriptor Polen</Typography>
                                </Grid>
                                <Divider />
                                <Grid item xs={12} sm={12} >
                                    <Typography> {`${Names} ${LastNames}`} </Typography>
                                    <Typography> {email}</Typography>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>

                    <Grid item xs={12} className={stepsClasses.mt35} >
                        <Card className={classes.root}>
                            <CardContent>
                                <Typography variant="h2" className={`${typographiesClasses.h1Black} ${stepsClasses.mb15}`}>Resumen de tu plan</Typography>
                                <Divider />
                                <Grid container direction="row" className={stepsClasses.mt15}>
                                    <Grid item xs={12} md={1} sm={1}>
                                        <Checkbox defaultChecked disabled className={classes.Checkbox} />
                                    </Grid>
                                    <Grid item xs={12} md={4} sm={4} className={`${classes.flex}`}>
                                        <img src={GOALS_A[goal]} width={"50px"} alt='class' />
                                        <Typography variant="body1" className={`${typographiesClasses.h1Bold} ${classes.textCenter}`}><b>{LABEL_A[goal]}</b></Typography>
                                        {/* <Typography variant="body1" className={typographiesClasses.h1Bold}><b>Remodelación</b></Typography> */}
                                    </Grid>
                                    <Grid item xs={12} md={4} sm={4}>
                                        <Typography variant="body1" className={`${typographiesClasses.h114Black} ${classes.textStart}`}><b>PLAN VEHÍCULO 1</b></Typography>
                                        <Typography variant="body1" className={`${typographiesClasses.h114Black} ${classes.textStart}`}><b>$50.000.000</b></Typography>
                                        <Typography variant="body1" className={`${classes.textStart}`}>X 24 Meses de plazo</Typography>
                                    </Grid>
                                    <Grid item xs={12} md={3} sm={3}>

                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={stepsClasses.mt5}>
                                    <Grid item xs={12} md={1} sm={1}>

                                    </Grid>
                                    <Grid item xs={12} md={4} sm={4}>

                                    </Grid>
                                    <Grid item xs={12} md={4} sm={4} style={{ borderWidth: 0, borderStyle: "solid" }}>
                                        <Typography variant="body1" className={`${classes.ml10}`}>Cuota de ingreso total {"\n"} (se diferirá en <span style={{ color: "#fe5b02", cursor: "pointer" }} onClick={() => { setOpenPayments(true) }}>XX pagos</span>)</Typography>
                                    </Grid>
                                    <Grid item xs={12} md={3} sm={3}>
                                        <Typography variant="body1" className={`${typographiesClasses.body1Bold} ${classes.textEnd}`}>$1.000.000</Typography>
                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={stepsClasses.mt5}>
                                    <Grid item xs={12} md={1} sm={1}>

                                    </Grid>
                                    <Grid item xs={12} md={4} sm={4}>

                                    </Grid>
                                    <Grid item xs={12} md={4} sm={4}>
                                        <Typography variant="body1" className={`${classes.textStart} ${classes.ml10}`}>1er Pago Cuota de ingreso</Typography>
                                    </Grid>
                                    <Grid item xs={12} md={3} sm={3}>
                                        <Typography variant="body1" className={`${typographiesClasses.body1Bold} ${classes.textEnd}`}>$1.000.000</Typography>
                                    </Grid>
                                </Grid>

                                <Grid container direction="row" className={stepsClasses.mt5}>
                                    <Grid item xs={12} md={1} sm={1}>

                                    </Grid>
                                    <Grid item xs={12} md={4} sm={4}>

                                    </Grid>
                                    <Grid item xs={12} md={4} sm={4}>
                                        <Typography variant="body1" className={`${classes.textStart} ${classes.ml10}`}>Cuota mensual (sin seguro)</Typography>
                                    </Grid>
                                    <Grid item xs={12} md={3} sm={3}>
                                        <Typography variant="body1" className={`${typographiesClasses.body1Bold} ${classes.textEnd}`}>$668.606</Typography>
                                    </Grid>
                                </Grid>



                            </CardContent>
                        </Card>
                    </Grid>


                    <Grid item xs={12} className={stepsClasses.mt35}>
                        <Card className={classes.root}>
                            <CardContent>

                                <Typography variant="h2" className={`${typographiesClasses.h1Black} ${stepsClasses.mb15}`}>Plan de seguro</Typography>
                                <Divider />
                                <Grid container direction="row" className={`${stepsClasses.mt5} ${stepsClasses.mb10}`}>
                                    <Typography variant="body1" className={`${classes.ml10}`}>
                                        Para ver las exclusiones y el inicio de la vigencia del seguro de vida has click <span style={{ color: "#fe5b02", cursor: "pointer", textDecoration: "underline" }} onClick={() => { setOpenHere(true) }}>aquí.</span>
                                    </Typography>
                                </Grid>
                                <Grid container direction="row" className={`${stepsClasses.mt5} ${stepsClasses.mb10}`}>
                                    <Grid item xs={2}>
                                        <Grid container direction="row">

                                        </Grid>
                                        <Grid container direction="row">
                                            <OrangeCheckbox checked={isCheckedInsurance} onChange={handleChangeCheckBox} className={classes.Checkbox} color="primary" />
                                        </Grid>
                                        <Grid container direction="row">

                                        </Grid>
                                    </Grid>
                                    <Grid item xs={10}>

                                        <Grid container direction="row">
                                            <Grid item xs={12} md={6} sm={6}>
                                                <Typography variant="body1" className={`${typographiesClasses.body1Bold}`}>Tipo de Seguro:</Typography>
                                            </Grid>
                                            <Grid item xs={12} md={6} sm={6}>
                                                <Typography variant="body1" className={`${classes.textEnd}`}>Seguro de vida</Typography>
                                            </Grid>
                                        </Grid>

                                        <Grid container direction="row">
                                            <Grid item xs={12} md={6} sm={6}>
                                                <Typography variant="body1" className={`${typographiesClasses.body1Bold}`}>Aseguradora</Typography>
                                            </Grid>
                                            <Grid item xs={12} md={6} sm={6}>
                                                <Typography variant="body1" className={`${classes.textEnd}`}>MAFRE</Typography>
                                            </Grid>
                                        </Grid>

                                        <Grid container direction="row">
                                            <Grid item xs={12} md={6} sm={6}>
                                                <Typography variant="body1" className={`${typographiesClasses.body1Bold}`}>Valor Cuota de Seguro Mes:</Typography>
                                            </Grid>
                                            <Grid item xs={12} md={6} sm={6}>
                                                <Typography variant="body1" className={`${classes.textEnd}`}>$250.000</Typography>
                                            </Grid>
                                        </Grid>

                                    </Grid>
                                </Grid>

                            </CardContent>
                        </Card>

                    </Grid>

                    <Grid item xs={12} className={stepsClasses.mt35}>
                        <Card className={classes.root}>
                            <CardContent>

                                <Grid container direction="row" className={stepsClasses.mt15}>
                                    <Grid item xs={12} md={7} sm={6}>
                                        <Typography variant="body1" className={`${typographiesClasses.body124Bold} ${classes.textCenter2}`}>Total Pago Inicial</Typography>
                                    </Grid>
                                    <Grid item xs={12} md={5} sm={6}>
                                        <Typography variant="body1" className={`${typographiesClasses.body124Bold} ${classes.textCenter3}`}>$2.050.000</Typography>
                                    </Grid>
                                </Grid>

                                <Grid item xs={12} className={classes.boxBtnNext}>
                                    <Button variant="contained" onClick={nextStep} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} >Siguiente</Button>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>

                <Grid item xs={4} md={5} className={stepsClasses.modelLeft}>
                    <Grid container direction="row" className={`${stepsClasses.mt90}`}>
                        <Card className={classes.root}>
                            <CardContent>
                                <Grid item xs={12} sm={12}>
                                    <Typography variant="h2" color={"inherit"} className={typographiesClasses.h1Bold}>Suscriptor Polen</Typography>
                                </Grid>
                                <Divider />
                                <Grid item >
                                    <Typography>{`${Names} ${LastNames}`}</Typography>
                                    <Typography >{email}</Typography>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid container direction="row" style={{ border: "0px solid red", marginTop: "8rem" }}>
                        <ModelImageComponent src={srcImg} width={90}>
                            <Typography variant="h2" className={typographiesClasses.h1Black}> {Names}, éste es el resumen de tu plan.</Typography>
                        </ModelImageComponent>
                    </Grid>
                </Grid>
            </Grid>
        </Fade>
    )
}
