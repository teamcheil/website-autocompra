import React from 'react';
import Step21 from './step21';
import Step22 from './step22';
import Step23 from './step23';
import Step24 from './step24';
import { Grid } from '@material-ui/core';



export default ({ nextStep }) => {
    
    
    

    const next = (e) =>{
        nextStep(e);
    }

    return (
            
            <Grid container direction={"row"}>
                <Grid item md={12}>
                    <Step21 nextStep={next} />
                </Grid>

                 <Grid item md={12}>
                    <Step22 nextStep={next} />
                </Grid>

                <Grid item md={12}>
                    <Step23 nextStep={next} />
                </Grid>

                <Grid item md={12}>
                    <Step24 nextStep={next} />
                </Grid>
    
             </Grid>
  
    )
}