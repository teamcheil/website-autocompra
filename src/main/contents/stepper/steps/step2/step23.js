import React, {useRef, useState} from 'react';
import { Grid, Typography, Button, Fade } from '@material-ui/core';


import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import srcImg from "../../../../../assets/images/model_step1_3.png";
import LinearWithValueLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { ModelImageComponent } from '../../../../../components/ModelImage';
import { VideoPlayer } from '../../../../../components/videoPlayer';



export default ({ nextStep }) => {

    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const typographiesClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);
    const refVideo = useRef(null);
    const src = "https://media.w3.org/2010/05/sintel/trailer_hd.mp4";

    
    const onPlay = (currentTime) =>{
        const currentTimeControl = 2.5;
       (currentTime > currentTimeControl) ? setFormValid(true) :  setFormValid(false);
    };


    return(
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>
                <Grid item xs={12} md={7}>
                    <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                        <LinearWithValueLabel value={45}  />
                    </Grid>
                    <Grid item xs={12} className={`${stepsClasses.mt35}`}>
                        <Typography variant="h3" className={`${stepsClasses.mt30}`}>Paso 2</Typography>
                        <Typography variant="h1">Simula tu Plan</Typography>
                        <Typography variant="body1">Ya elegiste cómo quieres invertir en tu futuro con Polen. Ahora, te contamos  con más detalle el proceso para que lleguemos a tu meta.</Typography>
                    </Grid>

                    <Grid item xs={12} md={12} className={`${stepsClasses.mt35}`}>
                        <Typography variant="body1" className={`${typographiesClasses.body116} ${stepsClasses.mb15}`}>Aquí conocerás:</Typography>
                        <Typography variant="body1" className={`${stepsClasses.mb20} ${stepsClasses.row2}`}><div style={{border: "1px solid red", backgroundColor: "#fe5b02", width: "5px", height: "5px", borderRadius: 11, marginRight: 5}}></div>Los mecanismos de adjudicación de tu meta.</Typography>
                        <Typography variant="body1" className={`${stepsClasses.mb20} ${stepsClasses.row2}`}><div style={{border: "1px solid red", backgroundColor: "#fe5b02", width: "5px", height: "5px", borderRadius: 11, marginRight: 5}}></div>La forma de realizar el pago de tus cuotas mensuales</Typography>
                        <Typography variant="body1" className={`${stepsClasses.mb20} ${stepsClasses.row2}`}><div style={{border: "1px solid red", backgroundColor: "#fe5b02", width: "5px", height: "5px", borderRadius: 11, marginRight: 5}}></div>La política de devolución del dinero </Typography>
                    </Grid>

                    <Grid item xs={12} className={`${stepsClasses.withLinearProgress}`}>
                        <VideoPlayer src={src} onPlay={onPlay} />
                    </Grid>

                    <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                        <Button variant="contained" onClick={nextStep} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Siguiente</Button>
                    </Grid>
                </Grid>

                <Grid item xs={4} md={5} className={stepsClasses.modelLeft}>
                    <ModelImageComponent src={srcImg} align="center" width={90}>
                        <Typography variant="h1" className={typographiesClasses.h1Black}><span className={typographiesClasses.h1Black}>¡Con Polen, tú tienes el poder de elegir!</span></Typography>
                    </ModelImageComponent>
                </Grid>
            </Grid>
        </Fade>
    )
}
