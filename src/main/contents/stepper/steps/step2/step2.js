import React, { useEffect, useState } from 'react';
import { Fade } from '@material-ui/core';
import LoadingScreen from 'react-loading-screen';
import LogoLoading from '../../../../../assets/images/logo.svg';


export default ({ nextStep }) => {
    const [state, setstate] = useState(true);

    useEffect(()=>{
        const timer = setTimeout(() => {
            setstate(false);
        }, 3000);
        return () => clearTimeout(timer);
    }, []);

    useEffect(() => {
        const timer = setTimeout(() => {
            setstate(false);
            nextStep();
        }, 3000);
        return () => clearTimeout(timer);
    });

    return (
        <Fade in={true} timeout={300}>
                <LoadingScreen
                    loading={state}
                    bgColor='#f2f2f2'
                    spinnerColor='rgba(254, 91, 2, 0.2)'
                    textColor='#676767'
                    logoSrc={LogoLoading}
                    text='Cargando simulador'
                >
                    <>
                    </>
                </LoadingScreen>         
        </Fade>
    );
}
