import React, { useState, useEffect } from 'react';
import {
    Button,
    Grid,
    Typography,
    FormControlLabel,
    RadioGroup,
    Radio,
    Divider,
    Slider,
    Tooltip,
    Card,
    CardContent,
    CardMedia,
    withStyles,
    CircularProgress,
    makeStyles,
    Fade,
    useMediaQuery,
    Link,
    Checkbox
} from '@material-ui/core';

//styles
import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import srcImg from "../../../../../assets/images/model_step1_42.png";
import srcImg2 from "../../../../../assets/images/carA.svg";

//components
import LinearProgressWithLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { ModelImageComponent } from '../../../../../components/ModelImage';

import { ImageGoal } from '../../../../../components/ImageGoals';

import { useDispatch, useSelector } from 'react-redux';
import { setGoal } from '../../../../../redux/actions/stepOneActions';
import { CarouselPlanCard } from '../../../../../components/planCard';
import { setPlain } from '../../../../../redux/actions/stepTwoActions';
import { ModalComponent } from '../../../../../components/Modal';
import { GOALS_A } from '../../../../../components/ImageGoals/ImagesGoals';

import car from "../../../../../assets/images/car.png";
import travel from "../../../../../assets/images/travel.png";
import motorcycle from "../../../../../assets/images/motorcycle.png";
import phone from "../../../../../assets/images/phone.png";
import house from "../../../../../assets/images/house.png";

import leftArrow from "../../../../../assets/images/flecha-izquierda.svg";
import rightArrow from "../../../../../assets/images/flecha-derecha.svg";
import close from "../../../../../assets/images/close.svg";
import iconWarning from "../../../../../assets/images/iconWarning.png";

import Carousel from 'react-material-ui-carousel';


const OrangeCheckbox = withStyles({
    root: {
        color: '#fe5b02',
        '&$checked': {
            color: '#fe5b02',
        },
        padding: '0px'
    },
    checked: {},
})((props) => <Checkbox color='default' {...props} />);

const HtmlTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: '#fff',
        color: 'rgba(254,91,2,1)',
        maxWidth: 220,
        fontFamily: 'BeVietnam-Bold',
        fontSize: '17px'
    },
}))(Tooltip);

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: 'rgba(242, 242, 242, 1)',
        marginBottom: '50px',
        borderRadius: '4px',
        boxShadow: 'none'
    },
    wrapper: {
        position: 'relative',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
    },
    fabProgress: {
        color: 'primary',
        position: 'absolute',
        top: -6,
        left: -6,
        zIndex: 1,
    },
    buttonProgress: {
        color: 'primary',
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    slide: {
        '& .MuiSlider-thumb': {
            width: '18px',
            height: '18px',
            boxShadow: '0 4px 7px 0 rgba(0, 0, 0, 0.25)',
            backgroundColor: 'rgba(254,91,2,1)',
            border: '5px solid #ffffff'
        },
        '& .MuiSlider-rail': {
            height: '8px'
        },
        '& .MuiSlider-track': {
            height: '8px',
            borderRadius: '100px'
        },
        '& .MuiSlider-mark': {
            width: '0px'
        }
    },
    center: {
        textAlign: 'center'
    },
    sliderMobileLabel: {
        width: "100%",
        textAlign: "center",
        color: theme.palette.primary.main,
        visibility: 'hidden',
        [theme.breakpoints.down('sm')]: {
            visibility: 'visible'
        },
    },
    label: {
        '& .MuiFormControlLabel-label': {
            fontWeight: "bold",
            fontSize: 19
        }
    },
    Carousel: {
        borderWidth: 0,
        borderColor: "red",
        borderStyle: "solid",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        [theme.breakpoints.down('sm')]: {
            '& >div:nth-child(2)': {
                height: '50% !important',
                top: '25% !important',
                '& .MuiButtonBase-root': {
                    padding: '5px',
                    right: '-15px'
                }
            },
            '& >div:nth-child(3)': {
                height: '100% !important',
                '& .MuiButtonBase-root': {
                    padding: '5px',
                    left: '-15px'
                }
            },
        },
        ['@media (max-width:359px)']: { // eslint-disable-line no-useless-computed-key
            '& >div:nth-child(1)': {
                maxWidth: '340px',
                width: '95%',
                margin: 'auto',
            },
            '& >div:nth-child(2)': {
                '& .MuiButtonBase-root': {
                    right: '-25px'
                }
            },
            '& >div:nth-child(3)': {
                '& .MuiButtonBase-root': {
                    left: '-25px'
                }
            },
        },
    }
}));



export default ({ nextStep }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { goal, Names } = useSelector(state => state.stepOne);
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const typographiesClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);
    const [valueValid, setValueValid] = useState(false);
    const [showPlans, setshowPlans] = useState(false);
    const [loading, setLoading] = React.useState(false);
    const [open, setOpen] = useState(false);
    const isSmallScreen = useMediaQuery(theme => theme.breakpoints.down("sm"));
    const [formValues, setFormValues] = useState({
        savingType: '',
        savingValue: 0
    });

    const { savingType, savingValue } = formValues;

    const scrollRef = React.createRef();
    const [scrollConstant, setScrollConstant] = useState(419);

    const [isChecked, setIsChecked] = useState(false);
    const [idPlain, setIdPlain] = useState(false);

    useEffect(() => {
    });

    const marksOpt1 = [
        {
            value: 10000000,
            label: '$10M',
        },
        {
            value: 20000000,
            label: '$20M',
        },
    ];
    const marksOpt2 = [
        {
            value: 300000,
            label: '$300.000',
        },
        {
            value: 1000000,
            label: '$1M',
        },
    ];

    function ValueLabelComponent(props) {
        const { children, open, value } = props;
        const NewNumber = Intl.NumberFormat(["ban", "id"]).format(value);
        return (
            <HtmlTooltip open={open && !isSmallScreen} enterTouchDelay={0} placement="top" title={`$${NewNumber}`}>
                {children}
            </HtmlTooltip>
        );
    }

    const handleRadioChange = ({ target }) => {
        setValueValid(false);
        setshowPlans(false);
        setFormValues({
            ...formValues,
            savingValue: target.value == "val1" ? 10000000 : 300000,
            [target.name]: target.value
        })
    }

    const handleSliderChange = (event, newValue) => {
        setValueValid(true);
        setFormValues({
            ...formValues,
            savingValue: newValue
        })
    }

    const onClickGoal = (e) => {
        dispatch(setGoal(e.target.id));
        setshowPlans(false);
        setFormValues({
            ...formValues,
            savingType: ''
        })
    };

    const handleCalculatePlans = (e) => {
        setValueValid(false);
        setLoading(true);
        const timer = setTimeout(() => {
            setshowPlans(true);
            setLoading(false);
            setValueValid(true);
        }, 1000);
        return () => clearTimeout(timer);
    }

    // const handleCheckBoxChange = ({ target }) =>{
    //     setIdPlain(target.value);
    //     setIsChecked(target.checked);
    //     const simulations = items.filter((item) =>( item.planTypeID == target.value ));
    //     onClick(obj);
    // }

    const onViewDetails = (item, i) => {

        const obj = simulations;

        obj.map((itm) => {
            itm["isSelected"] = false;
        })
        obj[i]["isSelected"] = true;

        setSimulations(obj)

        setIdPlain(item.planID);
        setFormValid(true);
        dispatch(setPlain(savingType, item.goalValue, item.planID, item.planName, item.planSpan, item.initialPayment, item.monthlyFee));
    };

    const handleClickPlain = (obj) => {
        const { IdPlan, Nombre, Meses, PagoInicial, Cuota } = obj[0];
        setFormValid(true);
        dispatch(setPlain(savingType, savingValue, IdPlan, Nombre, Meses, PagoInicial, Cuota));
    }

    const Items = [
        { IdPlan: 1, Nombre: 'Plan Vehículo 1', Meses: '24', PagoInicial: '1000000', Cuota: '500000' },
        { IdPlan: 2, Nombre: 'Plan Vehículo 2', Meses: '36', PagoInicial: '700000', Cuota: '200000' },
        { IdPlan: 3, Nombre: 'Plan Vehículo 3', Meses: '18', PagoInicial: '5000000', Cuota: '800000' },
        { IdPlan: 4, Nombre: 'Plan Vehículo 4', Meses: '48', PagoInicial: '7000000', Cuota: '900000' },
        { IdPlan: 5, Nombre: 'Plan Vehículo 5', Meses: '62', PagoInicial: '5000000', Cuota: '100000' },
    ]

    const [simulations, setSimulations] = useState([
        {
            planID: 1,
            planTypeID: 1,
            planName: "Plan vehículo 1",
            planSpan: "24",
            initialPayment: "100",
            goalValue: "30000000",
            monthlyFee: "500000",
            simulationStageID: 9,
            simulationStage: "Estado CRM"
        },
        {
            planID: 2,
            planTypeID: 1,
            planName: "Plan vehículo 2",
            planSpan: "24",
            initialPayment: "100",
            goalValue: "30000000",
            monthlyFee: "1000000",
            simulationStageID: 9,
            simulationStage: "Estado CRM"
        },
        {
            planID: 3,
            planTypeID: 3,
            planName: "Plan moto 3",
            planSpan: "36",
            initialPayment: "100",
            goalValue: "60000000",
            monthlyFee: "500000",
            simulationStageID: 9,
            simulationStage: "Estado CRM"
        },
        {
            planID: 4,
            planTypeID: 1,
            planName: "Plan vehículo 4",
            planSpan: "24",
            initialPayment: "100",
            goalValue: "30000000",
            monthlyFee: "500000",
            simulationStageID: 9,
            simulationStage: "Estado CRM"
        },
        {
            planID: 5,
            planTypeID: 1,
            planName: "Plan vehículo 5",
            planSpan: "24",
            initialPayment: "100",
            goalValue: "30000000",
            monthlyFee: "1000000",
            simulationStageID: 9,
            simulationStage: "Estado CRM"
        },
        {
            planID: 6,
            planTypeID: 3,
            planName: "Plan moto 1",
            planSpan: "36",
            initialPayment: "100",
            goalValue: "60000000",
            monthlyFee: "500000",
            simulationStageID: 9,
            simulationStage: "Estado CRM"
        }
    ]);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <>
            {/* <ModalComponent open={open} onClose={handleClose}>
                    <Grid container direction="row" justify="center">
                        <Grid item  xs={12} md={12} className={classes.center}>
                            <img alt="" src={GOALS_A[goal]} />
                        </Grid>

                        <Grid item  xs={12} md={12} className={classes.center}>
                            <Typography variant="h2" color="inherit">Plan vehiculo 1</Typography>
                        </Grid>

                        <Grid item  xs={12} md={12} className={classes.center}>
                            <Typography variant="body1" color="primary">24 meses</Typography>
                        </Grid>

                        <Grid item  xs={12} md={12}>
                            <Typography className={`${stepsClasses.mt30}`}>Lorem ipsum dolor sit amet consectetur adipiscing elit vivamus, cubilia pharetra imperdiet sollicitudin natoque </Typography>
                        </Grid>

                        <Grid item  xs={12} md={12}>
                            <Typography variant="h5" color="inherit" className={`${stepsClasses.mt30}`}>Descripcion</Typography>
                            <Typography>Lorem ipsum dolor sit amet consectetur adipiscing elit vivamus, cubilia pharetra imperdiet sollicitudin natoque Lorem ipsum dolor sit amet consectetur adipiscing elit vivamus, cubilia pharetra imperdiet sollicitudin natoque </Typography>
                        </Grid>

                        <Grid item xs={12} sm={12} md={12} className={classes.center}>
                            <Button color='primary' className={`${buttonsClasses.button} ${buttonsClasses.orangeButton} ${stepsClasses.mt30}`} onClick={handleClose}>Cerrar</Button>
                        </Grid>
                    </Grid>    
                </ModalComponent> */}

            <ModalComponent open={open} onClose={handleClose}>
                <Grid container direction="row" justify="center">
                    <Grid item xs={12} md={12} className={classes.center}>
                        <img alt="" src={GOALS_A[goal]} />
                    </Grid>

                    <Grid item xs={12} md={12} className={`${stepsClasses.mt15} ${classes.center}`}>
                        <Typography variant="h1" >Descripción del bien</Typography>
                    </Grid>

                    <Grid item xs={12} md={12} className={classes.center}>
                        <Typography variant="h2" color="primary">24 meses</Typography>
                    </Grid>

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            marginTop: "15px"
                        }}
                    >
                        <Typography>Valor de meta:</Typography>
                        <div className={`${stepsClasses.row}`}><Typography>$</Typography><Typography color="primary">000000000</Typography></div>
                    </Grid>

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between"
                        }}
                    >
                        <Typography>% cuota de ingreso:</Typography>
                        <div className={`${stepsClasses.row}`}><Typography color="primary">000</Typography><Typography>%</Typography></div>
                    </Grid>

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between"
                        }}
                    >
                        <Typography>Valor de cuota de ingreso:</Typography>
                        <div className={`${stepsClasses.row}`}><Typography>$</Typography><Typography color="primary">000000000</Typography></div>
                    </Grid>

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between"
                        }}
                    >
                        <Typography>% cuota de administración:</Typography>
                        <div className={`${stepsClasses.row}`}><Typography color="primary">000</Typography><Typography>%</Typography></div>
                    </Grid>

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between"
                        }}
                    >
                        <Typography>Valor de cuota mensual:</Typography>
                        <div className={`${stepsClasses.row}`}><Typography>$</Typography><Typography color="primary">000000000</Typography></div>
                    </Grid>

                    <Grid item xs={12} sm={12} md={12} className={classes.center}>
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton} ${stepsClasses.mt30}`} onClick={handleClose}>Cerrar</Button>
                    </Grid>
                </Grid>
            </ModalComponent>

            <Fade in={true} timeout={900}>
                <Grid container direction={"row"}>
                    <Grid item xs={12} md={7}>
                        <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                            <LinearProgressWithLabel value={40} />
                        </Grid>
                        <Grid item xs={12} className={`${stepsClasses.mt35}`}>
                            <Typography variant="h3" className={`${typographiesClasses.h1BlackBold} ${stepsClasses.mt30}`}>Paso 2</Typography>
                            <Typography variant="h1">Simula tu Plan</Typography>
                            <Typography variant="body1">Hay varias posibilidades para que unidos lleguemos a tu meta. En Polen, tú eliges el plan de compra programada que más te guste.</Typography>
                        </Grid>
                        <Grid item xs={12} md={12} className={`${stepsClasses.mt30}`}>
                            <Card className={classes.root}>
                                <CardContent>
                                    <Typography variant="h2" className={`${typographiesClasses.h2BlackBold} ${stepsClasses.mb15}`}>Meta</Typography>
                                    <Typography variant="body1" className={`${stepsClasses.mb20} ${typographiesClasses.h2BlackBold}`}>Ya elegiste tu meta desde el primer paso. Si deseas, puedes cambiarla ahora para realizar tu simulación</Typography>
                                    <ImageGoal onClick={onClickGoal} />
                                </CardContent>
                            </Card>
                        </Grid>

                        <Grid item xs={12}>
                            <Typography variant="h2" className={`${typographiesClasses.h2BlackBold} ${stepsClasses.mb15}`}>Selecciona Valor de tu meta o Valor de tu cuota mensual</Typography>
                            <Typography variant="body1">Puedes realizar la simulación según el valor de la meta que deseas alcanzar o el valor de la cuota mensual que se ajusta a tu flujo mensual, seleccionando el monto de tu preferencia:</Typography>
                        </Grid>

                        <Grid item xs={12} className={`${stepsClasses.mt35}`}>
                            <RadioGroup aria-label="savingType" name="savingType" value={savingType} onChange={handleRadioChange}>
                                <FormControlLabel value="val1" className={`${classes.label}`} control={<Radio color={"primary"} />} label="Valor de tu meta" />
                                {savingType === 'val1' &&
                                    <Grid className={`${stepsClasses.mt30} ${stepsClasses.mb15}`} item xs={12} md={12}>
                                        <Typography variant="body1" className={`${classes.sliderMobileLabel}`}>${savingValue}</Typography>
                                        <Slider
                                            className={classes.slide}
                                            value={savingValue}
                                            defaultValue={10000000}
                                            //ValueLabelComponent={ValueLabelComponent}
                                            step={500000}
                                            min={10000000}
                                            max={20000000}
                                            valueLabelDisplay="off"
                                            marks={marksOpt1}
                                            onChange={handleSliderChange}
                                        />
                                    </Grid>
                                }

                                <FormControlLabel value="val2" className={`${classes.label}`} control={<Radio color={"primary"} />} label="Valor de tu cuota mensual" />
                                {savingType === 'val2' &&
                                    <Grid className={`${stepsClasses.mt30}  ${stepsClasses.mb15} ${stepsClasses.boxLinearProgress}`} item xs={12} md={12}>
                                        <Typography variant="body1" className={`${classes.sliderMobileLabel}`}>${savingValue}</Typography>
                                        <Slider
                                            className={classes.slide}
                                            value={savingValue}
                                            defaultValue={300000}
                                            //ValueLabelComponent={ValueLabelComponent}
                                            step={10000}
                                            min={300000}
                                            max={1000000}
                                            valueLabelDisplay="off"
                                            marks={marksOpt2}
                                            onChange={handleSliderChange}
                                        />
                                    </Grid>
                                }
                            </RadioGroup>
                        </Grid>
                        <Grid item xs={12} md={12}>
                            <Divider />
                        </Grid>
                        <Grid item xs={12} md={12} className={stepsClasses.boxBtnNext}>
                            <div className={classes.wrapper}>
                                <Button variant="contained" onClick={handleCalculatePlans} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={valueValid === false} >Obtener planes</Button>
                                {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
                            </div>
                        </Grid>
                        <Grid item xs={12} md={12} className={`${stepsClasses.mt30} ${stepsClasses.mb20}`}>
                            {/*
                                showPlans &&
                                <Grid container direction="row" style={{ marginBottom: "30px" }}>

                                    <Grid item xs={12} md={12} >
                                        <Typography variant="h2" className={typographiesClasses.h1Bold}>{savingType === 'val1' ? "Planes según el valor de tu meta " : "Planes según valor cuota"}</Typography>
                                        <Typography variant="body1">Estos son nuestros planes disponibles. Escoge el de tu preferencia, continúa el proceso y haz parte de la comunidad Polen.</Typography>
                                    </Grid>

                                    <Grid item xs={12} md={12} className={`${stepsClasses.mt30}`} >
                                        <Carousel
                                            navButtonsAlwaysVisible={true}
                                            indicators={false}
                                            animation="fade"
                                            autoPlay={false}
                                            className={classes.Carousel}>
                                            {
                                                simulations.map((item, index) => {

                                                    let icon
                                                    switch (item.planTypeID) {
                                                        case 1:
                                                            icon = car
                                                            break;
                                                        case 2:
                                                            icon = travel
                                                            break;
                                                        case 3:
                                                            icon = motorcycle
                                                            break;
                                                        case 4:
                                                            icon = phone
                                                            break;
                                                        case 5:
                                                            icon = house
                                                            break;
                                                    }
                                                    return (
                                                        <Grid
                                                            key={index}
                                                            className={item.isSelected ? stepsClasses.simulationCardSelected2 : stepsClasses.simulationCard2}
                                                            onClick={() => {
                                                                //onCardSelection(index)
                                                                onViewDetails(item, index)
                                                            }}
                                                        >
                                                            <Grid container direction={"row"} className={classes.container} style={{ marginBottom: "7px" }}>
                                                                <Grid item style={{ marginRight: "31px" }}>
                                                                    <OrangeCheckbox checked={item.isSelected ? item.isSelected : false} name={`idPlan_${item.planTypeID}`} value={item.planTypeID} onClick={() => { onViewDetails(item, index) }}></OrangeCheckbox>
                                                                </Grid>
                                                                <Link onClick={() => { handleOpen() }}>
                                                                    <Typography variant="body1" color="primary">Ver Detalle</Typography>
                                                                </Link>
                                                            </Grid>
                                                            <img alt='' src={icon} className={stepsClasses.simulationCardIcon} />
                                                            <Typography className={stepsClasses.simulationCardPlanName}>
                                                                {item.planName}
                                                            </Typography>
                                                            <Typography>
                                                                {item.planSpan} meses
                                                        </Typography>
                                                            <Typography className={stepsClasses.simulationCardLabels}>
                                                                Valor meta
                                                        </Typography>
                                                            <Typography className={stepsClasses.simulationCardValues}>
                                                                ${item.goalValue}
                                                            </Typography>
                                                            <Typography className={stepsClasses.simulationCardLabels}>
                                                                Valor cuota mensual
                                                        </Typography>
                                                            <Typography className={stepsClasses.simulationCardValues}>
                                                                ${item.monthlyFee}
                                                            </Typography>
                                                        </Grid>
                                                    )

                                                })
                                            }

                                        </Carousel>
                                    </Grid>

                                </Grid>
                                        */}
                            {
                                showPlans &&
                                <Grid className={stepsClasses.simulationsContainerWrapper}>

                                    <Grid
                                        className={stepsClasses.arrowsContainer}
                                        onClick={() => {
                                            console.log("REF: ", scrollRef);
                                            scrollRef.current.scrollTo((scrollRef.current.scrollLeft - scrollConstant), 0)
                                        }}
                                    >
                                        <img alt='' src={leftArrow} />
                                    </Grid>
                                    <Grid ref={scrollRef} className={stepsClasses.simulationsContainer}>
                                        {
                                            simulations.map((item, index) => {
                                                let icon
                                                switch (item.planTypeID) {
                                                    case 1:
                                                        icon = car
                                                        break;
                                                    case 2:
                                                        icon = travel
                                                        break;
                                                    case 3:
                                                        icon = motorcycle
                                                        break;
                                                    case 4:
                                                        icon = phone
                                                        break;
                                                    case 5:
                                                        icon = house
                                                        break;
                                                }
                                                return (
                                                    <Grid
                                                        key={index}
                                                        className={item.isSelected ? stepsClasses.simulationCardSelected2 : stepsClasses.simulationCard2}
                                                        onClick={() => {
                                                            //onCardSelection(index)
                                                            onViewDetails(item, index)
                                                        }}
                                                    >
                                                        <Grid container direction={"row"} className={classes.container} style={{ marginBottom: "7px" }}>
                                                            <Grid item style={{ marginRight: "11px" }}>
                                                                <OrangeCheckbox checked={item.isSelected ? item.isSelected : false} name={`idPlan_${item.planTypeID}`} value={item.planTypeID} onClick={() => { onViewDetails(item, index) }}></OrangeCheckbox>
                                                            </Grid>
                                                            <Link onClick={() => { handleOpen() }}>
                                                                <Typography variant="body1" color="primary">Ver Detalle</Typography>
                                                            </Link>
                                                        </Grid>
                                                        <img alt='' src={icon} className={stepsClasses.simulationCardIcon} />
                                                        <Typography className={stepsClasses.simulationCardPlanName}>
                                                            {item.planName}
                                                        </Typography>
                                                        <Typography>
                                                            {item.planSpan} meses
                                                        </Typography>
                                                        <Typography className={stepsClasses.simulationCardLabels}>
                                                            Valor meta
                                                        </Typography>
                                                        <Typography className={stepsClasses.simulationCardValues}>
                                                            ${item.goalValue}
                                                        </Typography>
                                                        <Typography className={stepsClasses.simulationCardLabels}>
                                                            Valor cuota mensual
                                                        </Typography>
                                                        <Typography className={stepsClasses.simulationCardValues}>
                                                            ${item.monthlyFee}
                                                        </Typography>
                                                    </Grid>
                                                )
                                            })
                                        }
                                    </Grid>
                                    <Grid
                                        className={stepsClasses.arrowsContainer}
                                        onClick={() => {
                                            console.log("REF: ", scrollRef);
                                            scrollRef.current.scrollTo((scrollRef.current.scrollLeft + scrollConstant), 0)
                                        }}
                                    >
                                        <img alt='' src={rightArrow} />
                                    </Grid>
                                </Grid>
                            }
                        </Grid>
                        <Grid item xs={12} md={12} className={stepsClasses.boxBtnNext}>
                            <Button variant="contained" onClick={nextStep} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Siguiente</Button>
                        </Grid>

                    </Grid>
                    <Grid item xs={4} md={5} className={stepsClasses.modelLeft}>
                        <ModelImageComponent src={srcImg} align="center" width={130}>
                            <Typography variant="h2" className={typographiesClasses.h1Black}>{`${Names} ¡Vamos a ver tu plan de compra programada!`}</Typography>
                        </ModelImageComponent>
                    </Grid>
                </Grid>
            </Fade>
        </>
    )
}
