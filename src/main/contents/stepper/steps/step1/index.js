import React  from 'react';
import { Container, Grid, Zoom} from '@material-ui/core';
import Step1 from './step1';
import Step11 from './step1.1';
import Step12 from './step1.2';
import Step13 from './step1.3';
import Step14 from './step1.4';
//import { useDispatch } from 'react-redux';
//import { setPageStep } from '../../../../../redux/actions/stepperActions';



export default ({ nextStep, page }) =>{


    const next = (e) =>{
        nextStep(e);
        console.log(page);
    }

    return (

        <Container>
            <Grid container direction={"row"}>
                <Grid item md={12}>
                    {page === 1 && <Step1 nextStep={next} /> }
                </Grid>

                <Zoom in={page === 2} timeout={900}>
                    <Grid item md={12}> 
                         {page === 2 &&  <Step11 nextStep={next} /> }
                    </Grid>
               </Zoom>     
               

                <Zoom in={page === 3} timeout={900}>
                    <Grid item md={12}>
                         {page === 3 &&  <Step12 nextStep={next} /> }
                    </Grid>
                </Zoom>

                <Zoom in={page === 4} timeout={900}>
                    <Grid item md={12}>
                         {page === 4 &&  <Step13 nextStep={next} /> }
                    </Grid>
                </Zoom>

                <Zoom in={page === 5} timeout={900}>
                    <Grid item md={12}>
                    {page === 5 &&  <Step14 nextStep={next} /> }
                    </Grid>
                </Zoom>

            </Grid>
        </Container>
    )

}
