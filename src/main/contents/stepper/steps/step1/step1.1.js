import React, { useEffect } from 'react';
import { Grid, Typography, Fade } from '@material-ui/core';
import ModelImage from '../../../../../assets/images/model-name.jpg';
// Styles
import { typographiesStyles } from '../../../../../styles/theme/components/typographies';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import {ModelImageComponent} from "../../../../../components/ModelImage";

export default ({ nextStep }) => {
    const tClasses = typographiesStyles();
    const stepsClasses = stepsStyles();

    useEffect(() => {
        const timer = setTimeout(() => {
            nextStep();
        }, 300000);
        return () => clearTimeout(timer);
    });

    return (
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>
                <Grid item xs={12} className={stepsClasses.modelCenter}>
                    <ModelImageComponent src={ModelImage} align={'right'}>
                        <Typography variant="h1"><span className={tClasses.h1Black}>Hola soy Juli</span>X</Typography>
                        <Typography variant="body1" className={tClasses.body118}>Estoy aqui para resolver todas tus dudas y encontrar la mejor opción para cumplir tu meta.</Typography>
                    </ModelImageComponent>
                </Grid>
            </Grid>
        </Fade>
    );
}
