import React, { useState, useEffect, useCallback } from 'react';
import { Button, Box, Grid, TextField, FormControlLabel, Typography, Link, Fade } from '@material-ui/core';
import ReCAPTCHA from "react-google-recaptcha";
import { useDispatch } from 'react-redux';
import { setFormRegister } from '../../../../../redux/actions/stepOneActions';
import { ModelImageComponent } from '../../../../../components/ModelImage';
import LinearProgressWithLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import validator from 'validator';
// Styles
import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import { inputStyles, OrangeCheckbox } from '../../../../../styles/theme/components/inputs';
import ModelImage from "../../../../../assets/images/model_step1_42.png";

export default ({ nextStep }) => {
    const dispatch = useDispatch();
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const inputsClasses = inputStyles();
    const tClasses = typographiesStyles();
    const InputProps = { className: inputsClasses.input, disableUnderline: true };
    const errorEmail = "Email invalido",
        errorEmailConfirmation = "El email no coincide",
        errorPhone = 'Celular invalido, Min. 10',
        errorPhoneConfirmation = "El celular no coincide";

    let captcha;

    const [formValid, setFormValid] = useState(false);
    const [formState, SetFormState] = useState({
        email: '',
        emailConf: '',
        phone: '',
        phoneConf: '',
        terms_conditions: false,
    });

    const { email, emailConf, phone, phoneConf, terms_conditions } = formState;

    const onChange = (event) => {
        console.log(event);
    };

    const onLoadRecaptcha = () => {
        if (captcha) {
            captcha.reset();
        }
    }

    const verifyCallback = (recaptchaToken) => {
        // Here you will get the final recaptchaToken!!!
        console.log(recaptchaToken, "<= your recaptcha token")
    }

    const isFormValid = useCallback(() => {
        let isValid = true;
        isValid = !validateEmail(email) ? isValid : false;
        isValid = !validateEmailConfirmation(emailConf) ? isValid : false;
        isValid = !validatePhone(phone) ? isValid : false;
        isValid = !validatePhoneConfirmation(phoneConf) ? isValid : false;
        if (!terms_conditions) {
            console.log('Term Conditions should true');
            isValid = false
        } else if (onChange()) {
            console.log('is not Human jajaj');
            // isValid = false
        }
        return isValid;
    }, [email, emailConf, phone, phoneConf, terms_conditions]);

    const validateEmail = (x) => {
        return x.trim().length > 0 && !validator.isEmail(email);
    };

    const validateEmailConfirmation = (x) => {
        return x.trim().length > 0 && x !== email;
    };

    const validatePhone = (x) => {
        return x.trim().length > 0 && x.trim().length < 10;
    };

    const validatePhoneConfirmation = (x) => {
        return x.trim().length > 0 && x !== phone;
    };

    useEffect(() => {
        (isFormValid()) ? setFormValid(true) : setFormValid(false);
    }, [isFormValid]);


    const handleInputChange = ({ target }) => {
        SetFormState({
            ...formState,
            [target.name]: target.value
        });
    };

    const handleInputChangePhone = ({ target }) => {
        if (target.value.length <= 10 && (Math.sign(target.value) != -1 || target.value == 0)) SetFormState({
            ...formState,
            [target.name]: target.value
        });
    };

    const handleCheckBoxChange = ({ target }) => {
        SetFormState({
            ...formState,
            [target.name]: target.checked
        });
    };

    const handleRegister = (e) => {
        e.preventDefault();
        dispatch(setFormRegister(email, phone, terms_conditions));
        nextStep();
    };

    return (
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>
                <Grid item xs={12} md={7}>
                    <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                        <LinearProgressWithLabel value={30} />
                    </Grid>
                    <Grid item xs={12} className={`${stepsClasses.withLinearProgress} ${stepsClasses.mb20}`}>
                        <Typography variant="h3">Paso 1</Typography>
                        <Typography variant="h1">Conozcámonos</Typography>
                        <Typography variant="body1">Para estar más cerca de ti y llevarte a la meta que quieres, es importante conocerte mejor. </Typography>
                        <div className={`${stepsClasses.mt35}`}>
                            <Typography variant="body1" className={`${tClasses.body116}`}>Ingresa la siguiente información:</Typography>
                        </div>
                        <Grid item xs={12} className={stepsClasses.mt35}>

                            <form onSubmit={handleRegister} noValidate={true} autoComplete="off">
                                <TextField
                                    name="email"
                                    value={email}
                                    onChange={handleInputChange}
                                    label="Email:"
                                    placeholder="Escribe tu email"
                                    focused={false}
                                    fullWidth
                                    InputProps={InputProps}
                                    className={inputsClasses.inputContainer}
                                    InputLabelProps={{
                                        shrink: true,
                                        className: inputsClasses.label,
                                    }}
                                    error={validateEmail(email)}
                                    helperText={validateEmail(email) ? errorEmail : ''}
                                />
                                <TextField
                                    name="emailConf"
                                    value={emailConf}
                                    onChange={handleInputChange}
                                    label="Confirma tu Email:"
                                    placeholder="Confirma tu Email"
                                    focused={false}
                                    fullWidth
                                    InputProps={InputProps}
                                    className={inputsClasses.inputContainer}
                                    InputLabelProps={{
                                        shrink: true,
                                        className: inputsClasses.label,
                                    }}
                                    error={validateEmailConfirmation(emailConf)}
                                    helperText={validateEmailConfirmation(emailConf) ? errorEmailConfirmation : ''}
                                />
                                <TextField
                                    name="phone"
                                    value={phone}
                                    onChange={handleInputChangePhone}
                                    label="Celular:"
                                    placeholder="Escribe tu celular"
                                    focused={false}
                                    fullWidth
                                    type="number"
                                    InputProps={InputProps}
                                    className={inputsClasses.inputContainer}
                                    InputLabelProps={{
                                        shrink: true,
                                        className: inputsClasses.label,
                                    }}
                                    error={validatePhone(phone)}
                                    helperText={validatePhone(phone) ? errorPhone : ''}
                                />
                                <TextField
                                    name='phoneConf'
                                    value={phoneConf}
                                    onChange={handleInputChangePhone}
                                    label='Confirma tu Celular'
                                    placeholder='Confirma tu Celular'
                                    focused={false}
                                    fullWidth
                                    type="number"
                                    InputProps={InputProps}
                                    className={inputsClasses.inputContainer}
                                    InputLabelProps={{
                                        shrink: true,
                                        className: inputsClasses.label,
                                    }}
                                    error={validatePhoneConfirmation(phoneConf)}
                                    helperText={validatePhoneConfirmation(phoneConf) ? errorPhoneConfirmation : ''}
                                />
                                <Grid item xs={12} className={`${stepsClasses.mt15} ${stepsClasses.ckBoxInline}`}>
                                    <FormControlLabel
                                        name='terms_conditions'
                                        checked={terms_conditions}
                                        onChange={handleCheckBoxChange}
                                        control={<OrangeCheckbox color="default" />}
                                        label=""
                                        labelPlacement="end"
                                    />
                                    <Typography variant={"body1"} className={tClasses.body112}>
                                        He leído y acepto la <Link target="_blank" href="https://media.polen.com.co/documentos/Terminos_Condiciones.pdf">Autorización para Consulta y Verificación de Información</Link> y las <Link target="_blank" href="https://media.polen.com.co/documentos/Politicas_Tratamiento_Datos.pdf">Políticas de Tratamiento de Datos Personales y Datos Sensibles</Link> de Polen.
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} className={`${stepsClasses.mt15} ${stepsClasses.captcha}`}>
                                    <Box sitekey={{ backgroundColor: 'green', width: '200px' }}>
                                        <ReCAPTCHA
                                            ref={ (el) => { captcha = el; } }
                                            size="invisible"
                                            render="explicit"
                                            sitekey="6Le7jb4ZAAAAAEZTCc69RPNihoL7e8wLRd-DivEK"
                                            onloadCallback={onLoadRecaptcha}
                                            verifyCallback={verifyCallback}
                                            onChange={onChange}
                                        />
                                    </Box>
                                </Grid>
                                <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                                    <Button variant="contained" type={'submit'} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Registrarse</Button>
                                </Grid>
                            </form>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={5} className={stepsClasses.modelLeft}>
                    <ModelImageComponent src={ModelImage} align="center" width={130}>
                        <Typography variant="h1" className={tClasses.h1Black2}>{'¡Estás a un paso de descubrir la comunidad en la que alcanzarás tus metas!'}</Typography>
                    </ModelImageComponent>
                </Grid>
            </Grid>
        </Fade>
    )
}
