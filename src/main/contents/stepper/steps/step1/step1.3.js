import React, {useState, useEffect, useRef} from 'react';
import { Button, Grid, Typography, Fade } from '@material-ui/core';
import { useSelector } from 'react-redux';
import LinearProgressWithLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { VideoPlayer } from '../../../../../components/videoPlayer';
import { ModelImageComponent } from '../../../../../components/ModelImage';
// Styles
import { buttonsStyles } from '../../../../../styles/theme/components/buttons'
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps'
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import ModelImage from "../../../../../assets/images/model_step1_3.png";

export default ({ nextStep }) => {
    const { Names } = useSelector(state => state.stepOne)
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const tClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);
    const refVideo = useRef(null);
    const src = "https://media.w3.org/2010/05/sintel/trailer_hd.mp4";

    useEffect(()=>{
        onPlay();
    }, []);

    const onPlay = (currentTime) =>{
        const currentTimeControl = 2.5;
       (currentTime > currentTimeControl) ? setFormValid(true) :  setFormValid(false);
    };

    const onStop = () =>{

    };

    return (
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>
                <Grid item xs={12} md={7}>
                    <Grid item xs={12} className={`${stepsClasses.boxLineargitProgress} ${stepsClasses.showDesktop}`}>
                        <LinearProgressWithLabel value={25} />
                    </Grid>
                    <Grid item xs={12} className={stepsClasses.withLinearProgress}>
                        <Typography variant="h3">Paso 1</Typography>
                        <Typography variant="h1">Conozcámonos</Typography>
                        <Typography variant="body1">Ahora descubrirás por qué Polen es la alternativa para llegar lejos en comunidad.</Typography>
                        <Grid item xs={12} className={stepsClasses.mt35}>
                            {/* <Typography variant="body1" className={tClasses.body116}>Agradecemos que confíes en nosotros para cumplir tus metas. Ahora te explicamos por qué somos la mejor opción.</Typography> */}
                            <VideoPlayer src={src} onPlay={onPlay} onStop={onStop} />
                        </Grid>
                        <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                            <Button variant="contained" onClick={nextStep} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Siguiente</Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={5} className={stepsClasses.modelLeft}>
                    <ModelImageComponent src={ModelImage} align={'center'} width={70}>
                        <Typography variant="h1" className={ tClasses.h1Black }>{Names} bienvenido a Polen!</Typography>
                    </ModelImageComponent>
                </Grid>
            </Grid>
        </Fade>
    )
}
