import React, {useEffect, useState,} from 'react';
import { Button, Grid, Typography, Fade } from '@material-ui/core';
import { ImageGoal } from '../../../../../components/ImageGoals';

// Styles
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from '../../../../../styles/theme/components/typographies';
import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { useSelector } from 'react-redux';

export default ({ nextStep }) => {
    const stepsClasses = stepsStyles();
    const typographiesClasses = typographiesStyles();
    const buttonsClasses = buttonsStyles();
    const { goal } = useSelector(state => state.stepOne);
    const [goalActive, setGoalActive]  = useState(goal || false);

    useEffect(() => {
        setGoalActive(goal || false);
    },[ goal]);

    const onClickGoal = ({ target }) =>{
        setGoalActive(true);
    };

    return (
        <Fade in={true} timeout={700}>
            <Grid container direction={"row"} className={stepsClasses.nonLinearProgress}>
                <Typography variant="h1" className={`${typographiesClasses.h132}`}>¡Hola!</Typography>
                <Typography variant="h2" className={`${stepsClasses.mb90}`}>En Polen, unidos llegamos a donde queremos. 
                <br />Cuéntanos, ¿cuál es esa meta que te mueve?</Typography>
                <Typography variant="body1" className={`${typographiesClasses.body116}`}>Selecciona una de las siguientes opciones</Typography>
                <Grid item xs={12} md={12}>
                    <ImageGoal onClick={onClickGoal} />
                </Grid>
                <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                    <Button variant="contained" onClick={nextStep} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={goalActive === false} >Siguiente</Button>
                </Grid>
            </Grid>
        </Fade>
    );
}
