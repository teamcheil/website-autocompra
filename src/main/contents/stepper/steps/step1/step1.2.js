import React, {useState, useEffect, useCallback} from 'react';
import { Button, Grid, TextField, Typography, Fade } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { setNamesLastNames } from '../../../../../redux/actions/stepOneActions';
import { ModelImageComponent } from '../../../../../components/ModelImage';
import LinearProgressWithLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
// Styles
import { buttonsStyles } from '../../../../../styles/theme/components/buttons'
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps'
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import { inputStyles } from '../../../../../styles/theme/components/inputs';
import ModelImage from "../../../../../assets/images/model_step1_22.png";
import ModelImage1 from '../../../../../assets/images/model-name.png';
import validator from "validator";


export default ({ nextStep }) => {
    const dispatch = useDispatch();
    const {Names, LastNames} = useSelector(state => state.stepOne);
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const inputsClasses = inputStyles();
    const tClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);
    const [next, setNext] = useState(false);
    const [values, setValues] = useState({
        names: Names || '',
        lastnames: LastNames || ''
    });

    const { names , lastnames} = values;
  
    const InputProps = {
        className: inputsClasses.input,
        disableUnderline: true,
    };

    useEffect(()=>{
        const timer = setTimeout(() => {
            setNext(true);
        }, 3000);
        return () => clearTimeout(timer);
    });

    useEffect(() =>{
        validaTeForm();
    });

    const validaTeForm = () => {
        if(names.trim().length > 0 && lastnames.trim().length > 0){
            setFormValid(true);
        }else{
            setFormValid(false);
        }
    };

    const onClick = () => {
        dispatch( setNamesLastNames(names, lastnames) );
        nextStep();
    };

    const handleInputChange = ({target}) => {
        setValues({
            ...values,
            [target.name]: target.value
        });
    };

    return (
        <>
            { next &&
                <Fade in={true} timeout={900}>
                    <Grid container direction={"row"}>
                        <Grid item xs={12} md={7}>
                            <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                                <LinearProgressWithLabel value={20} />
                            </Grid>
                            <Grid item xs={12} className={stepsClasses.withLinearProgress}>
                                <Typography variant="h3">Paso 1</Typography>
                                <Typography variant="h1">Conozcámonos</Typography>
                                <Typography variant="body1">¡Somos Polen y es un gusto tenerte aquí!<br/>Cuéntanos, ¿cómo te llamas?</Typography>
                                <Grid item xs={12} className={stepsClasses.mt35}>
                                    <Typography variant="body1" className={tClasses.body116}>Ingresa tus datos</Typography>
                                    <Grid item xs={12} className={stepsClasses.mt15}>
                                        <TextField
                                            name="names"
                                            value={names}
                                            onChange={handleInputChange}
                                            label="Nombres:"
                                            focused={false}
                                            placeholder="Escribe tus nombres"
                                            fullWidth
                                            InputProps={InputProps}
                                            className={inputsClasses.inputContainer}
                                            InputLabelProps={{
                                                shrink: true,
                                                className: inputsClasses.label
                                            }}
                                        />
                                        <TextField
                                            name="lastnames"
                                            value={lastnames}
                                            onChange={handleInputChange}
                                            label="Apellidos:"
                                            focused={false}
                                            placeholder="Escribe tus apellidos"
                                            fullWidth
                                            InputProps={InputProps}
                                            className={inputsClasses.inputContainer}
                                            InputLabelProps={{
                                                shrink: true,
                                                className: inputsClasses.label,
                                            }}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                                    <Button variant="contained" onClick={onClick} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Siguiente</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={5} className={stepsClasses.modelLeft}>
                                <ModelImageComponent src={ModelImage} width={60}>
                                    <Typography variant="h1" className={tClasses.h1Black}>¡En Polen queremos llamarte por tu nombre!</Typography>
                                    {/* <Typography variant="body1" className={tClasses.boby116}>¿Cuál es tu nombre y apellido?</Typography> */}
                                </ModelImageComponent>
                        </Grid>
                    </Grid>
                </Fade>
            }
            { !next && 
                 <Fade in={true} timeout={900}>
                    <Grid container direction={"row"}>
                        <Grid item xs={12} className={stepsClasses.modelCenter}>
                            <Typography variant="h1" className={`${tClasses.h133}`}>¡En Polen, tu meta es nuestra meta!</Typography>
                            <ModelImageComponent src={ModelImage1} align={'right'}>
                                <Typography variant="h1"><span className={tClasses.h1Black}>Hola soy Juli</span></Typography>
                                <Typography variant="body1" className={tClasses.body118}>Hoy vamos a llegar a donde quieres, porque unidos llegamos más lejos.</Typography>
                            </ModelImageComponent>
                        </Grid>
                    </Grid>
             </Fade>

            }
        </>
    )
}
