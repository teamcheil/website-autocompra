import React, { useState, useEffect, useCallback } from 'react';
import { Button, Grid, Checkbox, makeStyles, withStyles, Typography, Box, Fade, FormControlLabel, Link } from '@material-ui/core';
import LinearProgressWithLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import { inputStyles } from '../../../../../styles/theme/components/inputs';
import iconFile from '../../../../../assets/images/iconFile.png';
import iconDelete from '../../../../../assets/images/iconDelete.png';
import ModelImage from "../../../../../assets/images/model_step1_42.png";
import { ModalComponent } from '../../../../../components/Modal';
import iconLetter from '../../../../../assets/images/iconLetter.png';
import { setFormDocuments } from '../../../../../redux/actions/stepFourActions';
import { ModelImageComponent } from "../../../../../components/ModelImage";
import { MyCustomDropzoneComponent } from "../../../../../components/DropzoneInput/myCustomDropzone";
import { useDispatch, useSelector } from 'react-redux';
import Dropzone from 'react-dropzone-uploader';
import { getDroppedOrSelectedFiles } from 'html5-file-selector'
import { render } from "@testing-library/react";
const ReactDOMServer = require('react-dom/server');
const djsConfig = { autoProcessQueue: false };

const OrangeCheckbox = withStyles({
    root: {
        color: '#fe5b02',
        '&$checked': {
            color: '#fe5b02',
        },
    },
    checked: {},
})((props) => <Checkbox color='default' {...props} />);

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '400px',
        backgroundColor: 'rgba(242, 242, 242, 1)',
        marginBottom: '50px',
        borderRadius: '4px',
        boxShadow: 'none'
    },
    container: {
        display: 'flex'
    },
    boxBtnNext: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: '30px'
    },
    textEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    textCenter: {
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    },
    Checkbox: {
        padding: '0px'
    },
    chekR: {
        marginRight: '.8rem',
        textAlign: 'center'
    },
    center: {
        textAlign: 'center'
    },
    left: {
        marginLeft: '.5rem'
    },
    colorMain:{
        color: theme.palette.primary.main
    }
}));

export default ({ nextStep }) => {
    const { Names } = useSelector(state => state.stepOne)
    const dispatch = useDispatch();
    const classes = useStyles();
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const inputClasses = inputStyles();
    const tClasses = typographiesStyles();
    const [open, setOpen] = useState(false);
    const [formValid, setFormValid] = useState(false);
    const [formState, SetFormState] = useState({
        frontCC: false,
        backCC: false,
        photoFace: false,
        terms_conditions: false,
    });

    const { frontCC, backCC, photoFace, terms_conditions } = formState;

    const isFormValid = useCallback(() => {
        if (!frontCC) return false;
        if (!backCC) return false;
        if (!terms_conditions) return false;
        return true;
    }, [frontCC, backCC, terms_conditions]);

    useEffect(() => {
        (isFormValid()) ? setFormValid(true) : setFormValid(false);
    }, [isFormValid]);


    const handleOpen = (e) => {
        e.preventDefault();
        dispatch(setFormDocuments(frontCC, backCC, photoFace, terms_conditions));
        setOpen(true);
    };

    const handleRegister = (e) => {
        e.preventDefault();
        console.log(formState);
        nextStep();
    };

    const handleCheckBoxChange = ({ target }) => {
        console.log(target.name)
        console.log(target)
        console.log(target.checked)
        SetFormState({
            ...formState,
            [target.name]: target.checked
        });
        isFormValid();
    };

    const loadFile = (files, position) => {
        console.log('loadFile => ', files[0], 'position =>', position);
        SetFormState({
            ...formState,
            [position]: files
        });
    };

    const deleteFile = (position) => {
        SetFormState({
            ...formState,
            [position]: false
        })
    };

    const getUploadParams = ({ meta }) => { return { url: 'https://httpbin.org/post' } };

    // called every time a file's `status` changes
    const handleChangeStatus = (meta, file, status, position) => {
        console.log(status, position);
        if (status == 'done') {
            console.log(status, meta, file);
            SetFormState({
                ...formState,
                [position]: { meta, file }
            })
        }
    };

    // receives array of files that are done uploading when submit button is clicked
    const handleSubmit = (files, allFiles) => {
        console.log(files.map(f => f.meta));
        allFiles.forEach(f => f.remove());
    };

    const getFilesFromEvent = e => {
        return new Promise(resolve => {
            getDroppedOrSelectedFiles(e).then(chosenFiles => {
                resolve(chosenFiles.map(f => f.fileObject))
            })
        })
    }

    const Preview = ({ meta }) => {
        const { name, percent, status } = meta
        return (

            <Grid container direction="row" alignItems="center" className={stepsClasses.dropzonePrev}>
                <Grid item xs={2} className={`${'image'}`}><img alt='iconFile' src={iconFile} /></Grid>
                <Grid item xs={8} className={`${'content'}`}>
                    <Typography variant="body1" className={`${'title'}`}>{`Foto${name}.jpg`}</Typography>
                    <Box></Box>
                    <Typography variant="body1" className={`${'percent'}`}>{Math.round(percent)}%</Typography>
                </Grid>
                <Grid item xs={2} className={`${'delete'}`}>
                    <Button onClick={() => deleteFile('frontCC')}>
                        <img alt='Model img' src={iconDelete} />
                    </Button>
                </Grid>
            </Grid>
        )
    }


    return (
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>
                <ModalComponent open={open} onClose={handleRegister}>
                    <Grid item xs={12} className={classes.center}>
                        <img width="96.2" height="94.9" alt="" src={iconLetter} />
                    </Grid>
                    {/* <Grid item xs={12} className={`${classes.center} ${stepsClasses.mt20}`} >
                        <Typography variant="h1" color="inherit">¡Envio exitoso!</Typography>
                    </Grid> */}
                    <Grid item xs={12} className={`${classes.center} ${tClasses.body118} ${stepsClasses.mt10}`} >
                        <Typography variant="body1">Tus documentos fueron enviados correctamente.</Typography>
                    </Grid>
                    <Grid item xs={12} className={`${classes.center} ${stepsClasses.mt20}`} >
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton} ${classes.left}`} onClick={handleRegister}>Siguiente</Button>
                    </Grid>
                </ModalComponent>
                <Grid item xs={12} md={7}>
                    <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                        <LinearProgressWithLabel value={85} />
                    </Grid>
                    <Grid item xs={12} className={stepsClasses.withLinearProgress}>
                        <Typography variant="h3">Paso 4</Typography>
                        <Typography variant="h1">Chequeo de identidad e información bancaria</Typography>
                        <Typography variant="body1">Para mayor seguridad sube una foto de la parte frontal y otra del respaldo de tu cédula de ciudadanía. Adicionalmente, te invitamos a subir una foto en la que se vea tu rostro claramente (Ver Términos y Condiciones de Polen para el Tratamiento de Imágenes y Uso de Datos Sensibles).</Typography>
                        <form onSubmit={handleOpen} noValidate={true} autoComplete="off">
                            <Grid item md={12} xs={12}>
                                <Typography variant="h1" className={`${tClasses.h1Black18} ${stepsClasses.mt35}`}>Frente del documento<span className={`${classes.colorMain}`}> *</span></Typography>
                                <Typography variant="body1">Por favor sube la parte frontal de tu cédula de Ciudadanía.</Typography>
                                {
                                    !formState.frontCC || formState.frontCC.length === 0 ?
                                        <Dropzone
                                            PreviewComponent={Preview}
                                            accept="image/*"
                                            getUploadParams={getUploadParams}
                                            InputComponent={MyCustomDropzoneComponent}
                                            multiple={false}
                                            getFilesFromEvent={getFilesFromEvent}
                                            maxFiles={1}
                                            maxSizeBytes={25165824}
                                            onChangeStatus={({ meta, file }, status) => handleChangeStatus(meta, file, status, 'frontCC')}
                                        >
                                            <div>
                                                Try dropping some files here, or click to select files to upload.
                                                <Typography>TEST</Typography>
                                            </div>
                                        </Dropzone>
                                        :
                                        <Grid container direction="row" alignItems="center" className={stepsClasses.dropzonePrev}>
                                            <Grid item xs={2} className={`${'image'}`}><img alt='iconFile' src={iconFile} /></Grid>
                                            <Grid item xs={8} className={`${'content'}`}>
                                                <Typography variant="body1" className={`${'title'}`}>{`Foto${formState.frontCC.file.name}.jpg`}</Typography>
                                                <Box></Box>
                                                <Typography variant="body1" className={`${'percent'}`}>100%</Typography>
                                            </Grid>
                                            <Grid item xs={2} className={`${'delete'}`}>
                                                <Button onClick={() => deleteFile('frontCC')}>
                                                    <img alt='Model img' src={iconDelete} />
                                                </Button>
                                            </Grid>
                                        </Grid>
                                }
                            </Grid>
                            <Grid item md={12} xs={12}>
                                <Typography variant="h1" className={`${tClasses.h1Black18} ${stepsClasses.mt15}`}>Respaldo del documento<span className={`${classes.colorMain}`}> *</span></Typography>
                                <Typography variant="body1">Por favor sube el respaldo de tu cédula de Ciudadanía</Typography>
                                {
                                    !formState.backCC || formState.backCC.length === 0 ?
                                        <Dropzone
                                            PreviewComponent={Preview}
                                            accept="image/*"
                                            getUploadParams={getUploadParams}
                                            InputComponent={MyCustomDropzoneComponent}
                                            multiple={false}
                                            getFilesFromEvent={getFilesFromEvent}
                                            maxFiles={1}
                                            maxSizeBytes={25165824}
                                            onChangeStatus={({ meta, file }, status) => handleChangeStatus(meta, file, status, 'backCC')}
                                        />
                                        :
                                        <Grid container direction="row" alignItems="center" className={stepsClasses.dropzonePrev}>
                                            <Grid item xs={2} className={`${'image'}`}><img alt='iconFile' src={iconFile} /></Grid>
                                            <Grid item xs={8} className={`${'content'}`}>
                                                <Typography variant="body1" className={`${'title'}`}>{`Foto${formState.backCC.file.name}.jpg`}</Typography>
                                                <Box></Box>
                                                <Typography variant="body1" className={`${'percent'}`}>100%</Typography>
                                            </Grid>
                                            <Grid item xs={2} className={`${'delete'}`}>
                                                <Button onClick={() => deleteFile('backCC')}>
                                                    <img alt='Model img' src={iconDelete} />
                                                </Button>
                                            </Grid>
                                        </Grid>
                                }
                            </Grid>
                            <Grid item md={12} xs={12}>
                                <Typography variant="h1" className={`${tClasses.h1Black18} ${stepsClasses.mt15}`}>Foto (Opcional)</Typography>
                                <Typography variant="body1">Por favor sube una foto tuya donde tu cara se vea claramente.</Typography>
                                {
                                    !formState.photoFace || formState.photoFace.length === 0 ?
                                        <Dropzone
                                            PreviewComponent={Preview}
                                            accept="image/*"
                                            getUploadParams={getUploadParams}
                                            InputComponent={MyCustomDropzoneComponent}
                                            multiple={false}
                                            getFilesFromEvent={getFilesFromEvent}
                                            maxFiles={1}
                                            maxSizeBytes={25165824}
                                            onChangeStatus={({ meta, file }, status) => handleChangeStatus(meta, file, status, 'photoFace')}
                                        />
                                        :
                                        <Grid container direction="row" alignItems="center" className={stepsClasses.dropzonePrev}>
                                            <Grid item xs={2} className={`${'image'}`}><img alt='iconFile' src={iconFile} /></Grid>
                                            <Grid item xs={8} className={`${'content'}`}>
                                                <Typography variant="body1" className={`${'title'}`}>{`Foto${formState.photoFace.file.name}.jpg`}</Typography>
                                                <Box></Box>
                                                <Typography variant="body1" className={`${'percent'}`}>100%</Typography>
                                            </Grid>
                                            <Grid item xs={2} className={`${'delete'}`}>
                                                <Button onClick={() => deleteFile('photoFace')}>
                                                    <img alt='Model img' src={iconDelete} />
                                                </Button>
                                            </Grid>
                                        </Grid>
                                }
                            </Grid>
                            <Grid item xs={12} className={`${stepsClasses.mt15} ${stepsClasses.ckBoxInline}`}>
                                <FormControlLabel
                                    name='terms_conditions'
                                    checked={terms_conditions}
                                    onChange={handleCheckBoxChange}
                                    control={<OrangeCheckbox color="default" />}
                                    label=""
                                    labelPlacement="end"
                                />
                                <Typography variant={"body1"} className={tClasses.body112}>
                                    He leído y doy mi <Link target="_blank" href="https://media.polen.com.co/documentos/Terminos_Condiciones_Tratamiento_Imagenes.pdf">Autorización de uso de imagen y uso de datos sensibles.</Link>
                                </Typography>
                            </Grid>
                            <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                                <Button variant="contained" type={'submit'} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Enviar</Button>
                            </Grid>
                        </form>
                    </Grid>
                </Grid>
                <Grid item xs={5} className={stepsClasses.modelLeft}>
                    <ModelImageComponent src={ModelImage} align="center" width={100} >
                        <Typography variant="h1">{(Names + ",") || 'N/A,'}</Typography>
                        <Typography variant={"h1"} className={tClasses.h1Black}>queremos proteger tu meta</Typography>
                    </ModelImageComponent>
                </Grid>
            </Grid>
        </Fade>
    )
};
