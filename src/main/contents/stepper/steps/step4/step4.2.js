import React, { useState, useEffect, useCallback } from 'react';
import {
    Button, Grid, Typography, Radio, Box, RadioGroup, FormControlLabel,
    Select, MenuItem, TextField, Fade, Link, withStyles, Checkbox, makeStyles
} from '@material-ui/core';
import LinearWithValueLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import { inputStyles } from '../../../../../styles/theme/components/inputs';
import ModelImage2 from "../../../../../assets/images/model_step1_22.png";
import 'react-dropzone-uploader/dist/styles.css'
import { useSelector } from 'react-redux';
import { setFormDataAccount, setFormDataToken } from '../../../../../redux/actions/stepFourActions';
import { useDispatch } from 'react-redux';
import { ModelImageComponent } from "../../../../../components/ModelImage";
import { ModalComponent } from '../../../../../components/Modal';
import phoneAsterisk from '../../../../../assets/images/phoneAsterisk.svg';
import iconCheckOrange from '../../../../../assets/images/iconCheckOrange.svg';
// import ModelImage from "../../../../../assets/images/stepCongrats.png";
import ModelImage from "../../../../../assets/images/model_step1_42.png";

import SuportImg from '../../../../../assets/images/group-463@3x.png';

const OrangeCheckbox = withStyles({
    root: {
        color: '#fe5b02',
        '&$checked': {
            color: '#fe5b02',
        },
    },
    checked: {},
})((props) => <Checkbox color='default' {...props} />);

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '400px',
        backgroundColor: 'rgba(242, 242, 242, 1)',
        marginBottom: '50px',
        borderRadius: '4px',
        boxShadow: 'none'
    },
    container: {
        display: 'flex'
    },
    boxBtnNext: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: '30px'
    },
    textEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    textCenter: {
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    },
    Checkbox: {
        padding: '0px'
    },
    chekR: {
        marginRight: '.8rem',
        textAlign: 'center'
    },
    center: {
        textAlign: 'center'
    },
    left: {
        marginLeft: '.5rem'
    },
    centerModal:{
        textAlign: "center",
    },
    widthIcon: {
        width: '23%',
        marginBottom: "20px"
    },
}));

export default ({ nextStep }) => {
    const { Names } = useSelector(state => state.stepOne)
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const classes = useStyles();
    const inputsClasses = inputStyles();
    const tClasses = typographiesStyles();
    const dispatch = useDispatch();
    const [token, SetStatusToken] = useState(false);
    const [open, setOpen] = useState(false);
    const [open2, setOpen2] = useState(false);
    const [openToken, setOpenToken] = useState(false);

    const [openUnhappy, setOpenUnhappy] = useState(false);

    const { email, phone } = useSelector(state => state.stepOne)

    const [formState, SetFormState] = useState({
        agree: 'agree',
        banco: '',
        typeAccount: '',
        numberAccount: '',
        tokenInput: '',
        terms_conditions: false
    });
    const { agree, banco, typeAccount, numberAccount, tokenInput, terms_conditions } = formState;
    const [formValid, setFormValid] = useState(false);

    const isFormValid = useCallback(() => {
        if (!agree) return true;
        if (!banco && agree == "agree") return false;
        if (!typeAccount && agree == "agree") return false;
        if (!numberAccount && agree == "agree") return false;
        if (!terms_conditions && agree == "agree") return false;
        return true;
    }, [agree, banco, typeAccount, numberAccount, terms_conditions]);

    useEffect(() => {
        (isFormValid()) ? setFormValid(true) : setFormValid(false);
    }, [isFormValid]);

    const handleCheckBoxChange = ({ target }) => {
        SetFormState({
            ...formState,
            [target.name]: target.checked
        });
        isFormValid();
    };

    const handleRegister = (e) => {
        setOpen(false);
        e.preventDefault()
        dispatch(setFormDataAccount(agree, banco, typeAccount, numberAccount));
        SetStatusToken(true);
    }

    const handleSetToken = (e) => {
        e.preventDefault()
        if(tokenInput !== "camino no feliz"){
            dispatch(setFormDataToken(tokenInput));
            setOpen2(true)
        } else {
            setOpenUnhappy(true)
        }
    }

    const handleNextStep = (e) => {
        e.preventDefault()
        nextStep();
    }

    const changeRadioButtonAgreeOrNot = ({ target }) => {
        SetFormState({
            ...formState,
            agree: target.value
        })
    }

    const handleInputChange = ({ target }) => {
        SetFormState({
            ...formState,
            [target.name]: target.value
        });
    };

    const handleChange = (event) => {
        SetFormState({
            ...formState,
            [event.target.name]: event.target.value
        })
    };

    const handleOpen = (e) => {
        e.preventDefault();
        setOpen(true);
    };

    const handleCloseToken = (e) => {
        setOpenToken(false);
    };

    const handleCloseUnhappy = (e) => {
        setOpenUnhappy(false)
    };

    const handleSendTokenAgain = (e) => {
        setOpenToken(true)
        e.preventDefault()
    }

    return (
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>

                <ModalComponent open={open} onClose={handleRegister}>
                    <Grid item xs={12} className={classes.center}>
                        <img width="96.2" height="94.9" alt="" src={phoneAsterisk} />
                    </Grid>
                    <Grid item xs={12} className={`${classes.center} ${tClasses.body118} ${stepsClasses.mt10}`} >
                        <Typography variant="body1">Acabamos de enviar un <b>número de token</b> a tu <b>celular.</b> </Typography>
                    </Grid>
                    <Grid item xs={12} className={`${classes.center} ${stepsClasses.mt20}`} >
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton} ${classes.left}`} onClick={handleRegister}>Siguiente</Button>
                    </Grid>
                </ModalComponent>

                <ModalComponent open={open2} onClose={handleRegister}>
                    <Grid item xs={12} className={classes.center}>
                        <img width="96.2" height="94.9" alt="" src={iconCheckOrange} />
                    </Grid>

                    <Typography variant="h1" className={`${tClasses.h1Black18} ${classes.center} ${stepsClasses.mt35}`}>¡Validación exitosa!</Typography>

                    <Grid item xs={12} className={`${classes.center} ${tClasses.body118} ${stepsClasses.mt10}`} >
                        <Typography variant="body1">Tu código fue validado<br />exitosamente. </Typography>
                    </Grid>
                    <Grid item xs={12} className={`${classes.center} ${stepsClasses.mt20}`} >
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton} ${classes.left}`} onClick={handleNextStep}>Siguiente</Button>
                    </Grid>
                </ModalComponent>

                <ModalComponent open={openToken} onClose={handleCloseToken}>
                    <Grid item xs={12} md={12} className={classes.centerModal}>
                        <img alt="" src={SuportImg} className={classes.widthIcon} />
                    </Grid>

                    <Grid item xs={12} md={12} className={`${classes.top15} ${classes.top15}`}>
                        {/* <Typography variant="body1" className={`${classes.centerModal}`}><span style={{ fontWeight: 'bold' }}>¡Con Polen, la logras! Hemos enviado a tu email</span> la información de tu plan, con tu <span style={{ fontWeight: 'bold' }}>número de contrato, y un token a tu celular.</span></Typography> */}
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span style={{ fontWeight: 'normal' }}>Hemos enviado </span> nuevamente</Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span>el token a tu email:</span></Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span style={{ fontWeight: 'bold', color: "rgba(254,91,2,1)" }}>{email}</span></Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span>y a tu celular:</span></Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span style={{ fontWeight: 'bold', color: "rgba(254,91,2,1)" }}>{phone}</span></Typography>
                    </Grid>

                    <Grid item xs={12} sm={12} md={12} className={`${classes.centerModal} ${classes.top30}`} style={{marginTop: 11}}>
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={handleCloseToken}>Cerrar</Button>
                    </Grid>
                </ModalComponent>

                <ModalComponent open={openUnhappy} onClose={handleCloseUnhappy}>
                    <Grid item xs={12} md={12} className={classes.centerModal}>
                        <img alt="" src={SuportImg} className={classes.widthIcon} />
                    </Grid>

                    <Grid item xs={12} md={12} className={`${classes.top15} ${classes.top15}`}>
                        {/* <Typography variant="body1" className={`${classes.centerModal}`}><span style={{ fontWeight: 'bold' }}>¡Con Polen, la logras! Hemos enviado a tu email</span> la información de tu plan, con tu <span style={{ fontWeight: 'bold' }}>número de contrato, y un token a tu celular.</span></Typography> */}
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span style={{ fontWeight: 'normal' }}>Tu token ha expirado o es inválido</span></Typography>
                    </Grid>

                    <Grid item xs={12} sm={12} md={12} className={`${classes.centerModal} ${classes.top30}`} style={{marginTop: 11}}>
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={handleCloseUnhappy}>Cerrar</Button>
                    </Grid>
                </ModalComponent>

                {
                    !token ?
                        <Four21
                            handleOpen={handleOpen}
                            changeRadioButtonAgreeOrNot={changeRadioButtonAgreeOrNot}
                            handleCheckBoxChange={handleCheckBoxChange}
                            handleChange={handleChange}
                            handleInputChange={handleInputChange}
                            formValid={formValid}
                            agree={agree}
                            banco={banco}
                            typeAccount={typeAccount}
                            numberAccount={numberAccount}
                            terms_conditions={terms_conditions}
                            handleNextStep={handleNextStep}
                        />
                        : <Four22
                            handleSetToken={handleSetToken}
                            handleInputChange={handleInputChange}
                            tokenInput={tokenInput}
                            handleSendTokenAgain={handleSendTokenAgain}
                        />
                }

                {!token ? <Grid item xs={5} className={stepsClasses.modelLeft}>
                    <ModelImageComponent src={ModelImage} align="right">
                        <Typography variant="h1">{Names || 'N/A'}</Typography>
                        <Typography variant={"h1"} className={tClasses.h1Black}>tu seguridad es primero.</Typography>
                    </ModelImageComponent>
                </Grid>
                    :
                    <Grid item xs={5} className={stepsClasses.modelLeft}>
                        <ModelImageComponent src={ModelImage2} align="center" width={60} >
                            <Typography variant="h1">{Names || 'N/A'}</Typography>
                            <Typography variant={"h1"} className={tClasses.h1Black}>tu seguridad es primero.</Typography>
                        </ModelImageComponent>
                    </Grid>
                }
            </Grid>
        </Fade>
    )
}


const Four21 = ({ handleOpen, changeRadioButtonAgreeOrNot,
    handleChange, handleInputChange, formValid, agree, banco, typeAccount, numberAccount,
    terms_conditions, handleCheckBoxChange, handleNextStep
}) => {
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const inputsClasses = inputStyles();
    const tClasses = typographiesStyles();
    const InputProps = {
        className: inputsClasses.input,
        disableUnderline: true,
    };

    const { frontCC, backCC, photoFace } = useSelector(state => state.stepFour);

    return <Grid item xs={12} md={7}>
        <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
            <LinearWithValueLabel value={90} />
        </Grid>
        <Grid item xs={12} className={stepsClasses.withLinearProgress}>
            <Typography variant="h3">Paso 4</Typography>
            <Typography variant="h1">Información bancaria</Typography>
            <Typography variant="body1">Tu identidad ya fue confirmada. ¡Solo debes ingresar tus datos bancarios!</Typography>
            <Grid container direction="row" className={stepsClasses.prevImagesContainer}>
                <Box className={stepsClasses.prevImagesBox}>
                    <Typography className={tClasses.body111Bold}>Frente de la C.C</Typography>
                    <Box className={stepsClasses.prevImagesBackground}>
                        {frontCC ?
                            <img src={frontCC.meta.previewUrl} />
                            : null
                        }
                    </Box>
                </Box>
                <Box className={stepsClasses.prevImagesBox}>
                    <Typography className={tClasses.body111Bold}>Respaldo de la C.C</Typography>
                    <Box className={stepsClasses.prevImagesBackground}>
                        {backCC ?
                            <img src={backCC.meta.previewUrl} />
                            : null
                        }
                    </Box>
                </Box>
                <Box className={stepsClasses.prevImagesBox}>
                    <Typography className={tClasses.body111Bold}>Foto</Typography>
                    <Box className={stepsClasses.prevImagesBackground}>
                        {photoFace ?
                            <img src={photoFace.meta.previewUrl} />
                            : null
                        }
                    </Box>
                </Box>
            </Grid>


            <Grid container direction="row" className={stepsClasses.mt35}>
                <Typography variant="h1" className={`${tClasses.h1Black18} ${stepsClasses.mt35}`}>Ingreso de datos bancarios</Typography>
                <Typography variant="body1">Ingresa tus datos bancarios para las transferencias que eventualmente deba realizarte Polen.</Typography>
                <Grid item md={12} xs={12}>
                    <Typography variant="body1" className={tClasses.body116}>Banco:</Typography>
                    <Select
                        value={banco}
                        onChange={handleChange}
                        name='banco'
                        displayEmpty
                        InputProps={InputProps}
                        className={inputsClasses.selectContainer}
                        InputLabelProps={{
                            shrink: true,
                            className: inputsClasses.label
                        }}
                    >
                        <MenuItem value="" disabled> Selecciona tu banco </MenuItem>
                        <MenuItem value={'BBVA'}>BBVA</MenuItem>
                        <MenuItem value={'Bancolombia'}>Bancolombia</MenuItem>
                        <MenuItem value={'Davivienda'}>Davivienda</MenuItem>
                    </Select>
                </Grid>
                <Grid item md={12} xs={12}>
                    <Typography variant="body1" className={tClasses.body116}>Tipo de cuenta:</Typography>
                    <Select
                        value={typeAccount}
                        onChange={handleChange}
                        name='typeAccount'
                        displayEmpty
                        InputProps={InputProps}
                        className={inputsClasses.selectContainer}
                        InputLabelProps={{
                            shrink: true,
                            className: inputsClasses.label
                        }}
                    >
                        <MenuItem value="" disabled> Selecciona un tipo de cuenta </MenuItem>
                        <MenuItem value={'Ahorros'}>Ahorros</MenuItem>
                        <MenuItem value={'Corriente'}>Corriente</MenuItem>
                    </Select>
                </Grid>

                <Grid item md={12} xs={12}>
                    <TextField
                        name="numberAccount"
                        value={numberAccount}
                        onChange={handleInputChange}
                        label="Número de cuenta:"
                        focused={false}
                        placeholder="Escribe el número de tu cuenta"
                        fullWidth
                        displayEmpty
                        InputProps={InputProps}
                        className={inputsClasses.inputContainer}
                        InputLabelProps={{
                            shrink: true,
                            className: inputsClasses.label
                        }}
                    />
                </Grid>
            </Grid>

            <Typography variant="h1" className={`${tClasses.h1Black18} ${stepsClasses.mt35}`}>Autorización débito automático.</Typography>
            <Typography variant="body1">Elige la opción “autorizo” para activar el débito automático. ¡Más comodidad y seguridad para ti!</Typography>
            <form onSubmit={agree === "agree" ? handleOpen : handleNextStep} noValidate={true} autoComplete="off">
                <RadioGroup aria-label="agree" name="agree" value={agree} onChange={changeRadioButtonAgreeOrNot}>
                    <Grid container direction="row" >
                        <Grid item xs={6}>
                            <FormControlLabel value="agree" control={<Radio color={"primary"} />} label={<Typography variant={"body1"}>Autorizo</Typography>} />
                        </Grid>
                        <Grid item xs={6}>
                            <FormControlLabel value="noAgree" control={<Radio color={"primary"} />} label={<Typography variant={"body1"}>No Autorizo</Typography>} />
                        </Grid>
                    </Grid>
                </RadioGroup>

                {
                    agree === "agree" ?
                        <Grid item xs={12} className={`${stepsClasses.mt15} ${stepsClasses.ckBoxInline}`}>
                            <FormControlLabel
                                name='terms_conditions'
                                checked={terms_conditions}
                                onChange={handleCheckBoxChange}
                                control={<OrangeCheckbox color="default" />}
                                label=""
                                labelPlacement="end"
                            />
                            <Typography variant={"body1"} className={tClasses.body112}>
                                He leído y autorizo el débito automático aceptando los <Link href="https://media.polen.com.co/documentos/Terminos_Condiciones_Debito_Automatico.pdf" target={'_blank'}>Términos y Condiciones para Débito Automático.</Link>
                            </Typography>
                        </Grid> : null
                }
                <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                    <Button variant="contained" type={'submit'} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Continuar</Button>
                </Grid>
            </form>
        </Grid>
    </Grid>
}

const Four22 = ({ handleSendTokenAgain, handleSetToken, handleInputChange, tokenInput }) => {
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const inputsClasses = inputStyles();
    const tClasses = typographiesStyles();
    const InputProps = {
        className: inputsClasses.input,
        disableUnderline: true,
    };

    const { frontCC, backCC, photoFace } = useSelector(state => state.stepFour);

    return <Grid item xs={12} md={7}>
        <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
            <LinearWithValueLabel value={63} />
        </Grid>
        <Grid item xs={12} className={stepsClasses.withLinearProgress}>
            <Typography variant="h3">Paso 4</Typography>
            <Typography variant="h1">Chequeo de identidad e información bancaria</Typography>
            <Typography variant="body1">Valida con el número de token la autorización de débito automatico mensual.</Typography>

            <Typography variant="h1" className={`${tClasses.h1Black18} ${stepsClasses.mt35}`}>Validación débito automático.</Typography>
            <Typography variant="body1">Para confirmar que <b>sí autorizaste</b> el débito automático, ingresa el <br />
            token que acabamos de enviar a tu celular:</Typography>
            <form onSubmit={handleSetToken} noValidate={true} autoComplete="off">
                <Grid container direction="row" className={stepsClasses.mt35}>
                    <Grid item md={12} xs={12}>
                        <TextField
                            name="tokenInput"
                            value={tokenInput}
                            onChange={handleInputChange}
                            label="Token"
                            focused={false}
                            placeholder="Escribe el número de tu token"
                            fullWidth
                            displayEmpty
                            InputProps={InputProps}
                            className={inputsClasses.inputContainer}
                            InputLabelProps={{
                                shrink: true,
                                className: inputsClasses.label
                            }}
                        />
                    </Grid>
                </Grid>

                <Typography variant={"body1"} onClick={handleSendTokenAgain} className={`${stepsClasses.sendAgainText} ${stepsClasses.textEnd} `} style={{marginTop: 11}}>Reenviar Token</Typography>

                <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                    <Button variant="contained" type={'submit'} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={tokenInput != '' ? false : true} >Siguiente</Button>
                </Grid>
            </form>
        </Grid>
    </Grid>
}
