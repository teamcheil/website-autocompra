import React, { useState, useRef, useEffect } from 'react';
import {Box, Button, Fade, Grid, Typography} from '@material-ui/core';
import LinearProgressWithLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';

// Styles
import { buttonsStyles } from '../../../../../styles/theme/components/buttons'
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps'
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import ModelImage from "../../../../../assets/images/model_step1_3.png";
import { VideoPlayer } from '../../../../../components/videoPlayer';
import {ModelImageComponent} from "../../../../../components/ModelImage";


export default ({ nextStep }) => {

    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const typographiesClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);
    const tClasses = typographiesStyles();
    const refVideo = useRef(null);
    const src = "https://media.w3.org/2010/05/sintel/trailer_hd.mp4";


    useEffect(() => {
        onPlay();
    }, []);


    const onPlay = (currentTime) => {
        const currentTimeControl = 3;
        (currentTime > currentTimeControl) ? setFormValid(true) : setFormValid(false);
    };

    return (
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>
                <Grid item xs={12} md={7}>
                    <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                        <LinearProgressWithLabel value={81} />
                    </Grid>
                    <Grid item xs={12} className={`${stepsClasses.withLinearProgress} ${stepsClasses.mb20}`}>
                        <Typography variant="h3">Paso 4</Typography>
                        <Typography variant="h1">Chequeo de identidad e información bancaria</Typography>
                        <Typography variant="body1">Para mayor seguridad y comodidad, te pedimos algunos datos adicionales</Typography>
                        <Typography variant="body1" className={`${tClasses.body116} ${stepsClasses.mt35}`}>Ahora te explicamos:</Typography>
                        <Typography variant="body1" className={`${tClasses.body116} ${tClasses.bodyList}`}>¿Por qué te pedimos datos adicionales para confirmar tu identidad?</Typography>
                        <Typography variant="body1" className={`${tClasses.body116} ${tClasses.bodyList}`}>¿Para qué sirve tu información bancaria?</Typography>
                        <VideoPlayer src={src} onPlay={onPlay} />
                        <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                            <Button variant="contained" onClick={nextStep} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Siguiente</Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={5} className={stepsClasses.modelLeft}>
                    <ModelImageComponent src={ModelImage} align="center" width={90}>
                        <Typography variant="h1" className={ tClasses.h1Black }>Un método fácil y seguro</Typography>
                    </ModelImageComponent>
                </Grid>
            </Grid>
        </Fade>
    )
}
