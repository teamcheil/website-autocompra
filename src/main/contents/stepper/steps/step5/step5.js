import React, { useEffect, useState, } from 'react';
import { Button, Grid, Typography, Box, Fade, Link } from '@material-ui/core';
import LinearWithValueLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { ModelImageComponent } from "../../../../../components/ModelImage";

// Assets
import appStore from "../../../../../assets/images/image-9@3x.png";
import googlePlay from "../../../../../assets/images/group@3x.png";
import ModelImage from "../../../../../assets/images/stepCongrats2.png";
import kit from "../../../../../assets/images/kit-2-1@3x.png";
import polen from "../../../../../assets/images/group-539@3x.png";
import knowOurKit from '../../../../../assets/images/knowOurKit.png';
// Styles
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from '../../../../../styles/theme/components/typographies';
import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { useSelector } from 'react-redux';

export default ({ nextStep }) => {
    const stepsClasses = stepsStyles();
    const tClasses = typographiesStyles();
    const buttonsClasses = buttonsStyles();
    const { goal } = useSelector(state => state.stepOne);
    const [goalActive, setGoalActive] = useState(goal || false);

    useEffect(() => {
        console.log('Componente step 1 montado');
        setGoalActive(goal || false);
    }, [goal]);

    const onClickGoal = ({ target }) => {
        setGoalActive(true);
    };

    const onStore = (store) => {
        console.log('onStore ' + store);
    };


    return (
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>
                <Grid item xs={12} md={7}>
                    <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                        <LinearWithValueLabel value={100} />
                    </Grid>
                    <Grid item xs={12} className={stepsClasses.withLinearProgress}>
                        <Typography variant="h1">
                            En comunidad, la logras.
                        </Typography>
                        <img src={knowOurKit} style={{ width: '100%' }} className={stepsClasses.mt35} />
                        <Grid item xs={12} className={`${stepsClasses.mb20}`}>
                            <img src={polen} style={{ width: '100%' }} />
                            <div className={stepsClasses.apps2}>
                                <div style={{ marginTop: '20px', }}>
                                    <div className={stepsClasses.flexStart}>
                                        <Typography variant="body2" className={`${tClasses.h1Black} ${stepsClasses.textEnd}`}>Descarga nuestra app</Typography>
                                    </div>
                                    <Grid container className={stepsClasses.contImageStores} >
                                        <div>
                                            <Link href="#"><img onClick={() => onStore('apple')} src={appStore} style={{ border: '0px solid red', marginRight: '5px' }} /></Link>
                                        </div>
                                        <div>
                                            <Link href="#"><img onClick={() => onStore('google')} src={googlePlay} style={{ border: '0px solid red', }} /></Link>
                                        </div>
                                    </Grid>
                                </div>
                            </div>
                        </Grid>
                        <Grid item xs={12} className={`${stepsClasses.mt35} ${stepsClasses.mb30}`}>
                            <Grid className={`${stepsClasses.Rectangle_133} ${stepsClasses.flex}`}>
                                <Typography variant="h1" className={`${stepsClasses.alignCenter}`}>Recuerda</Typography>
                                <Typography variant="body1" className={`${stepsClasses.mt10}`}>
                                    Polen es una plataforma <strong>100% digital.</strong>  Nuestros asesores están dispuestos a resolver cada duda que tengas y a apoyarte en tu plan para alcanzar tu meta.
                                    {/* Polen es una plataforma <strong>100% digital</strong>, nuestros asesores estan dispuestos a resolver cada duda que tengas y apoyarte en este proceso importante para cumplir tu meta. */}
                                </Typography>
                                <div className={`${stepsClasses.mt20} ${stepsClasses.mb15}`}>
                                    <Button variant="contained" type={'submit'} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton} ${buttonsClasses.buttonLarge}`} >Contactar un asesor</Button>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={5} className={stepsClasses.modelLeft}>
                    <ModelImageComponent src={ModelImage} align="right" width={130}>
                        <Typography variant="h1">¡Felicitaciones!</Typography>
                        <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mt5} ${stepsClasses.mb5}`}><span className={tClasses.h1Black}>Ya eres parte de la Comunidad Polen</span></Typography>
                    </ModelImageComponent>
                </Grid>
            </Grid>
        </Fade>
    );
}
