import React, {useRef, useState} from 'react';
import { Grid, Typography, Button } from '@material-ui/core';


import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import srcImg from "../../../../../assets/images/model_step1_3.png";
import LinearWithValueLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { ModelImageComponent } from '../../../../../components/ModelImage';
import { VideoPlayer } from '../../../../../components/videoPlayer';

export default ({ nextStep }) => {

    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const tClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);
    const refVideo = useRef(null);
    const src = "https://media.w3.org/2010/05/sintel/trailer_hd.mp4";

    
    const onPlay = (currentTime) =>{
        const currentTimeControl = 2.5;
       (currentTime > currentTimeControl) ? setFormValid(true) :  setFormValid(false);
    };

    return(
        <Grid container direction={"row"}>
            <Grid item xs={12} md={7}>
                <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                    <LinearWithValueLabel value={60} />
                </Grid>
                <Grid item xs={12} className={`${stepsClasses.mt35}`}>
                    <Typography variant="h3" className={`${tClasses.h2BlackBold} ${stepsClasses.mt30}`}>Paso 3</Typography>
                    <Typography variant="h1">Adquiere tu plan </Typography>
                    <Typography variant="body1">Lo primero es tu tranquilidad, por eso tomamos todas las medidas para la compra segura de tu plan.</Typography>
                </Grid>

                <Grid item xs={12} md={12} className={`${stepsClasses.mt35}`}>
                    <Typography variant="h1" className={`${tClasses.h1Black18} ${stepsClasses.mb15}`}>Contrato y token</Typography>
                    <Typography variant="body1" className={`${stepsClasses.mb20}`}>Aquí te explicamos los pasos a seguir con el contrato enviado a tu email y el número de token enviado a tu celular. </Typography>
                    <VideoPlayer src={src} onPlay={onPlay} />
                </Grid>
                <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                    <Button variant="contained" onClick={nextStep} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Siguiente</Button>
                </Grid>
            </Grid>

            <Grid item xs={5} className={stepsClasses.modelLeft}>
                <ModelImageComponent src={srcImg} align="right" width={60}>
                    <Typography variant="h1" className={tClasses.h1Black}>Estamos cerca del plan con el cual alcanzarás tu meta</Typography>
                </ModelImageComponent>
            </Grid>
        </Grid>
    )
}
