import React, { useState, useEffect } from 'react';
// Components
import LinearProgressWithLabel from "../../../../../components/LinearProgres/LinearProgressWithLabel";
import { ModelImageComponent } from "../../../../../components/ModelImage";

// Styles
import { Button, Grid, TextField, Typography, makeStyles, Select, MenuItem } from '@material-ui/core';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';

import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from '../../../../../styles/theme/components/typographies';
import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { inputStyles } from "../../../../../styles/theme/components/inputs";
import ModelImage from "../../../../../assets/images/model_step1_22.png";

import { useDispatch } from 'react-redux';
import { setInfo } from '../../../../../redux/actions/stepThreeActions';

import { ModalComponent } from '../../../../../components/Modal';

import SuportImg from '../../../../../assets/images/group-463@3x.png';
import loader from "../../../../../assets/images/loader.gif";
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '400px',
        backgroundColor: 'rgba(242, 242, 242, 1)',
        marginBottom: '50px',
        borderRadius: '4px',
        boxShadow: 'none'
    },
    container: {
        display: 'flex'
    },
    boxBtnNext: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: '30px'
    },
    textEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    textCenter: {
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    },
    Checkbox: {
        padding: '0px',
    },
    chekR: {
        marginRight: '.8rem',
        textAlign: 'center'
    },
    center: {
        textAlign: 'center'
    },
    left: {
        marginLeft: '.5rem'
    },
    top15: {
        marginTop: '1rem'
    },
    width: {
        width: '30%'
    },
    widthIcon: {
        width: '23%'
    }
}));

export default ({ nextStep }) => {

    const [formValues, setFormValues] = useState({
        idNumber: '',
        idExpeditionDate: '',
        address: '',
        address1: "",
        address2: '',
        address3: '',
        birthDate: new Date(),
        departamento: '',
        city: ''
    });

    const { departamento, city, address1, address2, address3, birthDate } = formValues;

    //const { email, phone } = useSelector(state => state.stepOne);
    const [email, setEmail] = useState("a@a.com");
    const [phone, setPhone] = useState("333 3 33 33 33");

    const [showLoader, setShowLoader] = useState(false);

    const handleChange = (event) => {
        console.log('event.target =>', event.target)
        setFormValues({
            ...formValues,
            [event.target.name]: event.target.value
        })
    };

    useEffect(() => {
        validateForm();
    });

    console.log('idNumber')
    console.log(formValues.idNumber)

    const dispatch = useDispatch();
    const classes = useStyles();
    const stepsClasses = stepsStyles();
    //const typographiesClasses = typographiesStyles();
    const tClasses = typographiesStyles();
    const buttonsClasses = buttonsStyles();
    const inputsClasses = inputStyles();
    const [formValid, setFormValid] = useState(true);

    const [open, setOpen] = useState(false);

    const [selectedDate, setSelectedDate] = useState(new Date());

    const handleInputChange = ({ target }) => {

        setFormValues({
            ...formValues,
            [target.name]: target.value
        });
    };

    const validateForm = () => {
        if (formValues.idNumber != "" && selectedDate != "" && address1 != "" && address2 != "" && birthDate != "" && city != "" && departamento != "") {
            setFormValid(true);
        } else {
            setFormValid(false);
        }
    };

    const InputProps = {
        className: inputsClasses.input,
        disableUnderline: true,
    };

    const onClick = () => {
        setShowLoader(true)
        setTimeout(() => {
            setShowLoader(false)
            setOpen(true);
        }, 5000)
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = (e) => {
        setOpen(false);
    };

    const handleCloseYes = (e) => {
        setOpen(false);
        dispatch(setInfo(formValues.idNumber, selectedDate, address1 + " # " + address2 + " " + address3, birthDate, city, departamento));
        nextStep();
    };

    const handleDecline = (e) => {
        setOpen(false);
    }

    const handleDateChange = (date) => {
        setSelectedDate(date);
    };

    const handleBirthDateChange = (date) => {
        setFormValues({
            ...formValues,
            ["birthDate"]: date
        })
    };

    return (
        <Grid container direction={"row"}>
            <ModalComponent open={open} onClose={handleClose}>
                <Grid item xs={12} md={12} className={classes.center}>
                    <img alt="" src={SuportImg} className={classes.widthIcon} />
                </Grid>

                <Grid item xs={12} md={12} className={`${classes.top15} ${classes.top15}`}>
                    {/* <Typography variant="body1" className={`${classes.center}`}><span style={{ fontWeight: 'bold' }}>¡Con Polen, la logras! Hemos enviado a tu email</span> la información de tu plan, con tu <span style={{ fontWeight: 'bold' }}>número de contrato, y un token a tu celular.</span></Typography> */}
                    <Typography variant="body1" className={`${classes.center} ${tClasses.boldBVR}`}><span>¡Con Polen, la logras!</span></Typography>
                    <Typography variant="body1" className={`${classes.center} ${tClasses.boldBVR}`}><span>Hemos enviado a tu email:</span></Typography>
                    <Typography variant="body1" className={`${classes.center}`}><span style={{ fontSize: "18px", color: "rgba(254,91,2,1)" }}>{email}</span></Typography>
                    <Typography variant="body1" className={`${classes.center}`}><span style={{ fontSize: "18px" }}>la información de tu plan, con tu</span></Typography>
                    <Typography variant="body1" className={`${classes.center} ${tClasses.boldBVR}`}><span>número de contrato, <span style={{ fontWeight: 'normal' }}>y</span> un token</span></Typography>
                    <Typography variant="body1" className={`${classes.center} ${tClasses.boldBVR}`}><span>a tu celular:</span></Typography>
                    <Typography variant="body1" className={`${classes.center}`}><span style={{ fontSize: "18px", color: "rgba(254,91,2,1)" }}>{phone}</span></Typography>
                </Grid>

                <Grid item xs={12} sm={12} md={12} className={`${classes.center} ${classes.top15}`}>
                    <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={handleCloseYes}>Siguiente</Button>
                </Grid>
            </ModalComponent>
            <Grid item xs={12} md={7}>
                <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                    <LinearProgressWithLabel value={55} />
                </Grid>
                <Grid container direction={"row"} className={stepsClasses.withLinearProgress}>
                    <Typography variant="h3">Paso 3</Typography>
                    <Typography variant="h1">Adquiere tu plan</Typography>
                    <Typography variant="body1">
                        ¡Cada vez estamos más cerca de adquirir el plan con el cual alcanzarás tu meta!
                        <br /><br />
                        Para continuar y garantizarte una experiencia de compra sin inconvenientes, por favor completa los siguientes campos:
                    </Typography>
                    <Grid item xs={12} className={stepsClasses.mt35}>
                        <TextField
                            name="idNumber"
                            value={formValues.idNumber}
                            onChange={handleInputChange}
                            label="Cédula de ciudadanía:"
                            focused={false}
                            placeholder="Escribe el número de tu C.C"
                            fullWidth
                            InputProps={InputProps}
                            className={inputsClasses.inputContainer}
                            InputLabelProps={{
                                shrink: true,
                                className: inputsClasses.label
                            }}
                        />
                        <div >
                            <MuiPickersUtilsProvider utils={DateFnsUtils} >
                                <Grid container justify="space-around">
                                    <KeyboardDatePicker
                                        autoOk
                                        className={inputsClasses.inputContainerDate}
                                        variant="dialog"
                                        format="MM/dd/yyyy"
                                        margin="normal"
                                        label="Fecha de nacimiento:"
                                        placeholder="Selecciona tu fecha de nacimiento"
                                        value={birthDate}
                                        onChange={handleBirthDateChange}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                        InputLabelProps={{
                                            shrink: true
                                        }}
                                        helperText={""}
                                    />
                                </Grid>
                            </MuiPickersUtilsProvider>
                        </div>
                        <div style={{ marginBottom: 15 }} />
                        <div >
                            <MuiPickersUtilsProvider utils={DateFnsUtils} >
                                <Grid container justify="space-around">
                                    <KeyboardDatePicker
                                        autoOk
                                        className={inputsClasses.inputContainerDate}
                                        variant="dialog"
                                        placeholder="Selecciona la fecha de expedición de tu documento"
                                        format="MM/dd/yyyy"
                                        margin="normal"
                                        label="Fecha de expedición de la cédula:"
                                        value={selectedDate}
                                        onChange={handleDateChange}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                        InputLabelProps={{
                                            shrink: true
                                        }}
                                        helperText={""}
                                    />
                                </Grid>
                            </MuiPickersUtilsProvider>
                        </div>
                        <div style={{ marginBottom: 15 }} />
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "space-between",
                                alignItems: "center"
                            }}
                        >

                            <TextField
                                name="address1"
                                value={address1}
                                onChange={handleInputChange}
                                label="Dirección:"
                                focused={false}
                                placeholder="( ej. Calle 100C )"
                                InputProps={InputProps}
                                className={`${inputsClasses.inputContainer} ${inputsClasses.width45}`}
                                InputLabelProps={{
                                    shrink: true,
                                    className: inputsClasses.label
                                }}
                            />

                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    fontSize: 20
                                }}
                            >
                                #
                            </div>

                            <TextField
                                name="address2"
                                value={formValues.address2}
                                onChange={handleInputChange}
                                label=" "
                                focused={false}
                                placeholder=" ( 22a - 25 )"
                                InputProps={InputProps}
                                className={`${inputsClasses.inputContainerNoLabel} ${inputsClasses.width45}`}
                                InputLabelProps={{
                                    shrink: true,
                                    className: inputsClasses.label
                                }}
                            />

                        </div>
                        <TextField
                            name="address3"
                            value={formValues.address3}
                            onChange={handleInputChange}
                            label="Complemento:"
                            focused={false}
                            placeholder="(Apto, edificio, bloque, oficina, etc.)"
                            fullWidth
                            InputProps={InputProps}
                            className={inputsClasses.inputContainer}
                            InputLabelProps={{
                                shrink: true,
                                className: inputsClasses.label
                            }}
                        />
                        <Typography variant="body1" className={tClasses.body116}>Departamento:</Typography>
                        <Grid item md={12} xs={12}>
                            <Select
                                value={departamento}
                                onChange={handleChange}
                                name='departamento'
                                displayEmpty
                                InputProps={InputProps}
                                className={inputsClasses.selectContainer}
                                InputLabelProps={{
                                    shrink: true,
                                    className: inputsClasses.label
                                }}
                            >
                                <MenuItem value="" disabled> Selecciona tu departamento </MenuItem>
                                <MenuItem value={'departamento1'}>departamento1</MenuItem>
                                <MenuItem value={'departamento2'}>departamento2</MenuItem>
                                <MenuItem value={'departamento3'}>departamento3</MenuItem>
                            </Select>
                        </Grid>
                        <Typography variant="body1" className={tClasses.body116}>Ciudad:</Typography>
                        <Grid item md={12} xs={12}>
                            <Select
                                value={city}
                                onChange={handleChange}
                                name='city'
                                displayEmpty
                                InputProps={InputProps}
                                className={inputsClasses.selectContainer}
                                InputLabelProps={{
                                    shrink: true,
                                    className: inputsClasses.label
                                }}
                            >
                                <MenuItem value="" disabled> Selecciona tu ciudad </MenuItem>
                                <MenuItem value={'ciudad1'}>ciudad1</MenuItem>
                                <MenuItem value={'ciudad2'}>ciudad2</MenuItem>
                                <MenuItem value={'ciudad3'}>ciudad3</MenuItem>
                            </Select>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                        <Button variant="contained" onClick={onClick} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Siguiente</Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={5} className={stepsClasses.modelLeft}>
                <ModelImageComponent src={ModelImage} align={'center'}>
                    <Typography variant="h1" className={`${tClasses.h1Black}`}>¡Unidos seguimos avanzando hacia tu meta!</Typography>
                </ModelImageComponent>
            </Grid>
            {
                showLoader ?
                <Grid className={stepsClasses.modalContainer}>
                    <Grid className={stepsClasses.modalLoader}>
                        <img alt={"loader"} src={loader} style={{marginBottom: "11px"}}/>
                        <Typography variant="body1" className={`${classes.center} ${tClasses.boldBVR}`}><span style={{ fontSize: "23px", color: "rgba(254,91,2,1)" }}>Estamos validando tu</span></Typography>
                        <Typography variant="body1" className={`${classes.center} ${tClasses.boldBVR}`}><span style={{ fontSize: "23px", color: "rgba(254,91,2,1)" }}>información</span></Typography>
                    </Grid>
                </Grid> : null
            }
        </Grid>
    );
}
