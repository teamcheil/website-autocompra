import React, { useState, useEffect } from 'react';
import { Grid, Typography, Button, makeStyles, Link, Fade, StepConnector } from '@material-ui/core';
import CarA from '../../../../../assets/images/carA.svg';
import TravelA from '../../../../../assets/images/travelA.svg';
import MotorcycleA from '../../../../../assets/images/motorcycleA.svg';
import PhoneA from '../../../../../assets/images/phoneA.svg';
import HouseA from '../../../../../assets/images/houseA.svg';
import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import LinearWithValueLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { ModelImageComponent } from '../../../../../components/ModelImage';
import { useSelector } from 'react-redux';
import kit from "../../../../../assets/images/knowOurKit.png";
import polen from "../../../../../assets/images/group-539@3x.png";
import appStore from "../../../../../assets/images/image-9@3x.png";
import googlePlay from "../../../../../assets/images/group@3x.png";
import ModelImage from "../../../../../assets/images/stepCongrats2.png";
import IconWarning from '../../../../../assets/images/iconWarning.png';
import { ModalComponent } from '../../../../../components/Modal';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: 'rgba(242, 242, 242, 1)',
        marginBottom: '50px',
        borderRadius: '4px',
        boxShadow: 'none'
    },
    container: {
        display: 'flex'
    },
    boxBtnNext: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: '30px'
    },
    textEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    textCenter: {
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    },
    Checkbox: {
        padding: '0px'
    },
    chekR: {
        marginRight: '.8rem',
        textAlign: 'center'
    },
    flexStart: {
        justifyContent: 'flex-start',
        display: 'flex'
    },
    apps: {
        width: '15rem', marginTop: '-5rem', position: 'absolute', marginLeft: '1.6rem', display: 'flex', flexDirection: 'column', justifyContent: 'space-around'
    },
    center: {
        textAlign: 'center'
    },
    left: {
        marginLeft: '.5rem'
    },
    mb10: {
        [theme.breakpoints.down('md')]: {
            marginBottom: 10
        }
    },
    backgroundWhite: {
        backgroundColor: "#fff",
        color: theme.palette.primary.main,
        borderColor: theme.palette.primary.main,
        borderWidth: 1
    }
}));

const GOALS_A = {
    1: CarA,
    2: TravelA,
    3: MotorcycleA,
    4: PhoneA,
    5: HouseA
}

const LABEL_A = {
    1: 'Vehículo',
    2: 'Viaje',
    3: 'Moto',
    4: 'Célular',
    5: 'Remodelación Casa'
}


export default ({ nextStep }) => {
    const classes = useStyles();
    const { goal, Names, LastNames, email } = useSelector(state => state.stepOne)
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const tClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);

    const [down, setDown] = useState(true);
    const [payMethod, setPayMethod] = useState('digital');

    const [payState, setPayState] = useState('Pago exitoso');

    const [next, setNext] = useState(false);

    const handleChange = (val) => {
        payMethod == 'digital' ? setPayMethod('efectivo') : setPayMethod('digital');
    };

    useEffect(() => {
        const timer = setTimeout(() => {
            setNext(true);
        }, 3000);
        return () => clearTimeout(timer);
    }, []);

    const handlePayMethod = () => {
        console.log('handlePayForm');
        setDown(!down);
        console.log(down);
    };

    const onStore = (store) => {
        console.log('onStore ' + store);
    };

    const onNext = () => {
        setNext(true);
    }

    return (
        <>
            <ModalComponent open={next} onClose={() => {setNext(false)}}>
                <Grid item xs={12} className={classes.center}>
                    <img width="96.2" height="94.9" alt="" src={IconWarning} />
                </Grid>

                <Typography variant="h1" className={`${tClasses.h1Black18} ${classes.center} ${stepsClasses.mt35}`}>¡Recuerda!</Typography>

                <Grid item xs={12} className={`${classes.center} ${tClasses.body118} ${stepsClasses.mt10}`} >
                    <Typography variant="body1">El paso 4 es opcional, pero si deseas, puedes subir ahora tu documento de identidad, una foto y tus datos bancarios para agilizar procesos futuros. </Typography>
                </Grid>
                <Grid item xs={12} className={`${classes.center} ${stepsClasses.mt20}`} >
                    <Grid container>
                        <Grid item xs={12} md={6} className={`${stepsClasses.mb10}`}>
                            <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.whiteButton}`} onClick={() => { setNext(false) }}>No</Button>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={nextStep}>Si</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </ModalComponent>
            <Fade in={true} timeout={900}>
                <Grid container direction={"row"}>
                    <Grid item xs={12} md={7}>
                        <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                            <LinearWithValueLabel value={80} />
                        </Grid>
                        <Grid item xs={12} className={stepsClasses.withLinearProgress}>
                            <Typography variant="h1">
                                En comunidad, la logras.
                        </Typography>
                            <Typography variant="body1" className={`${stepsClasses.mt15}`}>
                                En equipo llegamos lejos. ¡Descubre ya el increíble kit de bienvenida que creamos para ti!
                        </Typography>
                            <img src={kit} style={{ width: '100%' }} className={stepsClasses.mt15} />
                            <Grid item xs={12} className={`${stepsClasses.mb20}`}>
                                <img src={polen} style={{ width: '100%' }} />
                                <div className={stepsClasses.apps2}>
                                    <div style={{ marginTop: '20px', }}>
                                        <div className={stepsClasses.flexStart}>
                                            <Typography variant="body2" className={`${tClasses.h1Black} ${stepsClasses.textEnd}`}>Descarga nuestra app</Typography>
                                        </div>
                                        <Grid container className={stepsClasses.contImageStores} >
                                            <div>
                                                <Link href="#"><img onClick={() => onStore('apple')} src={appStore} style={{ border: '0px solid red', marginRight: '5px' }} /></Link>
                                            </div>
                                            <div>
                                                <Link href="#"><img onClick={() => onStore('google')} src={googlePlay} style={{ border: '0px solid red', }} /></Link>
                                            </div>
                                        </Grid>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={12} className={`${stepsClasses.mt35} ${stepsClasses.mb30}`}>
                                <Grid className={`${stepsClasses.Rectangle_133} ${stepsClasses.flex}`}>
                                    <Typography variant="h1" className={`${stepsClasses.alignCenter}`}>Recuerda</Typography>
                                    <Typography variant="body1" className={`${stepsClasses.mt10}`}>
                                        Polen es una plataforma <strong>100% digital</strong>. Nuestros asesores están dispuestos a resolver cada duda que tengas y a apoyarte en tu plan para alcanzar tu meta.
                                </Typography>
                                    <div className={`${stepsClasses.mt20} ${stepsClasses.mb15}`}>
                                        <Button variant="contained" onClick={onNext} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton} ${buttonsClasses.buttonLarge}`} disabled={formValid === true} >Siguiente</Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={5} className={stepsClasses.modelLeft}>
                        <ModelImageComponent src={ModelImage} align="right" width={130}>
                            <Typography variant="h1">¡Felicitaciones!</Typography>
                            <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mt5} ${stepsClasses.mb5}`}><span className={tClasses.h1Black}>Ya eres parte de la Comunidad Polen</span></Typography>
                        </ModelImageComponent>
                    </Grid>
                </Grid>
            </Fade>
        </>
    )
}
