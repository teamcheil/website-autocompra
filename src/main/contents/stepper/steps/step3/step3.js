import React, { useEffect } from 'react';
import { Grid } from '@material-ui/core';

export default ({ nextStep }) => {
    useEffect(() => {
        const timer = setTimeout(() => {
            nextStep();
        }, 50);
        return () => clearTimeout(timer);
    });
    return (
        <Grid></Grid>
    );
}
