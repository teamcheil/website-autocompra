import React, { useState } from 'react';
import { Grid, Typography, Button, Box, Divider, makeStyles, Checkbox, Card, CardContent, RadioGroup, FormControlLabel, IconButton, MenuIcon } from '@material-ui/core';
import CarA from '../../../../../assets/images/carA.svg';
import TravelA from '../../../../../assets/images/travelA.svg';
import MotorcycleA from '../../../../../assets/images/motorcycleA.svg';
import successful from '../../../../../assets/images/group-500@3x.png';
import pending from '../../../../../assets/images/group-502@3x.png';
import rejected from '../../../../../assets/images/group-501@3x.png';
import PhoneA from '../../../../../assets/images/phoneA.svg';
import HouseA from '../../../../../assets/images/houseA.svg';

import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import srcImg from "../../../../../assets/images/model_step1_2.png";
import LinearWithValueLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { ModelImageComponent } from '../../../../../components/ModelImage';
import { useSelector } from 'react-redux';
import { OrangeCheckbox } from '../../../../../components/OrangeCheckBox';

import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

import Radio from '@material-ui/core/Radio';
import { border } from '@material-ui/system';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: 'rgba(242, 242, 242, 1)',
        marginBottom: '50px',
        borderRadius: '4px',
        boxShadow: 'none'
    },
    container: {
        display: 'flex'
    },
    boxBtnNext: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: '30px'
    },
    textEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        [theme.breakpoints.down('xs')]: {
            justifyContent: 'flex-start',
            alignItems: 'center',
            width: "100%"
        }
    },
    textCenter: {
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    },
    Checkbox: {
        padding: '0px'
    },
    chekR: {
        marginRight: '.8rem',
        textAlign: 'center'
    },
    mt15: {
        marginTop: 15,
        [theme.breakpoints.down('xs')]: {
            marginTop: 0,
        }
    }
}));

const GOALS_A = {
    1: CarA,
    2: TravelA,
    3: MotorcycleA,
    4: PhoneA,
    5: HouseA
}

const LABEL_A = {
    1: 'Vehículo',
    2: 'Viaje',
    3: 'Moto',
    4: 'Célular',
    5: 'Remodelación Casa'
}


export default ({ nextStep }) => {
    const classes = useStyles();
    const { goal, Names, LastNames, email } = useSelector(state => state.stepOne)
    const { payMethod } = useSelector(state => state.stepThree)
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const tClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);
    const [down, setDown] = useState(true);
    const [rejected, setRejected] = useState(false);
    const [payState, setPayState] = payMethod == 'efectivo' ? useState('Pago pendiente') : rejected ? useState('Pago rechazado') : useState('Pago exitoso');
    const [transState, setTransState] = payMethod == 'efectivo' ? useState('Transacción pendiente') : rejected ? useState('Transacción rechazada') : useState('Transacción exitosa');
    const [colorState, setColorState] = payMethod == 'efectivo' ? useState('#227aff') : rejected ? useState('#d93030') : useState('#2ecc71');

    const handlePayMethod = () => {
        console.log('handlePayForm');
        setDown(!down);
        console.log(down);
    };

    return (
        <Grid container direction={"row"}>
            <Grid item xs={12} md={10}>
                <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                    <LinearWithValueLabel value={75} />
                </Grid>

                <Grid item xs={12} className={`${stepsClasses.mt35} ${stepsClasses.mb20}`}>
                    <Typography variant="h3" className={`${tClasses.h2BlackBold} ${stepsClasses.mt30}`}>Paso 3</Typography>
                    <Typography variant="h1">Adquiere tu plan</Typography>
                    <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>{payMethod == 'digital' ? "En Polen unidos llegamos a donde queremos." : ""} El resultado de tu transacción fue:</Typography>
                </Grid>

                <Grid item xs={12} className={stepsClasses.mt35} >
                    <Card className={classes.root}>
                        <CardContent>
                            <Grid className={`${stepsClasses.mt15} ${stepsClasses.mb30}`}>
                                <div className={`${stepsClasses.flex}`}>
                                    {payMethod == 'digital' ? <img src={successful} className={`${stepsClasses.Ellipse84}`} /> : null}
                                    {payMethod == 'efectivo' ? <img src={pending} className={`${stepsClasses.Ellipse84}`} /> : null}
                                </div>

                                <div className={`${stepsClasses.flex} ${stepsClasses.mt30} ${stepsClasses.mb30}`}>
                                    <div>
                                        <Typography variant="h1" className={`${tClasses.h1Black} ${tClasses.h132}`}>{payState}</Typography>
                                    </div>
                                </div>
                            </Grid>
                            <Divider />
                            <Box className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Typography variant="body1" className={tClasses.body1Bold}>Información del pago</Typography>
                            </Box>
                            <Divider />

                            <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Estado de la transacción</Typography>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${classes.textEnd} ${classes.mt15}`} style={{ color: colorState }}>{transState}</Typography>
                                </Grid>
                            </Grid>

                            <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Número de la transacción</Typography>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${classes.textEnd} ${classes.mt15}`}><b>PP-BQQJIV</b></Typography>
                                </Grid>
                            </Grid>

                            <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Fecha y hora</Typography>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${classes.textEnd} ${classes.mt15}`}><b>20 de Junio 2020 1:00 p.m.</b></Typography>
                                </Grid>
                            </Grid>

                            <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Medio de pago</Typography>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${classes.textEnd} ${classes.mt15}`}><b>Tarjeta de crédito</b></Typography>
                                </Grid>
                            </Grid>

                            {/* <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Grid item xs={6}>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Estado de la transacción</Typography>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Número de la transacción</Typography>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Fecha y hora</Typography>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Medio de pago</Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <Typography variant="body1" className={`${classes.textEnd} ${stepsClasses.mt15}`} style={{color: colorState}}>{transState}</Typography>
                                    <Typography variant="body1" className={`${classes.textEnd} ${stepsClasses.mt15}`}>PP-BQQJIV</Typography>
                                    <Typography variant="body1" className={`${classes.textEnd} ${stepsClasses.mt15}`}>PP-BQQJIV</Typography>
                                    <Typography variant="body1" className={`${classes.textEnd} ${stepsClasses.mt15}`}>Tarjeta de crédito</Typography>
                                </Grid>
                            </Grid> */}
                            <Divider />
                            <Box className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Typography variant="body1" className={tClasses.body1Bold}>Descripción</Typography>
                            </Box>
                            <Divider />

                            <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Plan de compra programada para "PRODUCTO" con plazo de "PLAZO" meses y una cuota mensual de</Typography>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${classes.textEnd} ${classes.mt15}`}><b>$3.166.000</b></Typography>
                                </Grid>
                            </Grid>

                            <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Con seguro incluido</Typography>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1" className={`${classes.textEnd} ${classes.mt15}`}><b>$250.000</b></Typography>
                                </Grid>
                            </Grid>

                            {/* <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Grid item xs={9}>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Plan de ahorro programado para vehículo X 30 meses de plazo. Con una cuota fija mensual de</Typography>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>Con seguro incluido</Typography>
                                </Grid>
                                <Grid item xs={3}>
                                    <Typography variant="body1" className={`${classes.textEnd} ${stepsClasses.mt15}`} >$3.166.000</Typography>
                                    <Typography variant="body1" className={`${classes.textEnd} ${stepsClasses.mt25}`}>$250.000</Typography>
                                </Grid>
                            </Grid> */}
                            <Divider />
                            <Box className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                <Typography variant="body1" className={tClasses.body1Bold}>Detalle de pago</Typography>
                            </Box>
                            <Divider />
                            <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>

                                <Grid container direction={"row"} className={`${stepsClasses.mt10} ${stepsClasses.mb10}`}>
                                    <Grid item xs={12} md={6}>
                                        <Typography variant="body1" className={`${stepsClasses.mt15}`}>TOTAL</Typography>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Typography variant="body1" className={`${classes.textEnd} ${classes.mt15}`}>$3.416.000</Typography>
                                    </Grid>
                                </Grid>

                                {/* <Grid item xs={9}>
                                    <Typography variant="body1" className={`${stepsClasses.mt15}`}>TOTAL</Typography>
                                </Grid>
                                <Grid item xs={3}>
                                    <Typography variant="body1" className={`${tClasses.body1Bold} ${classes.textEnd} ${stepsClasses.mt15}`} >$3.416.000</Typography>
                                </Grid> */}
                            </Grid>
                        </CardContent>
                    </Card>
                    <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                        <Button variant="contained" onClick={nextStep} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === true} >Siguiente</Button>
                    </Grid>
                </Grid>

            </Grid>

            <Grid item xs={4} md={4} className={stepsClasses.modelLeft}>
            </Grid>
        </Grid>
    )
}
