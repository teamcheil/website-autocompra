import React, { useState, useEffect } from 'react';
import { Button, Fade, Grid, TextField, Typography, Card, CardContent, makeStyles } from '@material-ui/core';

import LinearProgressWithLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
// Styles
import { buttonsStyles } from '../../../../../styles/theme/components/buttons'
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps'
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import { inputStyles } from '../../../../../styles/theme/components/inputs';

import { ModelImageComponent } from '../../../../../components/ModelImage';
import ModelImage from "../../../../../assets/images/model_step1_22.png";
import icono_contrato from "../../../../../assets/images/icono-contrato.svg";

import { setContract } from '../../../../../redux/actions/stepThreeActions';

import { ModalComponent } from '../../../../../components/Modal';
import SuportImg from '../../../../../assets/images/group-463@3x.png';

import logo from '../../../../../assets/images/logo.svg';

import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '100%',
        backgroundColor: 'rgba(242, 242, 242, 1)',
        marginBottom: '50px',
        borderRadius: '4px',
        boxShadow: 'none',
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    centerModal:{
        textAlign: "center",
    },
    center: {
        textAlign: "center",
        color: "#000",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    contractSampleNumber: {
        backgroundColor: theme.palette.primary.t_2,
        border: '2px solid',
        borderColor: theme.palette.primary.main,
        borderRadius: '5px',
        width: '60% !important',
        height: '61px',
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginTop: "20px",
    },
    xxxx: {
        display: 'block',
        textAlign: 'center',
        width: '100%',
        fontSize: "23px"
    },
    logoContractSample: {
        height: '79px',
        marginTop: "20px",
        marginBottom: "20px"
    },
    body116: {
        fontSize: '15px',
        margin: 1
    },
    widthIcon: {
        width: '23%',
        marginBottom: "20px"
    },
    top30: {
        marginTop: "30px",
    }
}))

export default ({ nextStep }) => {
    const { email, phone } = useSelector(state => state.stepOne)
    const classes = useStyles();
    const dispatch = useDispatch();
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const inputsClasses = inputStyles();
    const tClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);

    const [values, setValues] = useState({
        contractNumber: '',
        token: ''
    });

    //const [email, setEmail] = useState("a@a.com");

    const { contractNumber, token } = values;

    const InputProps = {
        className: inputsClasses.input,
        disableUnderline: true,
    };

    useEffect(() => {
        validaTeForm();
    });

    const [open, setOpen] = useState(false);
    const [openToken, setOpenToken] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = (e) => {
        setOpen(false);
    };

    const handleCloseToken = (e) => {
        setOpenToken(false);
    };


    const validaTeForm = () => {
        if (contractNumber.trim().length > 0 && token.trim().length > 0) {
            setFormValid(true);
        } else {
            setFormValid(false);
        }
    };

    const onClick = () => {
        dispatch(setContract(values.contractNumber, values.token));
        nextStep();
    };

    const handleInputChange = ({ target }) => {
        setValues({
            ...values,
            [target.name]: target.value
        });
    };

    const onSendAgain = (v) => {
        if(v == "token"){
            setOpenToken(true)
        } else {
            setOpen(true)
        }
        console.log('onSendAgain')
    };

    return (
        <Fade in={true} timeout={900}>
            <Grid container direction={"row"}>
                <ModalComponent open={open} onClose={handleClose}>
                    <Grid item xs={12} md={12} className={classes.centerModal}>
                        <img alt="" src={SuportImg} className={classes.widthIcon} />
                    </Grid>

                    <Grid item xs={12} md={12} className={`${classes.top15} ${classes.top15}`}>
                        {/* <Typography variant="body1" className={`${classes.centerModal}`}><span style={{ fontWeight: 'bold' }}>¡Con Polen, la logras! Hemos enviado a tu email</span> la información de tu plan, con tu <span style={{ fontWeight: 'bold' }}>número de contrato, y un token a tu celular.</span></Typography> */}
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span style={{ fontWeight: 'normal' }}>Hemos enviado </span> nuevamente la</Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span>información de tu plan </span><span style={{ fontWeight: 'normal' }}>con tu</span> número</Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span>de contrato, </span><span style={{ fontWeight: 'normal' }}>al </span> email:</Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span style={{ fontWeight: 'bold', color: "rgba(254,91,2,1)" }}>{email}</span></Typography>
                    </Grid>

                    <Grid item xs={12} sm={12} md={12} className={`${classes.centerModal} ${classes.top30}`}>
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={handleClose}>Cerrar</Button>
                    </Grid>
                </ModalComponent>

                <ModalComponent open={openToken} onClose={handleCloseToken}>
                    <Grid item xs={12} md={12} className={classes.centerModal}>
                        <img alt="" src={SuportImg} className={classes.widthIcon} />
                    </Grid>

                    <Grid item xs={12} md={12} className={`${classes.top15} ${classes.top15}`}>
                        {/* <Typography variant="body1" className={`${classes.centerModal}`}><span style={{ fontWeight: 'bold' }}>¡Con Polen, la logras! Hemos enviado a tu email</span> la información de tu plan, con tu <span style={{ fontWeight: 'bold' }}>número de contrato, y un token a tu celular.</span></Typography> */}
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span style={{ fontWeight: 'normal' }}>Hemos enviado </span> nuevamente</Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span>el token a tu email:</span></Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span style={{ fontWeight: 'bold', color: "rgba(254,91,2,1)" }}>{email}</span></Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span>y a tu celular:</span></Typography>
                        <Typography variant="body1" className={`${classes.centerModal} ${tClasses.boldBVR}`}><span style={{ fontWeight: 'bold', color: "rgba(254,91,2,1)" }}>{phone}</span></Typography>
                    </Grid>

                    <Grid item xs={12} sm={12} md={12} className={`${classes.centerModal} ${classes.top30}`}>
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={handleCloseToken}>Cerrar</Button>
                    </Grid>
                </ModalComponent>

                <Grid item xs={12} md={7}>
                    <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                        <LinearProgressWithLabel value={65} />
                    </Grid>
                    <Grid item xs={12} className={stepsClasses.withLinearProgress}>
                        <Grid item xs={12} className={`${stepsClasses.mt35} ${stepsClasses.mb20}`}>
                            <Typography variant="h3" className={`${tClasses.h2BlackBold} ${stepsClasses.mt30}`}>Paso 3</Typography>
                            <Typography variant="h1">Adquiere tu plan </Typography>
                            <Typography variant="body1">Para que sigas disfrutando de una compra segura, necesitamos algunos datos.</Typography>
                        </Grid>
                        <Grid item xs={12} md={12} className={`${stepsClasses.mt35}`}>
                            <Typography variant="h1" className={`${tClasses.h1Black18} ${stepsClasses.mb15}`}>Validación de contrato y token</Typography>
                            <Typography variant="body1" className={`${stepsClasses.mb20}`}>Confirma el número de tu contrato y token para hacer una compra segura de tu plan.</Typography>
                        </Grid>

                        <Grid item xs={12} className={stepsClasses.mt35} >
                            <Card className={classes.root}>
                                <CardContent className={`${classes.center}`}>
                                    <img src={icono_contrato} className={`${classes.logoContractSample}`} />
                                    <Typography variant="body1" className={`${classes.body116} ${classes.center}`}>Este es el número de tu contrato.</Typography>
                                    <Typography variant="body1" className={`${classes.body116} ${classes.center}`}>Recuerda ingresarlo en la página de Polen para que</Typography>
                                    <Typography variant="body1" className={`${classes.body116} ${classes.center}`}>continúes con el proceso.</Typography>
                                    <Grid container direction="row" className={`${classes.contractSampleNumber}`}>
                                        <Typography className={`${classes.xxxx}`}>XXXX</Typography>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>

                        {/* <Grid item xs={12} md={12} className={`${stepsClasses.contractSample} ${stepsClasses.contractSampleBorder}`}>
                            <Grid container direction="row" className={`${stepsClasses.contractSample} ${stepsClasses.mt15}`}>
                                <img src={logo} className={`${stepsClasses.logoContractSample}`} />
                                <Grid container direction="row" className={`${stepsClasses.contractSampleNumber}`}>
                                    <Typography className={`${stepsClasses.xxxx}`}>XXXX</Typography>
                                </Grid>
                                <Grid container direction="row" className={`${stepsClasses.contractSampleDialog}`}>
                                    <Typography variant="body1" className={`${tClasses.body110Center} ${stepsClasses.anexo} ${stepsClasses.textColorWhite}`}>Éste es tu</Typography>
                                    <Typography variant="body1" className={`${tClasses.body110Center} ${stepsClasses.anexoBold} ${stepsClasses.textColorWhite}`}>Número de contrato</Typography>
                                </Grid>
                            </Grid>
                            <Grid className={`${stepsClasses.contractSample}`}>
                                <Typography variant="body1" className={`${tClasses.body110} ${stepsClasses.alignCenter}`}>ANEXO 1 AL CONTRATO FINANCIERO DENOMINADO MULTICUPÓN NO GARANTIZADO SOBRE SUPUESTOS DE AJUSTE O SUPUESTOS ESPECIALES DE AJUSTE</Typography>
                            </Grid>
                        </Grid>
                        <Typography variant="body1" className={`${tClasses.body1Italic} ${stepsClasses.mt10}`}>Éste es un ejemplo de tu contrato Polen.</Typography> */}

                        <Grid item xs={12} className={`${stepsClasses.mt35}`}>
                            <Typography variant="body1" className={`${tClasses.body116} ${stepsClasses.mt30} ${stepsClasses.mb30}`}>Ingresa a continuación los números de <span style={{ fontWeight: 'bold' }}>contrato</span> y <span style={{ fontWeight: 'bold' }}>token</span> para realizar la firma digital de tu contrato:</Typography>
                            <TextField
                                name="contractNumber"
                                value={contractNumber}
                                onChange={handleInputChange}
                                label="Número de contrato:"
                                focused={false}
                                placeholder="Escribe tu número de contrato"
                                fullWidth
                                InputProps={InputProps}
                                className={inputsClasses.inputContainer}
                                InputLabelProps={{
                                    shrink: true,
                                    className: inputsClasses.label
                                }}
                            />
                        </Grid>
                        <Grid container direction="row" item xs={12} md={12} className={`${stepsClasses.sendAgain}`} style={{marginTop: "13px"}}>
                            <Typography variant={"body1"} onClick={() => onSendAgain("contrato")} className={`${stepsClasses.sendAgainText}`}>Reenviar No. de contrato</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="token"
                                value={token}
                                onChange={handleInputChange}
                                label="Token:"
                                focused={false}
                                placeholder="Escribe tu token"
                                fullWidth
                                InputProps={InputProps}
                                className={inputsClasses.inputContainer}
                                InputLabelProps={{
                                    shrink: true,
                                    className: inputsClasses.label,
                                }}
                            />
                        </Grid>
                        <Grid container direction="row" item xs={12} md={12} className={`${stepsClasses.sendAgain}`} style={{marginTop: "13px"}}>
                            <Typography variant={"body1"} onClick={() => onSendAgain("token")} className={`${stepsClasses.sendAgainText}`}>Reenviar Token</Typography>
                        </Grid>
                        <Grid item xs={12} className={stepsClasses.boxBtnNext}>
                            <Button variant="contained" onClick={()=>{onClick()}} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} disabled={formValid === false} >Siguiente</Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={5} className={stepsClasses.modelLeft}>
                    <ModelImageComponent src={ModelImage} width={60}>
                        <Typography variant="h1" className={tClasses.h1Black}>Estás a un paso de llegar a donde quieres</Typography>
                    </ModelImageComponent>
                </Grid>
            </Grid>
        </Fade>
    )
}
