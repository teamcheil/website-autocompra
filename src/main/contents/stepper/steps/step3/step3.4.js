import React, { useState } from 'react';
import { Grid, Typography, Button, Box, Divider, makeStyles, Checkbox, Card, CardContent, RadioGroup, FormControlLabel, IconButton, MenuIcon } from '@material-ui/core';
import CarA from '../../../../../assets/images/carA.svg';
import TravelA from '../../../../../assets/images/travelA.svg';
import MotorcycleA from '../../../../../assets/images/motorcycleA.svg';
import PhoneA from '../../../../../assets/images/phoneA.svg';
import HouseA from '../../../../../assets/images/houseA.svg';

import { buttonsStyles } from '../../../../../styles/theme/components/buttons';
import { stepsStyles } from '../../../../../styles/theme/layout/steps/steps';
import { typographiesStyles } from "../../../../../styles/theme/components/typographies";
import srcImg from "../../../../../assets/images/model_step1_2.png";
import LinearWithValueLabel from '../../../../../components/LinearProgres/LinearProgressWithLabel';
import { ModelImageComponent } from '../../../../../components/ModelImage';

import { useDispatch, useSelector } from 'react-redux';
import { setPayMethod } from '../../../../../redux/actions/stepThreeActions';

import { ModalComponent } from '../../../../../components/Modal';

import { OrangeCheckbox } from '../../../../../components/OrangeCheckBox';

import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

import Radio from '@material-ui/core/Radio';

import SuportImg from '../../../../../assets/images/group-462@3x.png';

import { GOALS_A, LABEL_A } from "../../../../../components/ImageGoals/ImagesGoals";

import american from '../../../../../assets/images/icono-american-express@3x.png';
import bancolombia from '../../../../../assets/images/icono-bancolombia@3x.png';
import corresponsal from '../../../../../assets/images/icono-corresponsal-bancolombia@3x.png';
import mastercard from '../../../../../assets/images/icono-mastercard@3x.png';
import nequi from '../../../../../assets/images/icono-nequi@3x.png';
import pse from '../../../../../assets/images/icono-pse@3x.png';
import visa from '../../../../../assets/images/icono-visa@3x.png';


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: 'rgba(242, 242, 242, 1)',
        marginBottom: '50px',
        borderRadius: '4px',
        boxShadow: 'none',
        padding: '15px 10px 0px',
    },
    rootModal: {
        width: '100%',
        backgroundColor: 'rgba(255, 255, 255, 1)',
        marginBottom: '10px',
        borderRadius: '4px',
        boxShadow: 'none',
        padding: '10px 10px 0px',
    },
    container: {
        display: 'flex'
    },
    boxBtnNext: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: '30px'
    },
    textStart: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        [theme.breakpoints.down('md')]: {
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
        }
    },
    textEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        [theme.breakpoints.down('xs')]: {
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
        }
    },
    textCenter: {
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center'
    },
    textCenter2: {
        textAlign: 'justify',
        [theme.breakpoints.down('md')]: {
            textAlign: 'center',
        }
    },
    textCenter3: {
        textAlign: 'right',
        [theme.breakpoints.down('md')]: {
            textAlign: 'center',
        }
    },
    Checkbox: {
        padding: '0px',
    },
    chekR: {
        marginRight: '.8rem',
        textAlign: 'center'
    },
    center: {
        textAlign: 'center'
    },
    left: {
        marginLeft: '.5rem'
    },
    top15: {
        marginTop: '1rem'
    },
    width: {
        width: '30%'
    },
    title: {
        marginTop: '-20px',
    },
    flex2: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    flex: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginRight: 10,
        [theme.breakpoints.down('xs')]: {
            marginRight: 0,
            justifyContent: "flex-start",
            alignItems: "flex-start",
        }
    },
    ml10: {
        marginLeft: 10,
        [theme.breakpoints.down('xs')]: {
            marginLeft: 0,
        }
    },
    flexModal: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginRight: 20,
        [theme.breakpoints.down('xs')]: {
            marginRight: 0,
            justifyContent: "flex-start",
            alignItems: "flex-start",
        }
    },
    ml20: {
        marginLeft: 20,
        [theme.breakpoints.down('xs')]: {
            marginLeft: 0,
        }
    },
    center: {
        textAlign: 'center'
    },
    flexCenter: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
}));

// const GOALS_A = {
//     1: CarA,
//     2: TravelA,
//     3: MotorcycleA,
//     4: PhoneA,
//     5: HouseA
// }

// const LABEL_A = {
//     1: 'Vehículo',
//     2: 'Viaje',
//     3: 'Moto',
//     4: 'Tecnología',
//     5: 'Remodelación'
// }


export default ({ nextStep }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { goal, Names, LastNames, email } = useSelector(state => state.stepOne)
    const { idNumber } = useSelector(state => state.stepThree)
    const buttonsClasses = buttonsStyles();
    const stepsClasses = stepsStyles();
    const tClasses = typographiesStyles();
    const [formValid, setFormValid] = useState(false);

    const [open, setOpen] = useState(false);

    const [down, setDown] = useState(true);
    const [payMethod, setPayMethod34] = useState('digital');

    const [openHere, setOpenHere] = useState(false);
    const [openPayments, setOpenPayments] = useState(false);

    const handleChange = (val) => {
        payMethod == 'digital' ? setPayMethod34('efectivo') : setPayMethod34('digital')
    };

    const handlePayMethod = () => {
        console.log('handlePayForm');
        setDown(!down);
        console.log(down);
    };

    const onClick = () => {
        if (payMethod !== "efectivo") {
            setOpen(true)
        } else {
            dispatch(setPayMethod(payMethod));
            nextStep();
        }
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = (value) => {
        if (value == "here") {
            setOpenHere(false);
        } else if (value == "payments") {
            setOpenPayments(false);
        } else {
            setOpen(false);
        }
    };

    // const handleClose = (e) => {
    //     console.log(e)
    //     setOpen(false);
    // };

    const handleCloseYes = (e) => {
        setOpen(false);
        dispatch(setPayMethod(payMethod));
        nextStep();
    };

    const handleDecline = (e) => {
        setPayMethod34('digital')
        setOpen(false);
    };

    return (
        <Grid container direction={"row"}>
            <ModalComponent open={openHere} onClose={() => handleClose("here")}>

                <Grid item xs={12} md={12} className={classes.center}>
                    <img alt="" src={SuportImg} style={{ width: "96px", marginBottom: "20px" }} />
                </Grid>

                <Grid item xs={12} md={12} className={classes.center} >
                    <Typography variant="h1" color="inherit">Exclusiones Seguro de Vida</Typography>
                </Grid>

                <Grid item xs={12} md={12}>
                    <Typography variant="body2" style={{ marginTop: 15, fontWeight: 'bold' }}>A la hora de adquirir tu seguro debes tener en cuenta:</Typography>
                </Grid>

                <Grid item xs={12} md={12}>
                    <Typography variant="body1">
                        1. La cobertura de tu seguro de vida inicia en el mes que participes en la primera asamblea de adjudicación.
    </Typography>
                    <Typography variant="body1">
                        2. Si eres mayor de 60 años y/o el valor asegurado en vida de todos tus planes es mayor a 200 millones, te contactaremos para diligenciar la solucitud individual del seguro, previo a la expedición del mismo.
    </Typography>
                    <Typography variant="body1">
                        3. Las exclusiones del seguro de vida son las siguientes:
    </Typography>
                </Grid>

                <Grid item xs={12} md={12}>
                    <Typography variant="body2" style={{ marginTop: 15, fontWeight: 'bold' }}>EXCLUSIONES POR CONDICIÓN FÍSICA:</Typography>
                    <Typography variant="body1">
                        - Personas discapacitadas.
    </Typography>
                    <Typography variant="body1">
                        - Personas que sufran de epilepsia.
    </Typography>
                    <Typography variant="body1">
                        - Personas con trastornos mentales y drogadicción.
    </Typography>
                </Grid>

                <Grid item xs={12} md={12}>
                    <Typography variant="body2" style={{ marginTop: 15, fontWeight: 'bold' }}>EXCLUSIONES POR OCUPACIÓN:</Typography>
                    <Typography variant="body1">
                        - Trabajadores de obras subterraneas. Mineros de socavón y túneles.
    </Typography>
                    <Typography variant="body1">
                        - Mataderos (matarifes).
    </Typography>
                    <Typography variant="body1">
                        - Personal de fábricas de pólvora y explosivos.
                    </Typography>
                    <Typography variant="body1">
                        - Productos químicos, producción, manipulación de clorhídrico, nítrico, clahídrico, álcalis, arsénico, etc.
    </Typography>
                </Grid>

                <Grid item xs={12} className={`${classes.flexCenter} ${stepsClasses.mt20}`} >
                    <Grid item xs={12} md={6} className={`${classes.flexCenter}`}>
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={() => handleClose("here")}>Cerrar</Button>
                    </Grid>
                </Grid>

            </ModalComponent>
            <ModalComponent open={openPayments} onClose={() => handleClose("payments")}>
                <Grid container direction="row" justify="center">
                    <Grid item xs={12} md={12} className={classes.center}>
                        <img alt="" src={GOALS_A[goal]} />
                    </Grid>

                    <Grid item xs={12} md={12} className={`${stepsClasses.mt15} ${classes.center}`}>
                        <Typography variant="h1" >Pensando en tu comodidad,</Typography>
                        <Typography variant="h1" >diferiremos tu cuota de ingreso en</Typography>
                        <Typography variant="h1" >XX pagos, así:</Typography>
                    </Grid>

                    {/* <Grid item xs={12} md={12} className={`${stepsClasses.mt10} ${classes.center}`}>
                        <Typography variant="h3" color="primary">XX pagos así:</Typography>
                    </Grid> */}

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            marginTop: "15px"
                        }}
                    >
                        <Typography className={`${classes.bold}`}>1. X%: </Typography>
                        <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                    </Grid>

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            marginTop: "15px"
                        }}
                    >
                        <Typography className={`${classes.bold}`}>1. X%: </Typography>
                        <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                    </Grid>

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            marginTop: "15px"
                        }}
                    >
                        <Typography className={`${classes.bold}`}>1. X%: </Typography>
                        <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                    </Grid>

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            marginTop: "15px"
                        }}
                    >
                        <Typography className={`${classes.bold}`}>1. X%: </Typography>
                        <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                    </Grid>

                    <Grid item xs={12} md={12}
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            marginTop: "15px"
                        }}
                    >
                        <Typography className={`${classes.bold}`}>1. X%: </Typography>
                        <div className={`${stepsClasses.row} ${classes.ml5}`}><Typography>$</Typography><Typography color="primary">000.000.000</Typography></div>
                    </Grid>

                    <Grid item xs={12} sm={12} md={12} className={classes.center}>
                        <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton} ${stepsClasses.mt30}`} onClick={() => handleClose("payments")}>Cerrar</Button>
                    </Grid>
                </Grid>
            </ModalComponent>
            {
                payMethod === 'efectivo' ?
                    <ModalComponent open={open} onClose={handleClose}>
                        <Grid item xs={12} md={12} className={classes.center}>
                            <img alt="" src={SuportImg} className={classes.width} />
                        </Grid>
                        <Grid item xs={12} md={12} >
                            <Typography variant="h1" color="inherit" className={`${classes.center} ${stepsClasses.mt20}`}>Recuerda</Typography>
                            <Typography variant="h1" color="inherit" className={`${tClasses.h1Black18} ${stepsClasses.mt10}`}>El pago en línea te brinda los siguientes beneficios:</Typography>
                        </Grid>

                        <Grid item xs={12} md={12} className={classes.top15}>
                            <Typography variant="body1">Seguridad y agilidad al reducir el manejo de efectivo.</Typography>
                        </Grid>

                        <Grid item xs={12} md={12} className={classes.top15}>
                            <Typography variant="body1">Realizas el pago sin moverte de tu hogar u oficina.</Typography>
                        </Grid>

                        <Grid item xs={12} md={12} className={classes.top15}>
                            <Typography variant="body1">Disponibilidad las 24 horas del día, 7 días a la semana y todos los días del año.</Typography>
                        </Grid>

                        <Grid item xs={12} md={12} className={classes.top15}>
                            <Typography variant="body1">Confirmación en línea de las transacciones.</Typography>
                        </Grid>

                        <Grid item xs={12} md={12} className={`${classes.center} ${classes.top15}`}>
                            <Typography variant="body" className={`${tClasses.body118} ${tClasses.body1Bold}`}>¿Estás seguro de hacer el pago en línea?</Typography>
                        </Grid>

                        <Grid item xs={12} className={`${classes.center} ${stepsClasses.mt20}`} >
                            <Grid container>
                                <Grid item xs={12} md={6} className={`${stepsClasses.mb10}`}>
                                    <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.whiteButton}`} onClick={handleCloseYes}>NO</Button>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Button color='primary' className={`${buttonsClasses.button36} ${buttonsClasses.orangeButton}`} onClick={handleDecline}>SÍ</Button>
                                </Grid>
                            </Grid>
                        </Grid>

                    </ModalComponent>
                    :
                    <ModalComponent open={open} onClose={handleClose} gray={'true'} style={{ overflowY: 'scroll', height: '90%' }}>
                        <Typography variant="h1" className={`${tClasses.h1Black} ${classes.title}`} >Confirmación compra</Typography>
                        <Divider />
                        <Typography variant="body1" color="inherit" className={`${tClasses.body118} ${classes.center} ${classes.top15}`}>
                            Ya casi finalizas el proceso para adquirir tu plan.
                        </Typography>
                        <Typography variant="body1" color="inherit" className={`${tClasses.body118} ${classes.center}`}>
                            ¡confirma tu compra y avancemos hacia esa meta!
                        </Typography>
                        <Grid item xs={12} className={`${stepsClasses.mt5}`}>
                            <Card className={classes.rootModal}>
                                <CardContent>
                                    <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>Suscriptor Polen</Typography>
                                    <Divider />
                                    <Grid item xs={12} >
                                        <Typography variant="body1" className={`${tClasses.body1Bold} ${stepsClasses.mb10} ${stepsClasses.mt15}`}>{`${Names || 'N/A'} ${LastNames || 'N/A'}`}</Typography>
                                        <Typography variant="body1" className={`${stepsClasses.mb10}`}>{email || 'N/A'}</Typography>
                                        <Typography variant="body1" className={`${stepsClasses.mb10}`}>{'CC: ' + idNumber || 'N/A'}</Typography>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} className={`${stepsClasses.mt5}`}>
                            <Card className={classes.rootModal}>
                                <CardContent>
                                    <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>Plan seguro</Typography>
                                    <Grid container direction="row" className={classes.container}>
                                        <Grid item xs={6} md={6}>
                                            <Typography variant="h5" className={tClasses.h1Black}>
                                                <Checkbox defaultChecked disabled className={classes.Checkbox} /> Incluido</Typography>
                                        </Grid>
                                        <Grid item xs={6} md={6}>
                                            <Typography variant="body2" className={`${tClasses.h1Bold} ${classes.textEnd}`}>$250.000</Typography>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} className={stepsClasses.mt5} >
                            <Card className={classes.rootModal}>
                                <CardContent>
                                    <Grid container direction="row" className={`${stepsClasses.spaceBetween}`}>
                                        <div>
                                            <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>
                                                Método de pago
                                            </Typography>
                                        </div>
                                        <div onClick={handlePayMethod} className={`${stepsClasses.cursorPointer}`} >
                                            {down ? null : null}
                                        </div>
                                    </Grid>
                                    <RadioGroup name="payMethod" value={payMethod}>
                                        <Grid container direction="row" >
                                            <FormControlLabel value="digital" control={<Radio color='primary' />} label="Pago en línea" />
                                        </Grid>
                                    </RadioGroup>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} className={stepsClasses.mt5} >
                            <Card className={classes.rootModal}>
                                <CardContent>
                                    <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>Resumen de plan</Typography>
                                    <Divider />
                                    <Grid container direction="row" className={stepsClasses.mt15}>
                                        <Grid item xs={12} md={1} sm={1}>
                                            <Checkbox defaultChecked disabled className={classes.Checkbox} />
                                        </Grid>
                                        <Grid item xs={12} md={4} sm={4} className={`${classes.flexModal}`}>
                                            <img src={GOALS_A[goal]} width={"50px"} alt='class' />
                                            <Typography variant="body1" className={`${tClasses.h1Bold} ${classes.textCenter}`}><b>{LABEL_A[goal]}</b></Typography>
                                            {/* <Typography variant="body1" className={`${tClasses.h1Bold} ${classes.textCenter}`}><b>Remodelación</b></Typography> */}
                                        </Grid>
                                        <Grid item xs={12} md={4} sm={4}>
                                            <Typography variant="body1" className={`${tClasses.h114Black} ${classes.textStart}`}><b>PLAN VEHÍCULO 1</b></Typography>
                                            <Typography variant="body1" className={`${tClasses.h114Black} ${classes.textStart}`}><b>$50.000.000</b></Typography>
                                            <Typography variant="body1" className={`${classes.textStart}`}>X 24 Meses de plazo</Typography>
                                        </Grid>
                                        <Grid item xs={12} md={3} sm={3}>

                                        </Grid>
                                    </Grid>

                                    <Grid container direction="row" className={stepsClasses.mt15}>
                                        <Grid item xs={12} md={1} sm={1}>

                                        </Grid>
                                        <Grid item xs={12} md={4} sm={4}>

                                        </Grid>
                                        <Grid item xs={12} md={4} sm={4}>
                                            <Typography variant="body1" className={`${classes.textStart} ${classes.ml20}`}>1er Pago Cuota de ingreso</Typography>
                                        </Grid>
                                        <Grid item xs={12} md={3} sm={3}>
                                            <Typography variant="body1" className={`${tClasses.body1Bold} ${classes.textEnd}`}>$1.000.000</Typography>
                                        </Grid>
                                    </Grid>

                                    <Grid container direction="row" className={stepsClasses.mt15}>
                                        <Grid item xs={12} md={1} sm={1}>

                                        </Grid>
                                        <Grid item xs={12} md={4} sm={4}>

                                        </Grid>
                                        <Grid item xs={12} md={4} sm={4}>
                                            <Typography variant="body1" className={`${classes.textStart} ${classes.ml20}`}>Cuota mensual</Typography>
                                        </Grid>
                                        <Grid item xs={12} md={3} sm={3}>
                                            <Typography variant="body1" className={`${tClasses.body1Bold} ${classes.textEnd}`}>$500.000</Typography>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} className={stepsClasses.mt5}>
                            <Card className={classes.rootModal}>
                                <CardContent>
                                    {/* <Grid container direction="row" className={`${stepsClasses.mt15} ${stepsClasses.mb15}`}>
                                        <Grid item xs={6}>
                                            <Typography variant="body1" className={`${stepsClasses.mb15}`}>subtotal</Typography>
                                            <Typography variant="body1" className={`${stepsClasses.mb15}`}>Seguro</Typography>
                                            <Typography variant="body1" className={`${stepsClasses.mb15}`}>Valor admisión y admon</Typography>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Typography variant="body1" className={`${classes.textEnd} ${stepsClasses.mb15}`}>$1.500.000</Typography>
                                            <Typography variant="body1" className={`${classes.textEnd} ${stepsClasses.mb15}`}>$250.000</Typography>
                                            <Typography variant="body1" className={`${classes.textEnd} ${stepsClasses.mb15}`}>$300.000</Typography>
                                        </Grid>
                                    </Grid>
                                    <Divider /> */}
                                    <Grid container direction="row" className={stepsClasses.mt15}>
                                        <Grid item xs={12} md={6} sm={6}>
                                            <Typography variant="body1" className={`${tClasses.body124Bold} ${classes.textCenter2}`}>Total</Typography>
                                        </Grid>
                                        <Grid item xs={12} md={6} sm={6}>
                                            <Typography variant="body1" className={`${tClasses.body124Bold} ${classes.textCenter3}`}>$2.050.000</Typography>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Button color='primary' className={`${buttonsClasses.button} ${buttonsClasses.orangeButton} ${buttonsClasses.button100}`} onClick={handleCloseYes}><span style={{ fontWeight: 'bold' }}>CONFIRMAR COMPRA</span></Button>
                    </ModalComponent>
            }
            <Grid item xs={12} md={7}>
                <Grid item xs={12} className={`${stepsClasses.boxLinearProgress} ${stepsClasses.showDesktop}`}>
                    <LinearWithValueLabel value={70} />
                </Grid>

                <Grid item xs={12} className={`${stepsClasses.mt35} ${stepsClasses.mb20}`}>
                    <Typography variant="h3" className={`${tClasses.h2BlackBold} ${stepsClasses.mt30}`}>Paso 3</Typography>
                    <Typography variant="h1">Adquiere tu plan</Typography>
                    <Typography variant="body1">Escoge el método de pago con el cual realizarás la compra. Tenemos varios medios de pago disponibles para ti.</Typography>
                </Grid>
                <Grid item xs={12} className={`${stepsClasses.mt35} ${stepsClasses.showMobile}`}>
                    <Card className={classes.root}>
                        <CardContent>
                            <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>Suscriptor Polen</Typography>
                            <Divider />
                            <Grid item xs={12} >
                                <Typography variant="body1" className={`${tClasses.body1Bold} ${stepsClasses.mb10} ${stepsClasses.mt15}`}>{`${Names || 'N/A'} ${LastNames || 'N/A'}`}</Typography>
                                <Typography variant="body1" className={`${stepsClasses.mb10}`}>{email || 'N/A'}</Typography>
                                <Typography variant="body1" className={`${stepsClasses.mb10}`}>{'CC: ' + idNumber || 'N/A'}</Typography>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} className={stepsClasses.mt35} >
                    <Card className={classes.root}>
                        <CardContent>
                            <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>Resumen de plan</Typography>
                            <Divider />
                            <Grid container direction="row" className={stepsClasses.mt15}>
                                <Grid item xs={12} md={1} sm={1}>
                                    <Checkbox defaultChecked disabled className={classes.Checkbox} />
                                </Grid>
                                <Grid item xs={12} md={4} sm={4} className={`${classes.flex}`}>
                                    <img src={GOALS_A[goal]} width={"50px"} alt='class' />
                                    <Typography variant="body1" className={`${tClasses.h1Bold} ${classes.textCenter}`}><b>{LABEL_A[goal]}</b></Typography>
                                    {/* <Typography variant="body1" className={`${tClasses.h1Bold} ${classes.textCenter}`}><b>Remodelación</b></Typography> */}
                                </Grid>
                                <Grid item xs={12} md={4} sm={4}>
                                    <Typography variant="body1" className={`${tClasses.h114Black} ${classes.textStart}`}><b>PLAN VEHÍCULO 1</b></Typography>
                                    <Typography variant="body1" className={`${tClasses.h114Black} ${classes.textStart}`}><b>$50.000.000</b></Typography>
                                    <Typography variant="body1" className={`${classes.textStart}`}>X 24 Meses de plazo</Typography>
                                </Grid>
                                <Grid item xs={12} md={3} sm={3}>

                                </Grid>
                            </Grid>

                            <Grid container direction="row" className={stepsClasses.mt15}>
                                <Grid item xs={12} md={1} sm={1}>

                                </Grid>
                                <Grid item xs={12} md={4} sm={4}>

                                </Grid>
                                <Grid item xs={12} md={4} sm={4}>
                                    {/* <Typography variant="body1" className={`${classes.textStart} ${classes.ml10}`}>Cuota de ingreso</Typography> */}
                                    <Typography variant="body1" className={`${classes.ml10}`}>Cuota de ingreso total (se diferirá en <span style={{ color: "#fe5b02", cursor: "pointer" }} onClick={() => { setOpenPayments(true) }}>XX pagos</span>)</Typography>
                                </Grid>
                                <Grid item xs={12} md={3} sm={3}>
                                    <Typography variant="body1" className={`${tClasses.body1Bold} ${classes.textEnd}`}>$1.000.000</Typography>
                                </Grid>
                            </Grid>

                            <Grid container direction="row" className={stepsClasses.mt5}>
                                <Grid item xs={12} md={1} sm={1}>

                                </Grid>
                                <Grid item xs={12} md={4} sm={4}>

                                </Grid>
                                <Grid item xs={12} md={4} sm={4}>
                                    <Typography variant="body1" className={`${classes.textStart} ${classes.ml10}`}>1er Pago Cuota de ingreso</Typography>
                                </Grid>
                                <Grid item xs={12} md={3} sm={3}>
                                    <Typography variant="body1" className={`${tClasses.body1Bold} ${classes.textEnd}`}>$1.000.000</Typography>
                                </Grid>
                            </Grid>

                            <Grid container direction="row" className={stepsClasses.mt5}>
                                <Grid item xs={12} md={1} sm={1}>

                                </Grid>
                                <Grid item xs={12} md={4} sm={4}>

                                </Grid>
                                <Grid item xs={12} md={4} sm={4}>
                                    <Typography variant="body1" className={`${classes.textStart} ${classes.ml10}`}>Cuota mensual (sin seguro)</Typography>
                                </Grid>
                                <Grid item xs={12} md={3} sm={3}>
                                    <Typography variant="body1" className={`${tClasses.body1Bold} ${classes.textEnd}`}>$668.606</Typography>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} className={stepsClasses.mt35}>
                    <Card className={classes.root}>
                        <CardContent>
                            <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>Plan de seguro</Typography>
                            <Divider />
                            <Grid container direction="row" className={`${stepsClasses.mt5} ${stepsClasses.mb10}`}>
                                <Typography variant="body1">
                                    Para ver las exclusiones y el inicio de la vigencia del seguro de vida has click <span style={{ color: "#fe5b02", cursor: "pointer", textDecoration: "underline" }} onClick={() => { setOpenHere(true) }}>aquí.</span>
                                </Typography>
                            </Grid>
                            <Grid container direction="row" className={`${stepsClasses.mt15} ${stepsClasses.mb10}`}>
                                <Grid item xs={2}>
                                    <Checkbox defaultChecked disabled className={classes.Checkbox} />
                                </Grid>
                                <Grid item xs={10}>

                                    <Grid container direction="row">
                                        <Grid item xs={12} md={6} sm={6}>
                                            <Typography variant="body1" className={`${tClasses.body1Bold}`}>Tipo de Seguro:</Typography>
                                        </Grid>
                                        <Grid item xs={12} md={6} sm={6}>
                                            <Typography variant="body1" className={`${classes.textEnd}`}>Seguro de vida</Typography>
                                        </Grid>
                                    </Grid>

                                    <Grid container direction="row">
                                        <Grid item xs={12} md={6} sm={6}>
                                            <Typography variant="body1" className={`${tClasses.body1Bold}`}>Aseguradora</Typography>
                                        </Grid>
                                        <Grid item xs={12} md={6} sm={6}>
                                            <Typography variant="body1" className={`${classes.textEnd}`}>MAFRE</Typography>
                                        </Grid>
                                    </Grid>

                                    <Grid container direction="row">
                                        <Grid item xs={12} md={6} sm={6}>
                                            <Typography variant="body1" className={`${tClasses.body1Bold}`}>Valor Cuota de Seguro Mes:</Typography>
                                        </Grid>
                                        <Grid item xs={12} md={6} sm={6}>
                                            <Typography variant="body1" className={`${classes.textEnd}`}>$250.000</Typography>
                                        </Grid>
                                    </Grid>

                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} className={stepsClasses.mt35} >
                    <Card className={classes.root}>
                        <CardContent>
                            <Grid container direction="row" className={`${stepsClasses.spaceBetween}`}>
                                <div>
                                    <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>Métodos de pago</Typography>
                                </div>
                                <div onClick={handlePayMethod} className={`${stepsClasses.cursorPointer}`} >
                                    {
                                        down ?
                                            <KeyboardArrowDownIcon />
                                            :
                                            <KeyboardArrowUpIcon />
                                    }
                                </div>
                            </Grid>
                            <Divider />
                            {
                                (!down && payMethod !== "efectivo") ?
                                    <Grid
                                        style={{
                                            display: "flex",
                                            flexWrap: "wrap",
                                            marginBottom: "13px"
                                        }}
                                    >
                                        <img src={visa} style={{ width: "90px", marginTop: "7px", marginRight: "5px" }} />
                                        <img src={mastercard} style={{ width: "90px", marginTop: "7px", marginRight: "5px" }} />
                                        <img src={american} style={{ width: "90px", marginTop: "7px", marginRight: "5px" }} />
                                        <img src={nequi} style={{ width: "90px", marginTop: "7px", marginRight: "5px" }} />
                                        <img src={pse} style={{ width: "90px", marginTop: "7px", marginRight: "5px" }} />
                                    </Grid>
                                    : (!down && payMethod == "efectivo") ?
                                        <Grid
                                            style={{
                                                display: "flex",
                                                flexWrap: "wrap",
                                                marginBottom: "13px"
                                            }}
                                        >
                                            <img src={bancolombia} style={{ width: "90px", marginTop: "7px", marginRight: "5px" }} />
                                            <img src={corresponsal} style={{ width: "90px", marginTop: "7px", marginRight: "5px" }} />
                                        </Grid>
                                        : null
                            }
                            <RadioGroup name="paymethod" value={payMethod} onChange={handleChange}>
                                <Grid container direction="row" >
                                    <Grid item xs={6}>
                                        <FormControlLabel value="digital" control={<Radio color='primary' />} label={<Typography variant={"body1"}>Pago en línea</Typography>} />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <FormControlLabel onChange={(event) => { setOpen(true) }} value="efectivo" control={<Radio color='primary' />} label={<Typography variant={"body1"}>Pago en efectivo</Typography>} />
                                    </Grid>
                                </Grid>
                            </RadioGroup>
                            {/* <Grid container direction="row" className={`${stepsClasses.mt15}`}>

                            </Grid> */}
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} className={stepsClasses.mt35}>
                    <Card className={classes.root}>
                        <CardContent>
                            <Grid container direction="row" className={stepsClasses.mt15}>
                                <Grid item xs={12} md={6} sm={6}>
                                    <Typography variant="body1" className={`${tClasses.body124Bold} ${classes.textCenter2}`}>Total Pago Inicial</Typography>
                                </Grid>
                                <Grid item xs={12} md={6} sm={6}>
                                    <Typography variant="body1" className={`${tClasses.body124Bold} ${classes.textCenter3}`}>$2.050.000</Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={12} className={classes.boxBtnNext}>
                                <Button variant="contained" onClick={onClick} className={`${buttonsClasses.button} ${buttonsClasses.orangeButton}`} >Enviar</Button>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <Grid item xs={5} className={stepsClasses.modelLeft}>
                <Grid container direction="row" className={`${stepsClasses.mt218}`}>
                    <Grid item xs={12}>
                        <Card className={classes.root}>
                            <CardContent>
                                <Typography variant="h1" className={`${tClasses.h1Black} ${stepsClasses.mb15}`}>Suscriptor Polen</Typography>
                                <Divider />
                                <Grid item xs={12} >
                                    <Typography variant="body1" className={`${tClasses.body1Bold} ${stepsClasses.mb10} ${stepsClasses.mt15}`}>{`${Names || 'N/A'} ${LastNames || 'N/A'}`}</Typography>
                                    <Typography variant="body1" className={`${stepsClasses.mb10}`}>{email || 'N/A'}</Typography>
                                    <Typography variant="body1" className={`${stepsClasses.mb10}`}>{'CC: ' + idNumber || 'N/A'}</Typography>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}
