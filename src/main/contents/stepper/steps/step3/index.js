import React, { useState, useEffect } from 'react';
import { Container, Grid, Zoom} from '@material-ui/core';
import Step3 from './step3';
import Step31 from './step3.1';
import Step32 from './step3.2';

export default ({...props}) =>{
    const [page, SetPage] = useState(1);

    const nextStep = (event) =>{
        SetPage(page + 1);
    };

    useEffect(()=>{
        console.log('Componente step1 General montado');
    });

    const prevStep = () =>{
        SetPage(page - 1);
    };

    return (

        <Container>
            <Grid container direction={"row"}>
                <Grid item md={12}>
                    {page === 1 && <Step3 nextStep={nextStep} /> }
                </Grid>
                <Zoom in={page === 2} timeout={900}>
                    <Grid item md={12}>
                        {page === 2 &&  <Step31 nextStep={nextStep} /> }
                    </Grid>
                </Zoom>
                <Zoom in={page === 3} timeout={900}>
                    <Grid item md={12}>
                        {page === 3 &&  <Step32 nextStep={nextStep} /> }
                    </Grid>
                </Zoom>
            </Grid>
        </Container>
    )

}
