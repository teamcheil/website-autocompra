import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Stepper, Step, StepLabel, StepContent, Button, Paper, Typography } from '@material-ui/core';
import { stepsStyles } from '../../../styles/theme/layout/steps/steps';
import Step1 from "./steps/step1";
//import Step13 from './steps/step1.3';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
  stepeer:{
    padding:0
  }
}));

function getSteps() {
  return ['Conozcámonos', 'Simula', 'Compra','Otros Documentos','Felcitaciones'];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return <Step1 />;
    case 1:
      return 'Paso 2';
    case 2:
      return `Paso 3`;
    case 3:
        return `Paso 4`;
    case 4:
        return `Paso 5`;
    default:
      return 'Unknown step';
  }
}

function getLabelDetailStep(step){
  switch (step) {
    case 0:
      return `Te explicamos cómo funciona nuestra plataforma.`;
    case 1:
      return `Elige el plan que se ajusta a tu meta`;
    case 2:
      return `Inicia tu meta de ahorro programado con nosotros`;
    case 3:
        return `Conocerte mejor es nuestra prioridad`;
    case 4:
        return `Ya eres parte de Polen.`;
    default:
      return 'Unknown step';
  }
}

export default function VerticalLinearStepper({ Step1 }) {
  const classes = useStyles();
  const stepClasses = stepsStyles();
  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();

  /*const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };*/


  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel>
              {label}
              <span><br/>{getLabelDetailStep(index)}</span>
            </StepLabel>
            <StepContent className={`${stepClasses.showMobile}`}>
                {getStepContent(index)}
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography>All steps completed - you&apos;re finished</Typography>
          <Button onClick={handleReset} className={classes.button}>
            Reset
          </Button>
        </Paper>
      )}
    </div>
  );
}
