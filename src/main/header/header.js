import React, {useState} from "react";
import Button from '@material-ui/core/Button';
import logo from '../../assets/images/logo-vigilado.svg';
import {
    Menu,
    MenuItem,
    IconButton,
    Toolbar,
    AppBar,
    Link,
    Container,
    Box,
    Typography,
    Grid,
    Fade
} from '@material-ui/core';

import MenuIcon from '@material-ui/icons/Menu';
// Styles
import { headerStyles } from '../../styles/theme/layout/header';
import { buttonsStyles } from '../../styles/theme/components/buttons'
import LinearWithValueLabel from "../../components/LinearProgres/LinearProgressWithLabel";
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";
import IconChat from "../../assets/images/iconChat.svg";
import IconPhone from "../../assets/images/iconPhone.svg";
import {stepsStyles} from "../../styles/theme/layout/steps/steps";
import {stepperStyles} from "../../styles/theme/components/stepper";
import {typographiesStyles} from "../../styles/theme/components/typographies";

export default () =>{
    const [open, setOpen] = useState(false);
    const hClasses = headerStyles();
    const bClasses = buttonsStyles();
    const stepClasses = stepsStyles();
    const tClasses = typographiesStyles();
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };
    const handleMobileMenuOpen = (event) => {
        setMobileMoreAnchorEl(event.currentTarget);
    };

    const mobileMenuId = 'menu-mobile-polen';
    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={mobileMenuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
            className={hClasses.headerContainer}
            >
            <MenuItem><Link href="#" color="inherit"><Typography variant={'body1'}>¿Qué es Polen?</Typography></Link></MenuItem>
            <MenuItem><Link href="#" color="inherit"><Typography variant={'body1'}>Eventos</Typography></Link></MenuItem>
            <MenuItem><Link href="#" color="inherit"><Typography variant={'body1'}>Háblanos</Typography></Link></MenuItem>
        </Menu>
    );

    const handleClose = () => {
        setOpen(!open);
    };

    return (
        <Box>
            <AppBar position="static" color="transparent" className={hClasses.headerContainer}>
                <Container>
                    <Toolbar>
                        <div className={hClasses.logo} >
                            <Link href="/"><img alt={logo} src={logo} /></Link>
                        </div>
                        <div className={hClasses.sectionDesktopLink}>
                            <Link href="#" color="inherit"><Typography variant={'body1'}>¿Qué es Polen?</Typography></Link>
                            <Link href="#" color="inherit"><Typography variant={'body1'}>Eventos</Typography></Link>
                            <Link href="#" color="inherit"><Typography variant={'body1'}>Háblanos</Typography></Link>
                        </div>
                        <div className={hClasses.sectionDesktop}>
                            <Button className={`${bClasses.button} ${bClasses.orangeButton} ${hClasses.mr21}`}>Simula tu plan</Button>
                            <Button className={`${bClasses.button} ${bClasses.whiteButton}`}>Ingresa a tu cuenta</Button>
                        </div>
                        <div className={hClasses.sectionMobile}>
                            <IconButton
                                aria-label="show more"
                                aria-controls={mobileMenuId}
                                aria-haspopup="true"
                                onClick={handleMobileMenuOpen}
                                color="inherit"
                                >
                                <MenuIcon style={{ fontSize: 40 }} />
                            </IconButton>
                        </div>
                    </Toolbar>
                </Container>
            </AppBar>
            {renderMobileMenu}
        </Box>
    )
    
}
