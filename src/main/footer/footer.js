import React from "react";
import logo from '../../assets/images/logofooter.png';
import { Container, Link, Link as Link2, Grid, Box, Typography } from "@material-ui/core";
import { FooterStyles } from '../../styles/theme/layout/footer';

import logo2 from '../../assets/images/logo2.svg'
import vigiladosuperintendencia from '../../assets/images/vigilado-superintendencia.svg'
import whatsapp from '../../assets/images/whatsapp.svg'
import facebook from '../../assets/images/facebook.svg'
import instagram from '../../assets/images/instagram.svg'
import youtube from '../../assets/images/youtube.svg'

import logoFooter from '../../assets/images/logoFooter.svg';
import superintendenciadesociedades from '../../assets/images/superintendenciadesociedades.svg';

export default () => {
    const date = new Date();
    const fullYear = date.getFullYear();
    const fClasses = FooterStyles();

    const FancyLink = React.forwardRef((props) => {
        return <Link2 color="inherit" href={props.ref} {...props} >{props.children}</Link2>
    })

    return (
        <Box className={`${fClasses.footer}`}>
            <Container maxWidth={"md"}>
                <Grid container direction={"row"}>
                    <Grid item xs={6} md={4} className={`${fClasses.fColumn}`}>
                        <Box>
                            <Typography variant={'h5'}>Comunícate con nosotros</Typography>
                            <Grid container direction={"column"}>
                                <Link color="inherit" target="_blank" href="https://wwwdev.polen.com.co/talkToUs/3"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Contáctanos</Typography></Link>
                                <Link color="inherit" target="_blank" href="https://wwwdev.polen.com.co/whatispolen"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Acerca de nosotros</Typography></Link>
                            </Grid>
                        </Box>
                    </Grid>
                    <Grid item xs={6} md={4} className={`${fClasses.fColumn} ${fClasses.column2}`}>
                        <Box>
                            <Typography variant={'h5'}>Accesos Rápidos</Typography>
                            <Grid container direction={"column"}>
                                <Link color="inherit" target="_blank" href="https://wwwdev.polen.com.co/talkToUs/1"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Preguntas frecuentes</Typography></Link>
                                <Link color="inherit" target="_blank" href="https://wwwdev.polen.com.co/schedule"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Eventos</Typography></Link>
                                <Link color="inherit" target="_blank" href="https://autocompradev.polen.com.co/"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Simulador</Typography></Link>
                            </Grid>
                        </Box>
                    </Grid>
                    <Grid item xs={12} md={4} className={`${fClasses.fColumn} ${fClasses.column3}`}>
                        <Box>
                            <Typography variant={'h5'} className='orange'>Legales</Typography>
                            <Grid container direction={"column"}>
                                <Link2 href="https://media.polen.com.co/documentos/Condiciones_Uso_Sitio_Web.pdf" target={'_blank'}><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Condiciones de uso</Typography></Link2>
                                <Link2 href="https://media.polen.com.co/documentos/Politicas_Tratamiento_Datos.pdf" target={'_blank'}><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Tratamiento de datos</Typography></Link2>
                                <Link2 href="https://media.polen.com.co/documentos/Terminos_Condiciones.pdf" target={'_blank'}><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Consulta de información</Typography></Link2>
                                <Link2 href="https://media.polen.com.co/documentos/Terminos_Condiciones_Debito_Automatico.pdf" target={'_blank'}><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Débito automático</Typography></Link2>
                                <Link2 href="https://media.polen.com.co/documentos/Terminos_Condiciones_Tratamiento_Imagenes.pdf" target={'_blank'}><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Uso de imagen</Typography></Link2>
                                <Link2 href="https://media.polen.com.co/documentos/Documentos_Requeridos_para_la_Aprobacion_del_Credito.pdf" target={'_blank'}><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Documentos de crédito</Typography></Link2>
                            </Grid>
                        </Box>
                    </Grid>
                    <Grid item xs={12} className={fClasses.borderTop} >
                        <Grid container>
                            <Grid className={'hiden logo'} item xs={12} md={4}>
                                <Link to="/"><img alt={"logoFooter"} src={logoFooter} /></Link>
                                <Typography variant={'body1'} style={{ color: "#fff" }}>Sociedad Administradora de Planes<br></br>de Autofinanciamiento Comercial.</Typography>
                            </Grid>
                            <Grid className='divRowReverse' item xs={12} md={4}>
                                <Box className={`${fClasses.flexBottomContainer} subDivRowReverse`}>
                                    <img className='superintendencia' alt='Logo Super' src={superintendenciadesociedades} />
                                </Box>
                            </Grid>
                            <Grid className='contentSocialNetworks' item xs={12} md={4}>
                                <div>
                                    <a href="https://www.facebook.com/PolenColombia/" target="_blank">
                                        <img alt='Facebook' src={facebook} />
                                    </a>
                                    <a href="https://www.instagram.com/polencolombia/" target="_blank">
                                        <img alt='Instagram' src={instagram} />
                                    </a>
                                    <a href="https://www.youtube.com/channel/UCTQeAOftTMq3nwleeOFC_DA?view_as=subscriber" target="_blank">
                                        <img alt='Yooutube' src={youtube} />
                                    </a>
                                    <a href="https://wa.me/+573213095057" target="_blank">
                                        <img alt='WhatsApp' src={whatsapp} />
                                    </a>
                                </div>
                                <Typography variant={'h5'} className={`${fClasses.h5Link} ${fClasses.alignLeftBottom} deleteAbsolute`}>&copy; Copyright {fullYear} Polen Colombia.</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    {/* <Grid item xs={12} md={4} className={`${fClasses.fColumn} ${fClasses.column3}`}>
                        <Box>
                            <Typography variant={'h5'}>Legales</Typography>
                            <Grid container direction={"column"}>
                                <Link color="inherit" target="_blank" href="https://media.polen.com.co/documentos/Condiciones_Uso_Sitio_Web.pdf"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Condiciones de uso</Typography></Link>
                                <Link color="inherit" target="_blank" href="https://media.polen.com.co/documentos/Politicas_Tratamiento_Datos.pdf"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Tratamiento de datos</Typography></Link>
                                <Link color="inherit" target="_blank" href="https://media.polen.com.co/documentos/Terminos_Condiciones.pdf"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Consulta de información</Typography></Link>
                                <Link color="inherit" target="_blank" href="https://media.polen.com.co/documentos/Terminos_Condiciones_Debito_Automatico.pdf"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Débito automático</Typography></Link>
                                <Link color="inherit" target="_blank" href="https://media.polen.com.co/documentos/Terminos_Condiciones_Tratamiento_Imagenes.pdf"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Uso de imagen</Typography></Link>
                                <Link color="inherit" target="_blank" href="https://media.polen.com.co/documentos/Documentos_Requeridos_para_la_Aprobacion_del_Credito.pdf"><Typography variant={'h5'} className={`${fClasses.h5Link}`}>Documentos de crédito</Typography></Link>
                            </Grid>
                        </Box>
                    </Grid>                    
                    <Grid item xs={12} className={fClasses.borderTop}>
                        <Box className={fClasses.flexBottomContainer}>
                            <Typography variant={'h5'} className={`${fClasses.h5Link} ${fClasses.alignLeftBottom}`}>Copyright &copy; {fullYear} Polen.</Typography>
                            <Grid container>
                                <Grid item xs={4} className={fClasses.flexContainer}>
                                    <Typography variant={'h5'} className={`${fClasses.h5Link}`}>Vigilado:</Typography>
                                </Grid>
                                <Grid item xs={8}>
                                    <img alt='Logo Super' src={logo}/>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                    <Grid item xs={12} className={fClasses.footerBottom}>
                        <Grid className={fClasses.footerLogos}>
                            <img alt='' src={logo2} />
                        </Grid>
                        <Grid className={fClasses.footerLogos}>
                            <img alt='' src={vigiladosuperintendencia} />
                        </Grid>
                        <Grid className={`${fClasses.footerLogos} ${fClasses.footerLogosColumn}`}>
                            <Grid className={fClasses.footerCopyrightLinks}>
                                <Link color="inherit" href="#"><img alt='' src={whatsapp} /></Link>
                                <Link color="inherit" href="#"><img alt='' src={facebook} /></Link>
                                <Link color="inherit" href="#"><img alt='' src={instagram} /></Link>
                                <Link color="inherit" href="#"><img alt='' src={youtube} /></Link>
                            </Grid>
                            <Typography variant={'h5'} className={`${fClasses.h5Link} ${fClasses.footerCopyright}`}>&copy; Copyright {fullYear} Polen Colombia.</Typography>
                        </Grid>
                    </Grid> */}
                </Grid>
            </Container>
        </Box>
    )
}
