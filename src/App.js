import React from 'react';
import Header from './main/header/header';
import Footer from './main/footer/footer';
import  Main  from './main/contents';
import './App.css';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { themePolen } from './styles/theme/themePolen'
import { GlobalStyles } from './styles/globalStyles'
import { Provider } from 'react-redux';
import { store } from './redux/store/store';


export const  App = () => {
  return (
      <Provider store={store}>
          <CssBaseline />
          <GlobalStyles />
          <MuiThemeProvider theme={themePolen}>
              <Header />
              <main>
                  <Main />
              </main>
              <Footer />
          </MuiThemeProvider>
      </Provider>
  );
};

export default App;
