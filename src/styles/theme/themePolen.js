import Colors from "./constants/colors";
import { BeVietnamBold, BeVietnamRegular, BeVietnamMedium, BeVietnamLight, MyriadProRegular } from './constants/fonts';
import { createMuiTheme } from "@material-ui/core";

export const themePolen = createMuiTheme({
    palette: {
        primary: {
            main: Colors.primary,
            t_9: Colors.primary_9,
            t_2: Colors.primary_2,
        },
        white: {
            main: Colors.white,
        },
        black: {
            main: Colors.black,
            t_12: Colors.black_12,
            t_24: Colors.black_24,
        },
        gray:{
            gray_F2: Colors.grayF2,
            gray_A7: Colors.grayA7,
        },
        red:{
            main: Colors.red,
        },
        text: {
            disabled: Colors.black_24
        },
        action: {
            disabled: Colors.black_12
        }
    },
    body: Colors.primary,
    text: '#363537',
    toggleBorder: '#FFF',
    gradient: 'linear-gradient(#39598A, #79D7ED)',

    button: {
        backgroundColor: Colors.primary,
        borderColor: Colors.primary,
        color: Colors.white
    },

    typography: {
        fontFamily: [
            'MyriadProRegular',
            'BeVietnamRegular',
            'BeVietnamBold',
            'Arial',
        ].join(','),
        h1: {
            fontSize: '24px',
            fontWeight: 'bold',
            fontStretch: 'normal',
            lineHeight: '1.05',
            letterSpacing: 'normal',
            color: Colors.primary,
            fontFamily: BeVietnamBold,
            width: '100%'
        },
        h2: {
            fontFamily: BeVietnamRegular,
            fontSize: '24px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            lineHeight: 'normal',
            letterSpacing: 'normal',
            color: Colors.black,
            width: '100%'
        },
        h3: {
            fontSize: '18px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            lineHeight: 'normal',
            letterSpacing: 'normal',
            color: Colors.black,
            fontFamily: BeVietnamMedium,
            width: '100%'
        },
        h4: {
            fontFamily: BeVietnamMedium,
            fontSize: '18px',
            fontWeight: '500',
            fontStretch: 'normal',
            lineHeight: 'normal',
            letterSpacing: 'normal',
            color: Colors.black,
            width: '100%'
        },
        h5: {
            fontFamily: BeVietnamBold,
            fontSize: '12px',
            fontWeight: 'bold',
            fontStretch: 'normal',
            lineHeight: 'normal',
            letterSpacing: 'normal',
            color: Colors.primary,
            width: '100%'
        },
        h6: {
            fontFamily: BeVietnamRegular,
            fontSize: '16px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            lineHeight: '1.05',
            letterSpacing: 'normal',
            color: Colors.black,
            marginBottom: '16px',
            width: '100%'
        },
        body1: {
            fontFamily: MyriadProRegular,
            fontSize: '14px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            lineHeight: '14.6px',
            letterSpacing: 'normal',
            color: Colors.black,
            marginTop: '5px',
            marginBottom: '5px',
        },
        subtitle1: {
            fontFamily: BeVietnamLight,
            fontSize: '18px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            lineHeight: '1.05',
            letterSpacing: 'normal',
            color: Colors.black,
            marginBottom: '16px',
        },
        overline: {
            fontFamily: BeVietnamRegular,
            fontSize: '16px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            lineHeight: '1.05',
            letterSpacing: 'normal',
            textTransform: 'none',
            color: Colors.black,
        },
        TBeVietnamBold: BeVietnamBold,
        TBeVietnamRegular: BeVietnamRegular,
        TBeVietnamLight: BeVietnamLight,
        TBeVietnamMedium: BeVietnamMedium,
        TMyriadProRegular: MyriadProRegular
    },
    overrides: {
        MuiCssBaseline: {
            '@global': {
                '@font-face': [BeVietnamBold, BeVietnamRegular, BeVietnamLight, BeVietnamMedium, MyriadProRegular],
            },
        }
    },
});
