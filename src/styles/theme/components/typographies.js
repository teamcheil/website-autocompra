import { makeStyles } from "@material-ui/core/styles";


export const typographiesStyles = makeStyles((theme) => ({
    colorPrimary: {
        color: theme.palette.primary.main,
    },
    h1Stepper: {
        fontSize: '16px',
        marginBottom: '0px',
    },
    h1StepperNum: {
        fontSize: '18px',
        color: theme.palette.white.main,
    },
    h1Black: {
        color: theme.palette.black.main,
    },
    h1Black2: {
        color: theme.palette.black.main,
        fontSize: "21px"
    },
    h132: {
        fontSize: '32px',
        marginBottom: '15px',
    },
    h133: {
        fontSize: '32px',
        marginBottom: '0px',
    },
    h1Black18: {
        fontSize: '18px',
        marginBottom: '6px',
        color: theme.palette.black.main,
    },
    h1BlackLight: {
        color: theme.palette.black.main,
        fontFamily: 'BeVietnam-Regular',
        fontWeight: 'normal',
    },
    h114Black: {
        fontSize: '14px',
        lineHeight: '14.4px',
        color: theme.palette.black.main,
    },
    h118White: {
        fontSize: '18px',
        lineHeight: '18.5px',
        color: theme.palette.white.main,
    },
    h116White: {
        fontSize: '16px',
        lineHeight: '16.5px',
        color: theme.palette.white.main,
    },
    h6Required: {
        '&:after': {
            content: '*',
            color: theme.palette.red.main
        }
    },
    body1Italic: {
        fontStyle: 'italic',
    },
    body1Bold: {
        fontWeight: 'bold',
    },
    body124Bold: {
        fontWeight: 'bold',
        fontSize: '24px',
        lineHeight: '24.8px',
    },
    body110: {
        fontSize: '10px',
        lineHeight: '10.2px',
    },
    body110Center: {
        fontSize: '10px',
        lineHeight: '10.2px',
        textAlign: 'center',
    },
    body111Bold: {
        fontSize: '11px',
        lineHeight: '11.3px',
        fontWeight: 'bold',
    },
    body112: {
        fontSize: '12px',
        lineHeight: '12.4px',
        '& a': {
            cursor: 'pointer',
            fontWeight: 'bold',
            '&:hover': {
                textDecoration: 'none',
            }
        }
    },
    body116: {
        fontSize: '16px',
        lineHeight: '16.7px',
    },
    body116White: {
        fontSize: '16px',
        lineHeight: '16.7px',
        color: theme.palette.white.main
    },
    body118: {
        fontSize: '18px',
        lineHeight: '18.8px',
    },
    bodyList: {
        marginTop: '10px',
        marginBottom: '15px',
        '&:before': {
            content: '"•"',
            color: theme.palette.primary.main,
            fontSize: '20px',
            margin: '0px 6px',
        },
    },
    boldBVR: {
        color: theme.palette.black.main,
        fontWeight: 'bold',
        fontSize: '18px',
    }
}));
