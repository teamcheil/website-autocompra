import { makeStyles } from "@material-ui/core/styles";

export const stepperStyles = makeStyles((theme) => ({
    MyStepper: {
        backgroundColor: theme.palette.white.main,
        height: 'auto',
        width: '100%',
        cursor: 'default',
        '& .MuiStep-completed': {
            '& .inactive': {
                '& .h1StepperNum': {
                    display: 'none',
                },
            },
        },
        '& .MuiStepLabel-root': {
            display: 'flex',
            height: 'auto',
            position: 'relative',
        },
        [theme.breakpoints.up('md')]: {
            minWidth: '280px',
            '& .MuiStepLabel-iconContainer': {
                width: '20%',
                paddingRight: '0px',
                position: 'absolute',
                right: '0px',
                display: 'flex',
                justifyContent: 'center',
            },
            '& .MuiStepLabel-labelContainer': {
                float: 'left',
                height: '100%',
                width: '80%',
                textAlign: 'right',
            },
            '& .MuiStepLabel-label': {
                color: theme.palette.gray.gray_A7,
                fontWeight: 'bold',
                fontSize: '16px',
                textAlign: 'right',
                '& span': {
                    color: theme.palette.black.main,
                    fontWeight: 'normal',
                    fontSize: '14px',
                    lineHeight: '12px',
                },
            },
            '& .MuiStepLabel-completed': {
                color: theme.palette.primary.main,
            },
            '& .MuiStepLabel-active': {
                color: theme.palette.black.main,
            },
            '& .MuiStepConnector-root': {
                marginLeft: '0px',
                marginTop: '5px',
                marginBottom: '5px',
                paddingBottom: '0px',
                width: '100%',
                '& .MuiStepConnector-lineVertical': {
                    width: '10%',
                    float: 'right',
                    borderLeftColor: theme.palette.gray.gray_A7,
                }
            },
            '& .MuiStepConnector-completed': {
                '& .MuiStepConnector-lineVertical': {
                    borderLeftColor: theme.palette.primary.main + ' !important',
                }
            },
            '& .MuiStepConnector-active': {
                '& .MuiStepConnector-lineVertical': {
                    borderLeftColor: theme.palette.primary.main + ' !important',
                }
            },
        },
        [theme.breakpoints.up('lg')]: {
            maxWidth: '300px',
        },
        [theme.breakpoints.down('md')]: {
            padding: '4px',
        }
    },
    MyStepMain: {
        [theme.breakpoints.down('md')]: {
            '&.MuiStep-completed': {
                '& + .MuiStepConnector-root':{
                    display: 'none',
                },
                '& + .MuiStepConnector-complete':{
                    display: 'block',
                },
                '&.inactive': {
                    '& + .MuiStepConnector-root':{
                        display: 'block',
                    },
                }
            },
            '& .MuiStepLabel-labelContainer': {
                '& .h1StepperNum': {
                    position: 'absolute',
                    left: '7px',
                    top: '50%',
                    fontSize: '20px',
                    width: 'auto',
                    textAlign: 'center',
                    marginTop: '-10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    cursor: 'default',
                }
            },
            '& .MuiStepIcon-active': {
                borderRadius: '50%',
                webkitBorderRadius: '50%',
                MozBorderRadius: '50%',
                backgroundColor: theme.palette.primary.main,
                width: '34px',
                height: '34px',
                '& .MuiStepIcon-text': {
                    fill: theme.palette.primary.main + ' !important',
                }
            },
            '& .MuiStepLabel-iconContainer': {
                width: 'auto',
                height: '34px',
                marginLeft: '-4px'
            },
            '& .MuiStepIcon-completed': {
                color: theme.palette.primary.main + ' !important',
            },
            '& .active': {
                '& .MuiStepIcon-completed': {
                    borderRadius: '50%',
                    webkitBorderRadius: '50%',
                    MozBorderRadius: '50%',
                    backgroundColor: theme.palette.primary.main + ' !important',
                    width: '34px',
                    height: '34px'
                }
            },
            '& .MuiStepIcon-root': {
                border: 'none',
                borderRadius: '50%',
                webkitBorderRadius: '50%',
                MozBorderRadius: '50%',
                width: '34px',
                height: '34px',
                color: theme.palette.gray.gray_A7,
                '& .MuiStepIcon-text': {
                    fill: theme.palette.gray.gray_A7,
                }
            },
        },
        [theme.breakpoints.up('md')]: {
            '&.MuiStep-completed': {
                '&.active': {
                    '& + .MuiStepConnector-root':{
                        display: 'block',
                    },
                    '& .h1StepperNum': {
                        right: '16px',
                    }
                },
            },
            '&.inactive': {
                '& .h1StepperNum': {
                    right: '22px',
                },
                '& .MuiStepLabel-label': {
                    '& h1': {
                        color: theme.palette.gray.gray_A7,
                        '&.h1StepperNum': {
                            color: theme.palette.white.main,
                        }
                    },
                    '& p': {
                        display: 'none',
                    }
                }
            },
            '&.inactive:last-of-type': {
                '& + .MuiStepConnector-root':{
                    display: 'none',
                },
            },
            '& .MuiStepLabel-labelContainer': {
                width: '75%',
                '& .h1StepperNum': {
                    position: 'absolute',
                    left: 'inherit',
                    right: '16px',
                    top: '50%',
                    fontSize: '20px',
                    textAlign: 'center',
                    marginTop: '-10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    width: 'auto',
                }
            },
            '&.MuiStep-completed + .MuiStepConnector-complete':{
                display: 'block',
            },
            '& .MuiStepLabel-completed': {
                '& .inactive': {
                    '& .h1StepperNum': {
                        display: 'none !important',
                    }
                },
            },
            '& .MuiStepLabel-iconContainer': {
                width: '20%',
                height: '34px',
                marginLeft: '0px'
            },
            '& .MuiStepIcon-root': {
                border: 'none',
                borderRadius: '50%',
                webkitBorderRadius: '50%',
                MozBorderRadius: '50%',
                width: '34px',
                height: '34px',
                color: theme.palette.gray.gray_A7,
                '& .MuiStepIcon-text': {
                    fill: theme.palette.gray.gray_A7,
                }
            },
            '& .MuiStepIcon-active': {
                borderRadius: '50%',
                webkitBorderRadius: '50%',
                MozBorderRadius: '50%',
                color: theme.palette.primary.main,
                width: '100%',
                height: '100%',
                '& .MuiStepIcon-text': {
                    fill: theme.palette.primary.main + ' !important',
                }
            },
            '& .MuiStepIcon-completed': {
                color: theme.palette.primary.main,
            },
            '& .active': {
                '& .MuiStepLabel-iconContainer': {
                    borderRadius: '50%',
                    webkitBorderRadius: '50%',
                    MozBorderRadius: '50%',
                    border: '2px dotted !important',
                    borderColor: theme.palette.primary.main,
                    color: theme.palette.primary.main,
                    width: '56px',
                    height: '56px',
                    padding: '1px',
                    right: '-6px',
                },
                '& .MuiStepIcon-completed': {
                    borderRadius: '50%',
                    webkitBorderRadius: '50%',
                    MozBorderRadius: '50%',
                    backgroundColor: theme.palette.primary.main,
                    width: '100%',
                    height: '100%'
                }
            }
        },
        [theme.breakpoints.up('lg')]: {
            '&.MuiStep-completed': {
                '&.active': {
                    '& .h1StepperNum': {
                        right: '19px',
                    }
                },
            },
            '&.inactive': {
                '& .h1StepperNum': {
                    right: '24px',
                }
            },
            '& .active': {
                '& .MuiStepLabel-iconContainer': {
                    right: '-3px',
                },
                '&.active': {
                    '& .h1StepperNum': {
                        right: '19px',
                    }
                },
            }
        }
    },
    MyStepSecondary: {
        '&.inactive': {
            display: 'none',
            '& + .MuiStepConnector-root':{
                display: 'none',
            },
        },
        fontSize: '12px',
        '& .MuiStepLabel-labelContainer': {
            '& .h1StepperNum': {
                display: 'none',
            }
        },
        '& .MuiStepLabel-root .MuiTypography-body1': {
            display: 'none',
        },
        '& .MuiTypography-h1': {
            fontSize: '12px',
        },
        '& .MuiStepIcon-root': {
            border: '1px solid',
            borderRadius: '50%',
            webkitBorderRadius: '50%',
            MozBorderRadius: '50%',
            width: '16px',
            height: '16px',
            color: theme.palette.gray.gray_A7,
            '& .MuiStepIcon-text': {
                fill: theme.palette.gray.gray_A7,
            }
        },
        '& .MuiStepIcon-active': {
            color: theme.palette.white.main + '!important',
            border: '1px solid',
            borderColor: theme.palette.primary.main,
            '& .MuiStepIcon-text': {
                fill: theme.palette.white.main + ' !important',
            }
        },
        '& .MuiStepIcon-completed': {
            color: theme.palette.primary.main,
        },
        [theme.breakpoints.down('md')]: {
            '&.active': {
                '& + .MuiStepConnector-root':{
                    display: 'none',
                },
            },
            '& .MuiStepLabel-root': {
                display: 'none',
                paddingLeft: '5px',
            },
            '&.MuiStep-completed': {
                display: 'none',
                '& + .MuiStepConnector-root':{
                    display: 'none',
                },
            },
            '&.MuiStep-active': {
                display: 'none',
                '& + .MuiStepConnector-root':{
                    display: 'none',
                },
            },
        },
        [theme.breakpoints.up('md')]: {
            '&.active': {
                display: 'block',
                '& + .MuiStepConnector-root':{
                    display: 'block',
                },
            },
            '& .MuiStepLabel-root': {
                display: 'block',
            },
            '&.MuiStep-active': {
                display: 'block',
                '& + .MuiStepConnector-root':{
                    display: 'block',
                },
            },
            '& .MuiStepLabel-active': {
                '& h1': {
                    color: theme.palette.black.main
                }
            },
            '& .Mui-disabled': {
                '& h1': {
                    color: theme.palette.gray.gray_A7
                }
            }
        },
    },
}));
