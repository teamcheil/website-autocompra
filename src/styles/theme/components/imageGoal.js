import {makeStyles} from "@material-ui/core/styles";

export const imageGoalStyles = makeStyles((theme) => ({
    rootIG: {
        width: '85px',
        cursor: 'pointer',
        [theme.breakpoints.up('sm')]: {
            width: '97px',
        },
        [theme.breakpoints.down('sm')]: {
            width: '70px',
        },
        [theme.breakpoints.up('md')]: {
            width: '60px',
        }
    },
    containerIG: {
        padding: '10px 15px',
        textAlign: 'center',
        display:'flex',
        [theme.breakpoints.up('md')]: {
            justifyContent: 'space-between'
        }
    },
    inActive:{
        marginTop:'4px',
        fontSize:'12px'
    },
    active:{
        fontWeight:'bold',
        marginTop:'4px',
        fontSize:'12px'
    },
}));
