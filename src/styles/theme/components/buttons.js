import { makeStyles } from "@material-ui/core/styles";

export const buttonsStyles = makeStyles((theme) => ({
    button: {
        height: '44px',
        borderRadius: '4px',
        width: '100%',
        color: theme.palette.primary.main,
        '&:focus': {
            outline: 'none',
            borderColor: theme.palette.primary.main,
        },
        [theme.breakpoints.up('md')]: {
            width: '159px',
        }
    },
    button36: {
        height: '44px',
        borderRadius: '4px',
        width: '100%',
        color: theme.palette.primary.main,
        '&:focus': {
            outline: 'none',
            borderColor: theme.palette.primary.main,
        },
        [theme.breakpoints.down('md')]: {
            width: '80%',
            maxWidth: '100%',
        },
        [theme.breakpoints.up('md')]: {
            maxWidth: '159px',
        }
    },
    buttonNo: {
        height: '44px',
        borderRadius: '4px',
        width: '100%',
        color: theme.palette.primary.main,
        borderColor: theme.palette.primary.main,
        borderWidth: 1
    },
    buttonLarge: {
        [theme.breakpoints.up('md')]: {
            width: '250px',
        }
    },
    button100: {
        [theme.breakpoints.up('md')]: {
            width: '100%',
        }
    },
    orangeButton: {
        color: theme.palette.white.main,
        backgroundColor: theme.palette.primary.main,
        fontFamily: theme.typography.TBeVietnamBold,
        textTransform: 'none',
        '&:hover': {
            backgroundColor: theme.palette.primary.t_9,
        }
    },
    whiteButton: {
        color: theme.palette.primary.main,
        backgroundColor: theme.palette.white.main,
        border: 'solid 1px',
        borderColor: theme.palette.primary.main,
        boxShadow: 'none',
        textTransform: 'none',
        '&:hover': {
            backgroundColor: theme.palette.white.main,
        }
    },
    grayButton: {
        color: theme.palette.black.t_24,
        backgroundColor: theme.palette.black.t_12,
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: '16px',
        '&:hover': {
            color: theme.palette.white.main,
            backgroundColor: theme.palette.primary.main,
        }
    }
}));
