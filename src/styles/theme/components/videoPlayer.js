import { makeStyles } from "@material-ui/core/styles";

export const videoPlayerStyles = makeStyles((theme) => ({
    myVideo: {
        width: '100%',
        display: 'block',
        height: 'auto',
        '& .vjs-tech': {
            width: '100%',
            height: '100%',
            display: 'block',
        },
        '& .vjs-big-play-button': {
            display: 'none !important',
        },
        '& .vjs-progress-control': {
            width: '100%',
            height: '15px',
            opacity: '0.7',
            borderRadius: '4px',
            backgroundColor: theme.palette.black.main,
            color: theme.palette.primary.main,
        },
        '& .video-js .vjs-play-progress:before, .video-js .vjs-volume-level:before, .vjs-icon-circle:before, .vjs-seek-to-live-control .vjs-icon-placeholder:before': {
            content: " ",
        },
        '& .vjs-load-progress div': {
            background: theme.palette.white.main,
        },
        '& .vjs-play-progress': {
            backgroundColor: theme.palette.primary,
        },
        '& .vjs-load-progress': {
            borderRadius: '5px',
            background: '#c4c4c4',
        },
        '& .vjs-control-bar': {
            backgroundColor: 'transparent',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: '18px',
            left: '5%',
            width: '90%',
            '& .vjs-time-control, .vjs-playback-rate, .vjs-volume-panel, .vjs-modal-dialog, .vjs-seek-to-live-control, .vjs-chapters-button, .vjs-descriptions-button, .vjs-subs-caps-button, .vjs-audio-button, .vjs-picture-in-picture-control': {
                display: 'none',
            },
            '& .vjs-play-control': {
                width: '32.7px',
                borderRadius: '7px',
                backgroundColor: theme.palette.primary.main
            },
            '& .vjs-progress-control .vjs-control': {
                height: '4px',
            }
        },
        '& .video-js': {
            color: theme.palette.black.main,
            border: 'solid 0',
            borderColor: theme.palette.white.main,
            borderRadius: '4px',
            backgroundColor: theme.palette.black.main,
            '& :focus': {
                outline: 'none',
            },
        },
        '& .button:focus': {
            outline: '0px dotted',
        },
        '& video:focus': {
            outline: 'none !important',
        },
        '& button:focus': {
            outline: 'none !important'
        },
    }
}));
