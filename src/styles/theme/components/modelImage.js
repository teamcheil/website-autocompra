import {makeStyles} from "@material-ui/core/styles";

export const modelImageStyles = makeStyles((theme) => ({
    modelContainer: {
        width: '100%',
        maxWidth: '495px',
        padding: '0px 15px',
        margin: 'auto',
        minHeight: '550px',
        height: '100%',
        position: 'relative'
    },
    adjustBottom: {
        position: 'absolute',
        bottom: '0',
        left: '0',
        display: 'grid',
    },
    bubble: {
        width: '100%',
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        height:'127px',
        maxHeight:'127px',
        maxWidth: '353px',
        borderRadius: '56.5px',
        backgroundColor: theme.palette.gray.gray_F2,
        padding: '12px 10% 12px 10%',
        margin: 'auto',
        textAlign: 'center',
    },
    littleBubblesBox: {
        width: '100%',
        textAlign: 'center',
        position: 'relative',
    },
    littleBubblesBoxLeft: {
        width: '80%',
        textAlign: 'center',
        position: 'relative',
        left: '0'
    },
    littleBubblesBoxRight: {
        width: '80%',
        textAlign: 'center',
        position: 'relative',
        left: '20%'
    },
    bubbleCircleLg: {
        width: '12px',
        height: '12px',
        borderRadius: '50%',
        backgroundColor: theme.palette.gray.gray_F2,
        margin: 'auto',
        marginTop: '5px',
    },
    bubbleCircleMd: {
        width: '8px',
        height: '8px',
        borderRadius: '50%',
        backgroundColor: theme.palette.gray.gray_F2,
        margin: 'auto',
        marginTop: '3px',
    },
    bubbleCircleSm: {
        width: '6px',
        height: '6px',
        borderRadius: '50%',
        backgroundColor: theme.palette.gray.gray_F2,
        margin: 'auto',
        marginTop: '2px',
        marginBottom: '15px',
    },
    modelImage: {
        width: '100%'
    },
    modelImage60: {
        width: '60%',
        margin: 'auto'
    },
    modelImage90: {
        width: '90%',
        margin: 'auto'
    },
    modelImage100: {
        width: '100%',
        margin: 'auto',
        marginBottom: '13px'
    },
    modelImage130: {
        width: '120%',
        margin: 'auto',
        marginBottom: '20px'
    },
    modelImage150: {
        width: '150%',
        margin: 'auto',
        marginBottom: '0px'
    },
    modelImage200: {
        width: '200%',
        margin: 'auto',
        marginBottom: '57px'
    },
    modelImage70: {
        width: '70%',
        margin: 'auto'
    },
}));
