import { makeStyles, withStyles } from "@material-ui/core/styles";
import { Checkbox } from "@material-ui/core";
import React from "react";

export const inputStyles = makeStyles((theme) => ({
    input: {
        borderRadius: '4px',
        border: 'solid 1px #a7a8aa',
        backgroundColor: '#ffffff',
        padding: '5px',
        margin: '10px 0px 20px',
    },
    inputContainer: {
        paddingTop: '10px',
        '& label + .MuiInput-formControl': {
            marginTop: '12px',
            marginBottom: '25px'
        },
        "& .MuiFormLabel-asterisk": {
            color: 'rgba(254,91,2,1)'
        },
        '& .MuiFormLabel-root.Mui-error': {
            color: '#000 !important'
        },
        '& .MuiFormHelperText-root': {
            fontSize: '12px',
            position: 'absolute',
            bottom: '5px',
        }
    },
    inputContainerNoLabel: {
        paddingTop: '10px',
        '& label + .MuiInput-formControl': {
            marginTop: '12px',
            marginBottom: '25px'
        },
        "& .MuiFormLabel-asterisk": {
            color: '#fff',
        },
        '& .MuiFormLabel-root.Mui-error': {
            color: '#000 !important'
        },
        '& .MuiFormHelperText-root': {
            fontSize: '12px',
            position: 'absolute',
            bottom: '5px',
        },
        '& .makeStyles-label-156': {
            color: '#fff',
        },
        '& .makeStyles-label-212': {
            color: '#fff',
        }
    },
    width45: {
        width: "45%"
    },
    selectContainer: {
        width: '100%',
        height: '50px',
        marginTop: '0px',
        '&:before': {
            borderBottom: 'none'
        },
        '&:hover:not(.Mui-disabled):before': {
            borderBottom: 'none'
        },
        '&.MuiInput-underline:after': {
            borderBottom: 'none'
        },
        '& .MuiSelect-root': {
            borderRadius: '4px',
            border: 'solid 1px',
            borderColor: theme.palette.gray.gray_A7,
            backgroundColor: theme.palette.white.main,
            width: '100%',
            padding: '10px 5px',
            margin: '10px 0px 20px',
            '& input': {
                display: 'none',

            }
        },
        '& .MuiSelect-icon': {
            top: 'calc(50% - 18px)',
        },
    },
    inputContainerDate: {
        width: '100%',
        border: '1px solid #a7a8aa',
        height: '40px',
        borderRadius: '5px',
        '& .MuiInput-underline': {
            borderBottom: '0px solid red !important',
            '&:before': {
                borderBottom: '0px solid red !important',
            },
            '&:after': {
                borderBottom: '0px solid red !important',
            }
        },
        '& .MuiFormLabel-root': {
            border: '0px solid red',
            fontSize: '20px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            lineHeight: '20.8px',
            letterSpacing: 'normal',
            width: '130%',
            marginTop: '-25px'
        },
        '& .MuiInputBase-input': {
            border: '0px solid red',
            padding: '0px',
            marginLeft: '5px',
            marginTop: '-5px'
        },
        '& .MuiFormLabel-root.Mui-focused': {
            color: '#000',
        },
        '& .MuiFormLabel-root  .MuiFormLabel-asterisk': {
            color: 'rgba(254,91,2,1) !important',
            '&:before': {
                color: 'rgba(254,91,2,1) !important',
            },
            '&:after': {
                color: 'rgba(254,91,2,1) !important',
            }
        },
        '& .MuiPaper-root': {
            border: '1px solid red !important',
            marginLeft: '-15px !important',
            width: '35% !important',
            display: 'flex !important',
            justifyContent: 'center !important',
        }
    },
    label: {
        fontSize: '20px',
        fontWeight: 'normal',
        fontStretch: 'normal',
        lineHeight: '20.8px',
        letterSpacing: 'normal',
        color: theme.palette.black.main,
        width: '100%',
        marginTop: '0px'
    },
    myDropzone: {
        border: '1px dotted',
        borderColor: theme.palette.primary.main,
        width: '100%',
        height: '150px',
    }
}));

export const OrangeCheckbox = withStyles({
    root: {
        color: '#fe5b02',
        '&$checked': {
            color: '#fe5b02',
        },
    },
    checked: {},
})((props) => <Checkbox color='default' {...props} />);
