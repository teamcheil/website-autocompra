import { makeStyles, withStyles } from "@material-ui/core/styles";
import React from "react";

export const myCustomDropzoneStyle = makeStyles((theme) => ({
    container: {
        border: '2px dashed',
        borderColor: theme.palette.primary.main,
        padding: '15px 5px 10px',
        margin: '10px 0px',
        display: 'block',
        width: '100%',
        height: '100%',
        textAlign: 'center',
        borderRadius: '5px',
    },
    image: {
        width: 'auto',
        maxHeight: '30px',
        margin: 'auto',
    },
    text: {
        margin: '5px auto 10px',
        textAlign: 'center',
    },
}));
