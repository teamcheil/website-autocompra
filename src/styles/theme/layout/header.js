import {makeStyles} from "@material-ui/core/styles";

export const headerStyles = makeStyles((theme) => ({
    headerContainer:{
        padding: '10px 0px',
        boxShadow: '0 4px 4px 0 rgba(0, 0, 0, 0.05)'
    },
    grow: {
        flexGrow: 1,
    },
    logo:{
        padding: 'padding: 12px',
        [theme.breakpoints.down('sm')]: {
            flexGrow: 1,
            padding: '15px'
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex'
        }
    },
    sectionDesktopLink:{
        display: 'none',
        [theme.breakpoints.up('md')]: {
            flexGrow: 1,
            display: 'flex',
            justifyContent: 'space-evenly'

        }
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none'
        },
    },
    mr21: {
        marginRight: '21px',
    },
    barProgress:{
        [theme.breakpoints.up('md')]: {
            display: 'none'
        },
    }
}));
