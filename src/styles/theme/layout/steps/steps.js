import { makeStyles } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import React from "react";
import {BeVietnamBold, BeVietnamRegular, MyriadProRegular} from '../../constants/fonts'

export const stepsStyles = makeStyles((theme) => ({
    mainContainer: {
        [theme.breakpoints.down('md')]: {
            paddingTop: '70px !important',
            position: 'relative',
        },
        [theme.breakpoints.up('md')]: {
            paddingTop: '20px !important',
            position: 'relative',
        }
    },
    showMobile: {
        display: 'block',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        }
    },
    showDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'block',
        }
    },
    LinearMobile: {
        width: '100%'
    },
    boxLinearProgress: {
        marginTop: '45px !important',
    },
    nonLinearProgress: {
        marginTop: '0px',
        position: 'relative',
        [theme.breakpoints.up('md')]: {
            marginTop: '30px',
        }
    },
    withLinearProgress: {
        marginTop: '30px !important',
    },
    mb90: {
        marginBottom: '90px !important',
    },
    mb15: {
        marginBottom: '15px !important',
    },
    mb5: {
        marginBottom: '5px !important',
    },
    mb20: {
        marginBottom: '20px !important',
    },
    mt0: {
        marginTop: '0px !important',
    },
    mt5: {
        marginTop: '5px !important',
    },
    mt35: {
        marginTop: '35px !important',
    },
    mt15: {
        marginTop: '15px !important',
    },
    mt20: {
        marginTop: '20px !important',
    },
    mt25: {
        marginTop: '25px !important',
    },
    mt30: {
        marginTop: '30px !important',
    },
    mt90: {
        marginTop: '90px !important',
    },
    mt218: {
        marginTop: '218px !important',
    },
    mt30p: {
        marginTop: '30% !important',
    },
    boxBubble: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    bubble: {
        width: '100%',
        maxWidth: '353px',
        borderRadius: '56.5px',
        backgroundColor: theme.palette.gray.gray_F2,
        padding: '12px 48px 12px 48px',
        marginTop: '1rem',
        textAlign: 'center',
    },
    bubbleLeft: {
        borderRadius: '56.5px',
        backgroundColor: theme.palette.gray.gray_F2,
        padding: '24px 14px 16px',
        textAlign: 'center',
    },
    littleBubblesBox: {
        width: '100%',
        textAlign: 'center',
        position: 'relative',
    },
    littleBubblesBoxLeft: {
        width: '80%',
        textAlign: 'center',
        position: 'relative',
        left: '0'
    },
    littleBubblesBoxRight: {
        width: '80%',
        textAlign: 'center',
        position: 'relative',
        left: '20%'
    },
    bubbleCircleLg: {
        width: '12px',
        height: '12px',
        borderRadius: '50%',
        backgroundColor: theme.palette.gray.gray_F2,
        margin: 'auto',
        marginTop: '5px',
    },
    bubbleCircleMd: {
        width: '8px',
        height: '8px',
        borderRadius: '50%',
        backgroundColor: theme.palette.gray.gray_F2,
        margin: 'auto',
        marginTop: '3px',
    },
    bubbleCircleSm: {
        width: '6px',
        height: '6px',
        borderRadius: '50%',
        backgroundColor: theme.palette.gray.gray_F2,
        margin: 'auto',
        marginTop: '2px',
        marginBottom: '15px',
    },
    bubbleRight: {
        left: '20px',
        position: 'relative',
    },
    boxBtnNext: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: '30px !important',
        [theme.breakpoints.up('md')]: {
            marginBottom: '50px !important',
        }
    },
    boxVideo: {
        width: '100%',
    },
    alignBottom: {
        position: 'absolute',
        bottom: '0px',
        width: '100%',
        padding: '0px 15px',
    },
    modelImage: {
        width: '100%',
        maxWidth: '435px'
    },
    // CheckBox
    ckBoxInline: {
        display: 'flex',
        alignItems: 'center',
        '& .MuiFormControlLabel-root': {
            marginRight: '0px',
        }
    },
    // ModelContainersStyles
    modelLeft: {
        alignItems: 'flex-end',
        position: 'relative',
        padding: '0px 0px 0px 20px',
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'block',
        }
    },
    modelCenter: {
        textAlign: 'center',
        alignItems: 'flex-end',
        position: 'relative',
        minHeight: '550px',
        height: '100%',
    },
    iconSupport: {
        width: '49px',
    },
    // STEP 3
    contractSample: {
        padding: '7px',
        justifyContent: 'space-between',
    },
    contractSampleBorder: {
        border: '2px solid #fe5b02',
        borderRadius: '5px',
        position: 'relative',
    },
    contractSampleNumber: {
        backgroundColor: theme.palette.primary.t_2,
        border: '3px solid',
        borderColor: theme.palette.primary.main,
        borderRadius: '5px',
        width: '53px !important',
        height: '31px'
    },
    contractSampleDialog: {
        position: 'absolute',
        backgroundColor: 'rgba(254, 91, 2)',
        border: '3px solid',
        borderColor: theme.palette.white.main,
        borderRadius: '5px',
        width: '153px !important',
        height: '43px',
        right: '13px',
        top: '-20px',
        padding: '2px',
    },
    dialog: {
        fontWeight: 'bold',
        margin: 'auto'
    },
    logoContractSample: {
        height: '31px'
    },
    xxxx: {
        display: 'block',
        textAlign: 'center',
        width: '100%',
    },
    anexo: {
        display: 'block',
        width: '100%',
        margin: '5px 0px 0px 0px',
    },
    anexoBold: {
        fontWeight: 'bold',
        display: 'block',
        width: '100%',
        margin: '0px 0px 0px 0px',
    },
    alignCenter: {
        textAlign: "center"
    },
    textColorWhite: {
        color: '#fff'
    },
    sendAgain: {
        border: '0px solid #fe5b02',
        justifyContent: 'flex-end',
        color: 'red'
    },
    sendAgainText: {
        cursor: "pointer",
        color: '#fe5b02',
        textDecorationLine: 'underline',
        marginTop: '-15px',
    },
    Ellipse84: {
        width: '120px',
        height: '120px',
        backgroundColor: '#2ecc71',
        borderRadius: '120px',
        [theme.breakpoints.down('xs')]: {
            width: '80px',
            height: '80px',
        }
    },
    mb30: {
        marginBottom: '30px !important',
    },
    mb10: {
        marginBottom: '10px !important',
    },
    mt10: {
        marginTop: '10px !important',
    },
    mr15: {
        marginRight: '15px !important',
    },
    spaceBetween: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    cursorPointer: {
        cursor: 'pointer'
    },
    flex: {
        border: '0px solid red',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Rectangle_133: {
        padding: '23px',
        flexDirection: 'column',
        height: '198px',
        borderRadius: '10px',
        boxShadow: '0 4px 4px 0 rgba(141, 141, 141, 0.15)',
        backgroundColor: '#ffffff'
    },
    prevImagesContainer: {
        width: '100%',
        display: 'flex',
        minHeight: '120px',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '30px',
        marginBottom: '20px',
    },
    prevImagesBox: {
        margin: 'auto',
    },
    prevImagesBackground: {
        borderRadius: '10px',
        webkitBorderRadius: '10px',
        backgroundColor: theme.palette.gray.gray_F2,
        width: '120px',
        height: '120px',
        textAlign: 'center',
        '& img': {
            width: 'auto',
            maxWidth: '100px',
            height: 'auto',
            maxHeight: '100px',
            margin: '10px auto'
        }
    },
    divAlert: {
        marginTop: '35px',
        border: '1px solid',
        borderColor: theme.palette.primary.main,
        backgroundColor: theme.palette.primary.t_2,
        padding: '20px 30px',
    },
    flexStart: {
        justifyContent: 'flex-start',
        display: 'flex'
    },
    apps: {
        width: '15rem',
        marginTop: '-5rem',
        position: 'absolute',
        marginLeft: '1.6rem', display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    textEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    // HelpButton
    mobileHelpButton: {
        height: '50px',
        width: '100%',
        backgroundColor: theme.palette.primary.main,
        marginBottom: '20px',
        position: 'absolute',
        top: '0px',
        left: '0px',
        display: 'flex',
        alignItems: 'center',
    },
    mobileHelpButtonIcon: {
        marginRight: '10px',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        '& img': {
            width: 'auto',
            maxHeight: '40px',
        }
    },
    mobileHelpButtonArrow: {
        marginRight: '20px',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        '& img': {
            width: 'auto',
            maxHeight: '40px',
        }
    },
    mobileHelpContainer: {
        position: "absolute",
        textAlign: 'center',
        left: '0px',
        top: '50px',
        backgroundColor: theme.palette.primary.main,
        width: '100%',
        height: '98%',
        color: theme.palette.white.main,
        zIndex: '100',
    },
    mobileHelpBody1: {
        margin: '20px',
        maxWidth: '380px',
    },
    mobileHelpContainerButton: {
        marginTop: '30px',
        '& img': {
            height: '80px',
            width: 'auto',
        },
        '& p': {
            textAlign: 'center',
            fontWeight: 'bold'
        }
    },
    // DropZone
    dropzonePrev: {
        textAlign: 'center',
        padding: '5px 20px',
        border: '1px solid',
        borderColor: theme.palette.gray.gray_A7,
        '& .image': {
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            '& img': {
                width: 'auto',
                maxHeight: '30px',
            }
        },
        '& .content': {
            textAlign: 'left',
            '& .title': {
                fontSize: '12px',
            },
            '& div': {
                border: '1px solid',
                borderRadius: '25px',
                webkitBorderRadius: '25px',
                borderColor: theme.palette.primary.main,
                backgroundColor: theme.palette.primary.main,
                height: '5px',
                width: '100%',
            },
            '& .percent': {
                fontSize: '10px',
                color: theme.palette.gray.gray_A7,
                margin: '1px 0px',
            }
        },
        '& .delete': {
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
            '& button': {
                minWidth: '20px'
            },
            '& img': {
                width: 'auto',
                maxHeight: '25px',
            }
        }
    },
    row: {
        display: "flex",
        flexDirection: "row",
    },
    row2: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center"
    },

    apps2: {
        width: '15rem',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        [theme.breakpoints.up('sm')]: {
        },
        [theme.breakpoints.down('md')]: {
            // position: 'absolute',
            // marginTop: '-6rem',
            // marginLeft: '1.6rem',
        },
        [theme.breakpoints.up('lg')]: {
            position: 'absolute',
            marginTop: '-5rem',
            marginLeft: '1.6rem',
        }
    },

    contImageStores: {
        width: '20rem',
        [theme.breakpoints.up('sm')]: {
            flexDirection: "column",
            "& img": {
                width: '5rem',
            }
        },
        [theme.breakpoints.down('md')]: {
            flexDirection: "row",
            "& img": {
                width: '7rem',
            }
        },
        [theme.breakpoints.up('lg')]: {
            flexDirection: "row",
            "& img": {
                width: '5rem',
            }
        }
    },
    captcha: {
        '& .grecaptcha-badge': {
            maxWidth: '100% !important',
            position: 'inherit !important',
            margin: 'auto !important'
        }
    },
    //SIMULACIONES
    modalContainer: {
        position: "fixed",
        top: 0,
        left: 0,
        zIndex: 3,
        backgroundColor: "rgba(0, 0, 0, .5)",
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    modal: {
        borderWidth: 0,
        borderColor: "#000",
        borderStyle: "solid",
        backgroundColor: "#fff",
        borderRadius: 13,
        width: 821,
        height: 541,
        [theme.breakpoints.down('md')]: {
            width: "90%",
            height: 557
        }
    },
    close: {
        borderWidth: 0,
        borderColor: "#000",
        borderStyle: "solid",
        width: 19,
        height: 19,
        position: "relative",
        top: 13,
        left: "96%",
        cursor: "pointer",
        [theme.breakpoints.down('md')]: {
            left: "91%",
        }
    },
    modalContent: {
        borderWidth: 0,
        borderColor: "blue",
        borderStyle: "solid",
        width: "96%",
        height: 421,
        margin: "0 auto",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        [theme.breakpoints.down('md')]: {
            height: 479
        }
    },
    simulationsContainerWrapper: {
        borderWidth: 0,
        borderColor: "red",
        borderStyle: "solid",
        width: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    arrowsContainer: {
        borderWidth: 0,
        borderColor: "#000",
        borderStyle: "solid",
        width: 29,
        height: 43,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer"
    },
    simulationsContainer: {
        borderWidth: 0,
        borderColor: "#000",
        borderStyle: "solid",
        width: "100%",
        display: "flex",
        flexDirection: "row",
        //justifyContent: "space-between",
        alignItems: "center",
        overflowX: "scroll"
    },
    simulationCard: {
        borderWidth: 0,
        borderColor: "#000",
        borderStyle: "solid",
        borderRadius: "8px",
        width: 152,
        minWidth: 152,
        height: 271,
        margin: 7,
        padding: 13,
        boxShadow: "0 4px 4px 0 rgba(141, 141, 141, 0.15)",
        backgroundColor: "#ffffff",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer"
    },
    simulationCardSelected: {
        borderWidth: 1,
        borderColor: "#fe5b02",
        borderStyle: "solid",
        borderRadius: "8px",
        width: 152,
        minWidth: 152,
        height: 271,
        margin: 7,
        padding: 13,
        boxShadow: "0 4px 4px 0 rgba(141, 141, 141, 0.15)",
        backgroundColor: "#ffffff",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    simulationCard2: {
        borderWidth: 0,
        borderColor: "#000",
        borderStyle: "solid",
        borderRadius: "8px",
        width: 132,
        minWidth: 132,
        height: 251,
        margin: 3,
        padding: 13,
        boxShadow: "0 4px 4px 0 rgba(141, 141, 141, 0.15)",
        backgroundColor: "#ffffff",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer"
    },
    simulationCardSelected2: {
        borderWidth: 1,
        borderColor: "#fe5b02",
        borderStyle: "solid",
        borderRadius: "8px",
        width: 132,
        minWidth: 132,
        height: 251,
        margin: 3,
        padding: 13,
        boxShadow: "0 4px 4px 0 rgba(141, 141, 141, 0.15)",
        backgroundColor: "#ffffff",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    simulationCardIcon: {
        width: 41,
        height: 41
    },
    buttonsContainer: {
        borderWidth: 0,
        borderColor: "green",
        borderStyle: "solid",
        width: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        [theme.breakpoints.down('md')]: {
            flexDirection: "column-reverse",
        }
    },
    enabledButton: {
        borderWidth: 1,
        borderColor: "#fe5b02",
        borderStyle: "solid",
        borderRadius: 5,
        width: 167,
        height: 47,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        margin: 7,
        cursor: "pointer"
    },
    disabledButton: {
        borderWidth: 0,
        borderColor: "#fe5b02",
        borderStyle: "solid",
        borderRadius: 5,
        backgroundColor: "rgba(0, 0, 0, 0.12)",
        width: 167,
        height: 47,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        margin: 7,
        cursor: "auto"
    },
    enabledButton2: {
        borderWidth: 1,
        borderColor: "#fe5b02",
        borderStyle: "solid",
        borderRadius: 5,
        backgroundColor: "#fe5b02",
        width: 167,
        height: 47,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        margin: 7,
        cursor: "pointer"
    },
    disabledButton2: {
        borderWidth: 0,
        borderColor: "#fe5b02",
        borderStyle: "solid",
        borderRadius: 5,
        backgroundColor: "rgba(0, 0, 0, 0.12)",
        width: 167,
        height: 47,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        margin: 7,
        cursor: "auto"
    },
    enabledButtonText: {
        fontFamily: BeVietnamBold,
        fontSize: 16,
        fontWeight: "bold",
        color: "#000000",
        textAlign: "center"
    },
    disabledButtonText: {
        fontFamily: MyriadProRegular,
        fontSize: 16,
        fontWeight: "bold",
        color: "rgba(0, 0, 0, 0.12)",
        textAlign: "center"
    },
    enabledButton2Text: {
        fontFamily: MyriadProRegular,
        fontSize: 16,
        fontWeight: "bold",
        color: "#fff",
        textAlign: "center"
    },
    disabledButton2Text: {
        fontFamily: MyriadProRegular,
        fontSize: 16,
        fontWeight: "bold",
        color: "rgba(0, 0, 0, 0.12)",
        textAlign: "center"
    },
    simulationCardPlanName: {
        fontFamily: BeVietnamRegular,
        fontSize: 14,
        fontWeight: "bold",
        color: "#000000",
    },
    simulationCardLabels: {
        textAlign: "center",
        fontFamily: MyriadProRegular,
        fontSize: 12,
        color: "#000000",
    },
    simulationCardValues: {
        fontFamily: MyriadProRegular,
        fontSize: 14,
        fontWeight: "600",
        color: "#000000",
        textAlign: "center"
    },
    simulationCardDelete: {
        fontFamily: MyriadProRegular,
        fontSize: 12,
        fontWeight: "600",
        color: "#fe5b02",
        cursor: "pointer"
    },
    title: {
        fontFamily: BeVietnamBold,
        fontSize: 24,
        fontWeight: "bold",
        color: "#000",
        marginTop: 43,
        marginBottom: 23,
        textAlign: "center"
    },
    subtitle: {
        fontFamily: BeVietnamRegular,
        fontSize: 16,
        color: "#000",
        marginBottom: 19,
        textAlign: "center"
    },
    title2: {
        fontFamily: BeVietnamBold,
        fontSize: 16,
        fontWeight: "bold",
        color: "#000",
        marginBottom: 23
    },
    subtitle2: {
        fontFamily: BeVietnamRegular,
        fontSize: 16,
        color: "#000",
        marginBottom: 19,
        textAlign: "center"
    },
    modalContent2: {
        borderWidth: 0,
        borderColor: "blue",
        borderStyle: "solid",
        width: "96%",
        height: 310,
        margin: "0 auto",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        [theme.breakpoints.down('md')]: {
            height: 337
        }
    },
    modalContent3: {
        borderWidth: 0,
        borderColor: "blue",
        borderStyle: "solid",
        width: "96%",
        height: 410,
        margin: "0 auto",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        [theme.breakpoints.down('md')]: {
            height: 450
        }
    },
    modal2: {
        borderWidth: 0,
        borderColor: "#000",
        borderStyle: "solid",
        backgroundColor: "#fff",
        borderRadius: 13,
        width: 350,
        height: 350,
        zIndex: 5,
        [theme.breakpoints.down('md')]: {
            width: "97%",
            height: 373
        }
    },
    modal3: {
        borderWidth: 0,
        borderColor: "#000",
        borderStyle: "solid",
        backgroundColor: "#fff",
        borderRadius: 13,
        width: 350,
        height: 450,
        zIndex: 5,
        [theme.breakpoints.down('md')]: {
            width: "97%",
            height: 490
        }
    },
    close2: {
        width: 19,
        height: 19,
        position: "relative",
        top: 13,
        left: "90%",
        cursor: "pointer"
    },
    iconopopuppaso2: {
        width: 89,
        height: 89,
        marginBottom: 17
    },
    //loader
    modalLoader: {
        borderWidth: 0,
        borderColor: "#000",
        borderStyle: "solid",
        backgroundColor: "#fff",
        borderRadius: 13,
        width: 409,
        height: 283,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        [theme.breakpoints.down('sm')]: {
            width: "91%",
            height: 283
        }
    }
}));
