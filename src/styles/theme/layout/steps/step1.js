import {makeStyles} from "@material-ui/core/styles";

export const step1Styles = makeStyles((theme) => ({
    imgCenterMarginTop: {
        marginTop: 45,
        textAlign: 'center',
    },
    imgModelCenter: {
        width: '100%',
        maxWidth: '496px',
        height: '330px',
        objectFit: 'contain',
    },
}));
