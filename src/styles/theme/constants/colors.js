const Colors = {
    primary: 'rgba(254,91,2,1)',
    primary_9: 'rgba(254,91,2,0.9)',
    primary_2: 'rgba(254,91,2,0.2)',
    white: 'rgba(255, 255, 255, 1)',
    black: 'rgba(0, 0, 0, 1)',
    black_12: 'rgba(0, 0, 0, 0.12)',
    black_24: 'rgba(0, 0, 0, 0.24)',
    grayF2: 'rgba(242, 242, 242, 1)',
    grayA7: 'rgba(167, 168, 170, 1)',
    red: 'rgba(255, 0, 0, 1)',
};

export default Colors;
