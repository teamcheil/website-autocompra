// import FontBeVietnamBold from '../../../assets/fonts/BeVietnam-Bold.ttf';
// import FontBeVietnamRegular from '../../../assets/fonts/BeVietnam-Regular.ttf';
// import FontBeVietnamMedium from '../../../assets/fonts/BeVietnam-Medium.ttf';
// import FontBeVietnamLight from '../../../assets/fonts/BeVietnam-Light.ttf';
// import FontMyriadProRegular from '../../../assets/fonts/MyriadPro-Regular.otf';

import FontBeVietnamBold from '../../../assets/fonts/BeVietnam-Bold.woff2';
import FontBeVietnamRegular from '../../../assets/fonts/BeVietnam-Regular.woff2';
import FontBeVietnamMedium from '../../../assets/fonts/BeVietnam-Medium.woff2';
import FontBeVietnamLight from '../../../assets/fonts/BeVietnam-Light.woff2';
import FontMyriadProRegular from '../../../assets/fonts/MyriadPro-Regular.woff2';

export const BeVietnamBold = {
    fontFamily: 'BeVietnamBold',
    fontStyle: 'bold',
    src: `
    local('BeVietnam-Bold'),
    url(${FontBeVietnamBold}) format('woff2')
  `,
    unicodeRange:
        'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

export const BeVietnamRegular = {
    fontFamily: 'BeVietnamRegular',
    fontStyle: 'normal',
    src: `
    local('BeVietnamRegular'),
    local('BeVietnam-Regular'),
    url(${FontBeVietnamRegular}) format('woff2')
  `,
    unicodeRange:
        'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

export const BeVietnamLight = {
    fontFamily: 'BeVietnamLight',
    fontStyle: 'normal',
    src: `
    local('BeVietnamLight'),
    local('BeVietnam-Light'),
    url(${FontBeVietnamLight}) format('woff2')
  `,
    unicodeRange:
        'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

export const BeVietnamMedium = {
    fontFamily: 'BeVietnamMedium',
    fontStyle: 'normal',
    src: `
    local('BeVietnamMedium'),
    local('BeVietnam-Medium'),
    url(${FontBeVietnamMedium}) format('woff2')
  `,
    unicodeRange:
        'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

export const MyriadProRegular = {
    fontFamily: 'MyriadProRegular',
    fontStyle: 'normal',
    src: `
    local('MyriadProRegular'),
    local('MyriadPro-Regular'),
    url(${FontMyriadProRegular}) format('otf')
  `,
    unicodeRange:
        'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

