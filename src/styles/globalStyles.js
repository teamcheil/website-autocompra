// global.js
// Source: https://github.com/maximakymenko/react-day-night-toggle-app/blob/master/src/global.js#L23-L41
import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  body {
    padding: 0;
    background-color: #FFFFFF;
  }
  .dzu-dropzone {
    border: none;
  }
  .MuiStepper-root {
    padding: 24px 0px !important
  }
  `;
