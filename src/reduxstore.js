var createStore = require('redux').createStore;

var initialState = {
    goal: '',
    names: '',
    lastnames: '',
    email: '',
    phone: '',
    terms_conditions: false
}

function reducer(state, action) {
    switch (action.type) {
        case 'SET':
            state[action.var] = action.value;
            return state;
        default:
            return state;
    }
}

export var store = createStore(reducer, initialState)
export var state = store.getState()